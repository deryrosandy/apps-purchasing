<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Laporan Purchase Order</h3>
            </div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-8 col-md-offset-1">
            	<!-- form start -->
					<form action="<?php echo base_url(); ?>report/print_po_report" class="form-horizontal" id=""  target="_blank" method="get" enctype="multipart/form-data">			
						<div class="box-body">

							<div class="form-group">
								<label for="dari_tanggal" class="col-sm-3 control-label">Dari Tanggal</label>

								<div class="col-sm-9">
									<div class="input-group date print_date" data-date-format="yyyy-mm-dd">
										<input type='text' class="form-control" placeholder="yyyy-mm-dd" required name="dari_tanggal" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="dari_tanggal" class="col-sm-3 control-label">Sampai Tanggal</label>

								<div class="col-sm-9">
									<div class="input-group date print_date" data-date-format="yyyy-mm-dd">
										<input type='text' class="form-control" placeholder="yyyy-mm-dd" required name="sampai_tanggal"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<a href="<?php echo base_url(); ?>dashboard" class="btn btn-default">Cancel</a>
							<button type="submit" id="btn_submit_laporan" class="btn btn-primary pull-right"><i class="fa fa-print"></i> Cetak Laporan</button>
							<div>&nbsp;</div>
						</div>
						<!-- /.box-footer -->
					</form>
         		</div>
          	</div>
		</div>
	</div>
</div>