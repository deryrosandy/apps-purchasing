<div class="row">
  
  <div class="col-md-10">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#users" data-toggle="tab">Users</a></li>
        <li><a href="#settings" data-toggle="tab">Settings</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="users">
            
          <div class="form-horizontal table-responsive">
            <table id="" class="tabel-report table table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode</th>
                  <th>Username</th>
                  <th>Nama Karyawan</th>
                  <th>level</th>
                  <th>Status</th>
                  <th>Branch</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="list_data_therapist">
                <?php $no = 1; ?>
                <?php foreach($users as $user): ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $user->kodekar; ?></td>
                    <td><?php echo $user->username; ?></td>
                    <td><?php echo $user->fullname; ?></td>
                    <td><?php echo $user->level; ?></td>
                    <td><?php echo $user->status; ?></td>
                    <td><?php echo $user->branch_id; ?></td>
                    <td>
                      <div class="btn-group" style="display: -webkit-inline-box">
                        <button class="btn btn-xs btn-primary view_therapist" kodekar="<?php echo $user->kodekar; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</button>
                      </div>
                    </td>
                  </tr>
                  <?php $no++; ?>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>

        </div>
        <div class="tab-pane" id="settings">
          <form class="form-horizontal" action="<?php echo base_url('profile/change_password') ?>" method="POST">
            <div class="form-group">
              <label for="passLama" class="col-sm-3 control-label">Password Lama</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-3 control-label">Password Baru</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-3 control-label">Konfirmasi Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="msg" style="">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>

  </div>
</div>