<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#">Sistem Informasi Permintaan Pembelian Kebutuhan Kantor</a>.</strong> All rights reserved.
</footer>