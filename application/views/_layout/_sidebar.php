<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
	
	 <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/img/profil1.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->nama_lengkap; ?></p>
        <!-- Status -->
        <a href="<?php echo base_url(); ?>assets/#"><i class="fa fa-dot-circle-o text-success"></i> Online</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
		<li class="header">Main Menu</li>
		<!-- Optionally, you can add icons to the links -->

		<li <?php if ($page == 'dashboard') {echo 'class="active"';} ?>>
			<a href="<?php echo base_url('dashboard'); ?>">
			<i class="fa fa-home"></i>
			<span>Dashboard</span>
			</a>
		</li>
		<li class="treeview <?php if ($page == 'request_list' || $page == 'input_request'|| $page == 'request_for_po'|| $page == 'my_request' || $page == 'request_for_approved') {echo 'active';} ?>"><a href="<?php echo site_url()?>#"> <i class="fa fa-envelope"></i>
			<span>Request</span> <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li class="<?php if ($page == 'input_request') {echo 'active';} ?>">
					<a href="<?php echo site_url()?>purchase/input_request"><i class="fa fa-dot-circle-o"></i></i>
						New Request
					</a>
				</li>
				<li class="<?php if ($page == 'request_list') {echo 'active';} ?>">
					<a href="<?php echo site_url()?>purchase/index"><i class="fa fa-dot-circle-o"></i></i>
						Request List
					</a>
				</li>
				<li class="<?php if ($page == 'my_request') {echo 'active';} ?>">
					<a href="<?php echo site_url()?>purchase/my_request"><i class="fa fa-dot-circle-o"></i></i>
						My Request
					</a>
				</li>

				<?php if($this->userdata->akses_level !== 'staff'){ ?>
					<li class="<?php if ($page == 'request_for_approved') {echo 'active';} ?>">
						<a href="<?php echo site_url()?>purchase/request_for_approved"><i class="fa fa-dot-circle-o"></i></i>
							Request For Approved
						</a>
					</li>
				<?php } ?>
				<?php if($this->userdata->akses_level == 'purchase' || $this->userdata->akses_level == 'kabag_purchase'){ ?>
					<li class="<?php if ($page == 'request_for_po') {echo 'active';} ?>">
						<a href="<?php echo site_url()?>purchase/request_for_po"><i class="fa fa-dot-circle-o"></i></i>
							Request For PO
						</a>
					</li>
				<?php } ?>
			</ul>
		</li>
		
		<?php if($this->userdata->akses_level !== 'kabag' && $this->userdata->akses_level !== 'staff'){ ?>

			<li <?php if ($page == 'purchase_order') {echo 'class="active"';} ?>>
				<a href="<?php echo base_url('purchase/purchase_order'); ?>">
				<i class="fa fa-shopping-cart"></i>
				<span>Purchase Order</span>
				</a>
			</li>

		<?php } ?>

		<li class="treeview <?php if ($page == 'po_report' || $page == 'request_report') {echo 'active';} ?>"><a href="<?php echo site_url()?>#"> <i class="fa fa-print"></i>
			<span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
			<ul class="treeview-menu">
				<li  class="<?php if ($page == 'request_report') {echo 'active';} ?>">
					<a href="<?php echo site_url()?>report/request_report"><i class="fa fa-dot-circle-o"></i></i>
						Request Report
					</a>
				</li>
				<li  class="<?php if ($page == 'po_report') {echo 'active';} ?>">
					<a href="<?php echo site_url()?>report/po_report"><i class="fa fa-dot-circle-o"></i></i>
						PO Report
					</a>
				</li>
			</ul>
		</li>
		
		<?php if($this->userdata->akses_level == 'admin'){ ?>

			<li class="treeview <?php echo ($page == 'vendor' || $page == 'user' || $page == 'satuan' || $page == 'divisi' ? 'active': ""); ?>">
				<a href="<?php echo site_url()?>master/"> <i class="fa fa-database"></i>
				<span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">					
					<li class="<?php echo ($page == 'user' ? 'active': ""); ?>">
						<a href="<?php echo site_url()?>master/data_users"><i class="fa fa-dot-circle-o"></i></i>
							Users
						</a>
					</li>
					<li class="<?php echo ($page == 'divisi' ? 'active': ""); ?>">
						<a href="<?php echo site_url()?>master/data_divisi"><i class="fa fa-dot-circle-o"></i></i>
							Divisi
						</a>
					</li>
					<li class="<?php echo ($page == 'satuan' ? 'active': ""); ?>">
						<a href="<?php echo site_url()?>master/data_satuan"><i class="fa fa-dot-circle-o"></i></i>
							Satuan
						</a>
					</li>
				</ul>
			</li>
		
		<?php }elseif($this->userdata->akses_level !== 'kabag' && $this->userdata->akses_level !== 'staff'){ ?>

			<li class="treeview <?php echo ($page == 'vendor' || $page == 'user' || $page == 'satuan' || $page == 'divisi' ? 'active': ""); ?>">
				<a href="<?php echo site_url()?>master/"> <i class="fa fa-database"></i>
				<span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li class="<?php echo ($page == 'divisi' ? 'active': ""); ?>">
						<a href="<?php echo site_url()?>master/data_divisi"><i class="fa fa-dot-circle-o"></i></i>
							Divisi
						</a>
					</li>
					<li class="<?php echo ($page == 'satuan' ? 'active': ""); ?>">
						<a href="<?php echo site_url()?>master/data_satuan"><i class="fa fa-dot-circle-o"></i></i>
							Satuan
						</a>
					</li>
				</ul>
			</li>

		<?php } ?>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>