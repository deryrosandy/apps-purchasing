<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.6 -->
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/moment/moment.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- Morris.js charts -->
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/plugins/morris.js/morris.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/rwd-table.js"></script>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/dist/js/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/validate/jquery.validate.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/validate/localization/messages_id.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.maskMoney.js" charset="UTF-8"></script>

<!-- AdminLTE App -->

<!-- My Script -->
<?php include './assets/js/script.php'; ?>
