<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="<?php echo base_url(); ?>assets/#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <p class="top_date"><?php echo getHari(date("Y/m/d")); ?>, <?php echo tgl_indo(date("Y/m/d")); ?></p>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- User Account Menu -->
      <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="<?php echo base_url(); ?>assets/#" class="dropdown-toggle" data-toggle="dropdown">
          <!-- The user image in the navbar-->
          <img src="<?php echo base_url(); ?>assets/img/profil1.jpg" class="user-image" alt="User Image">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span class="hidden-xs"><?php echo $userdata->nama_lengkap; ?>&nbsp;&nbsp;</span>
          <i class="fa fa-angle-down"></i>
        </a>
        
        <ul class="dropdown-menu">
          <!-- The user image in the menu -->
          <li class="user-header">
            <img src="<?php echo base_url(); ?>assets/img/profil1.jpg" class="img-circle" alt="User Image">
            <p>
              <?php echo $userdata->nama_lengkap; ?>
            </p>            
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <?php if($userdata->akses_level == 'vendor'){ ?>
                <a href="<?php echo base_url('profile_vendor'); ?>" class="btn btn-default btn-flat">Profile</a>
              <?php }else{ ?>
                <a href="<?php echo base_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
              <?php } ?>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url('auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>