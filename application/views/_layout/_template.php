<!DOCTYPE html>
<html>
  <head>
  
    <?php /*<title><?php echo $this->general->getRestoName(); ?> | <?php echo ucwords(@$page ? $page:''); ?> - <?php echo ucwords(@$deskripsi ? $deskripsi:''); ?></title> */ ?>
    <title><?php echo $judul; ?> - Sistem Informasi Permintaan Pembelian Kebutuhan Kantor</title>
    <!-- meta -->
    <?php echo @$_meta; ?>

    <!-- css --> 
    <?php echo @$_css; ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
  </head>

  <body class="hold-transition skin-blue sidebar-mini wysihtml5-supported">
    <div class="wrapper">
      <!-- header -->
      <?php echo @$_header; ?> <!-- nav -->
      
      <!-- sidebar -->
      <?php echo @$_sidebar; ?>
      
      <!-- content -->
      <?php echo @$_content; ?> <!-- headerContent --><!-- mainContent -->
    
      <!-- footer -->
      <?php echo @$_footer; ?>
    
      <div class="control-sidebar-bg"></div>
    </div>

    <!-- js -->
    <?php echo @$_js; ?>
	
  </body>
</html>