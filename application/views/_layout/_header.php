<header class="main-header">
	<!-- Logo -->
	<a href="<?php echo base_url(); ?>" class="logo navbar-static-top">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini"><small>SP</small></span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg"><b>E-Purchasing</b></span>
	</a>
	<!-- nav -->
	<?php echo @$_nav; ?>
</header>