<div class="row">
  <?php /*
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
		<h3 class="profile-username text-center"><?php echo $profile->nama_lengkap; ?></h3>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Username</b> <span class="pull-right"><?php echo $profile->username; ?></span>
          </li>
          <li class="list-group-item">
            <b>Nama Lengkap</b> <span class="pull-right"><?php echo $profile->nama_lengkap; ?></span>
          </li>
          <li class="list-group-item">
            <b>Email</b> <span class="pull-right"><?php echo $profile->email; ?></span>
          </li>
          <!--
          <li class="list-group-item">
            <b>Divisi</b> <a class="pull-right"></a>
          </li>
          -->
        </ul>
      </div>
    </div>
  </div>
  */ ?>

  <div class="col-md-7">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Profile</a></li>
        <li><a href="#password" data-toggle="tab">Ubah Password</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="settings">
			<form class="form-horizontal" action="<?php echo base_url('profile/update') ?>" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="inputUsername" class="col-sm-2 control-label">Username</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" disabled id="username" placeholder="Username" name="username" value="<?php echo $profile->username; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="fullname" class="col-sm-2 control-label">Nama Lengkap</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" value="<?php echo $profile->nama_lengkap; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="fullname" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $profile->email; ?>">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
        </div>
        <div class="tab-pane" id="password">
          <form class="form-horizontal" action="<?php echo base_url('profile/change_password') ?>" method="POST">
            <div class="form-group">
              <label for="passLama" class="col-sm-3 control-label">Password Lama</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-3 control-label">Password Baru</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-3 control-label">Konfirmasi Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="msg" style="">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>

  </div>
</div>