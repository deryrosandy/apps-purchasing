<div class="row">
    <div class="col-md-12">
        <div class="msg" style="">
            <?php echo @$this->session->flashdata('msg'); ?>
        </div>
        <div class="pull-right">
            <a style="margin-bottom: 15px;" class="btn btn-md btn-success" href="<?php echo base_url('master/create_vendor'); ?>">
                <i class="fa fa-plus"></i> Tambah Vendor
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $deskripsi; ?>
            </div>
            <div class="panel-body">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="active tab-pane">
                            <div class="form-horizontal table-stripe">
                                <table id="data-divisi" class="table-report table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Vendor</th>
                                            <th>Telp</th>
                                            <th>Email</th>
                                            <th>Nama PIC</th>
                                            <th>No. HP PIC</th>
                                            <th>Alamat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; ?>
                                        <?php foreach($data_vendor as $vendor): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $vendor->name; ?></td>
                                                <td><?php echo $vendor->no_telp; ?></td>
                                                <td><?php echo $vendor->email; ?></td>
                                                <td><?php echo $vendor->nama_pic; ?></td>
                                                <td><?php echo $vendor->no_telp_pic; ?></td>
                                                <td><?php echo $vendor->alamat; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>master/edit_vendor/<?php echo $vendor->id; ?>"  title="Edit"  class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-danger delete"  title="Delete"  data-id="<?php echo $vendor->id; ?>" data-url="<?php echo site_url('master/delete_vendor')?>">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php $no++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>