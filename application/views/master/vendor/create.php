<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Form Tambah Vendor</h3>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-1">
                    <form action="<?php echo base_url(); ?>master/tambah_vendor" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Nama Vendor</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="name" required autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">No. Telp</label>
                                <div class="col-sm-9">
                                    <input type='number' class="form-control" name="no_telp" required autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type='email' class="form-control" name="email" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Nama PIC</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="nama_pic" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">No. Telp PIC</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="no_telp_pic"  autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-9">
                                    <textarea name="alamat" id="" cols="30" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?php echo base_url('master/data_vendor'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
							<button type="submit" id="btn_submit_perijinan" class="btn btn-primary pull-right"><i class="fa fa-save"></i>  Simpan</button>
							<div>&nbsp;</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>