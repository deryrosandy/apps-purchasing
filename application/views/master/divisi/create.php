<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Form Tambah Divisi</h3>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form action="<?php echo base_url(); ?>master/tambah_divisi" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Nama Divisi</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="name" required autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Deskripsi Divisi</label>
                                <div class="col-sm-9">
                                    <textarea name="description" id="" cols="30" rows="10" class="form-control" require></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?php echo base_url('master/data_divisi'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
							<button type="submit" id="btn_submit_perijinan" class="btn btn-primary pull-right"><i class="fa fa-save"></i>  Simpan</button>
							<div>&nbsp;</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>