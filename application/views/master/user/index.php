<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <a style="margin-bottom: 15px;" class="btn btn-md btn-success" href="<?php echo base_url('master/create_user'); ?>">
                <i class="fa fa-plus"></i> Tambah User
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $deskripsi; ?>
            </div>
            <div class="panel-body">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="active tab-pane">
                            <div class="form-horizontal table-stripe">
                                <table id="data-divisi" class="table-report table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Username</th>
                                            <th>Nama Lengkap</th>
                                            <th>Email</th>
                                            <th>Divisi</th>
                                            <th>Level</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; ?>
                                        <?php foreach($users as $user): ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $user->username; ?></td>
                                                <td><?php echo $user->nama_lengkap; ?></td>
                                                <td><?php echo $user->email; ?></td>
                                                <td><?php echo get_divisi_name($user->divisi_id); ?></td>
                                                <td><?php echo $user->akses_level; ?></td>
                                                <td><?php echo status_active($user->status); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>master/edit_user/<?php echo $user->id; ?>"  title="Edit"  class="btn btn-xs btn-primary">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                    <button class="btn btn-xs btn-danger delete"  title="Delete"  data-id="<?php echo $user->id; ?>" data-url="<?php echo site_url('master/delete_user')?>">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php $no++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>