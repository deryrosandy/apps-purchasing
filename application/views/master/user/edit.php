<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Form Ubah User</h3>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-1">
                    <form action="<?php echo base_url("master/update_user/".$user->id); ?>" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Nama Lengkap</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="nama_lengkap" required placeholder="Nama Lengkap" autocomplete="off"  value="<?php echo $user->nama_lengkap; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type='email' class="form-control" name="email" required placeholder="Email" autocomplete="off"  value="<?php echo $user->email; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="username" required  placeholder="Username" autocomplete="off"  value="<?php echo $user->username; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Akses Level</label>
                                <div class="col-sm-9">
                                    <select name="akses_level" class="form-control select2" placeholder="Pilih Level" required>
										<option value="">- Pilih Level -</option>
											<option value="admin"  <?php echo ($user->akses_level== 'admin' ? 'selected' : ''); ?>>Admin</option>
											<option value="staff" <?php echo ($user->akses_level== 'staff' ? 'selected' : ''); ?>>Staff</option>
											<option value="kabag" <?php echo ($user->akses_level== 'kabag' ? 'selected' : ''); ?>>Kabag</option>
											<option value="purchase" <?php echo ($user->akses_level== 'purchase' ? 'selected' : ''); ?>>Purchase</option>
											<option value="kabag_purchase" <?php echo ($user->akses_level== 'kabag_purchase' ? 'selected' : ''); ?>>Kabag Purchase</option>
											<option value="directur" <?php echo ($user->akses_level== 'directur' ? 'selected' : ''); ?>>Direktur</option>
									</select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Divisi</label>
                                <div class="col-sm-9">
                                    <select name="divisi_id" class="form-control select2" placeholder="Pilih Divisi" required>
										<option value="">- Pilih Divisi -</option>
										
										<?php foreach($data_divisi as $divisi){ ?>
											<option value="<?php echo $divisi->id; ?>" <?php echo ($user->divisi_id==$divisi->id ? 'selected' : ''); ?>><?php echo $divisi->name; ?></option>
										<?php } ?>
									</select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Pasword</label>
                                <div class="col-sm-9">
                                    <input type='password' class="form-control" name="password" placeholder="Kosongkan Jika Password Tidak Di Ganti" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" for="branch_id">Status :</label>
                                <div class="col-md-6 col-xs-12">
                                <label>
                                    <input type="checkbox" name="status" class="minimal" <?php echo ($user->status== 1 ? 'checked' : ''); ?> />
                                </label>
                                </div>							
                            </div>	
                        </div>
                        <div class="box-footer">
                            <a href="<?php echo base_url('master/data_user'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
							<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i>  Simpan</button>
							<div>&nbsp;</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>