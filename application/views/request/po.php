<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $deskripsi; ?>
            </div>
            <div class="panel-body">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="active tab-pane">
                            <table id="list-pembelian" class="table-report table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No. PO</th>
                                        <th>Name</th>
                                        <th>Divisi</th>
                                        <th>QTY</th>
                                        <th>Nominal</th>
                                        <th>Spesifikasi</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach($purchase as $row): ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $row->no_po; ?></td>
                                            <td><?php echo $row->name; ?></td>
                                            <td><?php echo $row->nama_divisi; ?></td>
                                            <td><?php echo $row->qty; ?> <?php echo get_satuan_name($row->satuan_id); ?></td>
                                            <td><?php echo format_rupiah($row->nominal); ?></td>
                                            <td><?php echo $row->spesifikasi; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($row->date)); ?></td>
                                            <td><?php echo status_request($row->status); ?></td>
                                            <td>
                                                <div class="btn-group" style="display: -webkit-inline-box">	
                                                   <a href="<?php echo base_url(); ?>purchase/show/<?php echo $row->id; ?>" title="View Detail" class="btn btn-sm btn-info" data-id="<?php echo $row->id; ?>"><i class="fa fa-file-text-o " aria-hidden="true"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>