<div class="row">
	<div class="col-md-8">
		<div class="msg">
			<?php echo @$this->session->flashdata('msg'); ?>
		</div>
		<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Permintaan</h3>
            </div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<div class="col-md-12 col-md-offset-0">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<td id="no">&nbsp;</span></td>
								</tr>
								<tr>
									<td><strong>No. Permintaan</span></strong></td>
									<td><?php echo $pembelian->no_request; ?></span></td>
								</tr>
								<tr>
									<td><strong>Nama Permintaan</span></strong></td>
									<td><?php echo $pembelian->name; ?></span></td>
								</tr>
								<tr>
									<td><strong>Qty</span></strong></td>
									<td><?php echo $pembelian->qty; ?> <?php echo get_satuan_name($pembelian->satuan_id) ?></td>
								</tr>
								<tr>
									<td><strong>Tgl Permintaan</span></strong></td>
									<td><?php echo date('d-m-Y', strtotime($pembelian->created_at)); ?></td>
								</tr>
								<tr>
									<td><strong>Perkiraan Biaya</span></strong></td>
									<td><?php echo format_rupiah($pembelian->nominal); ?></td>
								</tr>
								<tr>
									<td><strong>Diminta Oleh</span></strong></td>
									<td><?php echo $pembelian->nama_lengkap; ?></td>
								</tr>
								<tr>
									<td><strong>Divisi</span></strong></td>
									<td><?php echo $pembelian->nama_divisi; ?></td>
								</tr>
								<tr>
									<td><strong>Tgl Kebutuhan</span></strong></td>
									<td><?php echo date('d-m-Y', strtotime($pembelian->date)); ?></td>
								</tr>
								<tr>
									<td><strong>Spesifikasi</span></strong></td>
									<td>
                                        <?php echo ($pembelian->spesifikasi) ?>
                                    </td>
								</tr>
                                <tr>
                                    <td><strong>Deskripsi</strong></td>
                                    <td>
                                        <?php echo $pembelian->description ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Status</strong></td>
                                    <td>
                                        <?php echo status_request($pembelian->status) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Posisi Permintaan</strong></td>
                                    <td>
                                        <?php echo posisi_request($pembelian->posisi_request) ?>
                                    </td>
                                </tr>
							</tbody>
						</table>
         			</div>
         		</div>
			</div>		
		</div>
	</div>
</div>
<?php if($this->userdata->akses_level != 'staff'): ?>
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">

			<?php if((($pembelian->posisi_request ==1) && $this->userdata->akses_level == 'kabag') || (($pembelian->posisi_request ==2) && $this->userdata->akses_level == 'purchase') || (($pembelian->posisi_request ==3) && $this->userdata->akses_level == 'kabag_purchase') || (($pembelian->posisi_request ==4) && $this->userdata->akses_level == 'directur' && $pembelian->is_dir==1)): ?>
				<div class="box-header with-border">
				<h3 class="box-title">Aprove Permintaan Pembelian</h3>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-offset-0">						
						<div class="col-md-12 col-md-offset-0">
							<form action="<?php echo base_url(); ?>purchase/aprove/" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
								<div class="box-body">
									<div class="form-group">
										<input type="hidden" value="<?php echo $pembelian->id; ?>" name="request_id">
										<input type="hidden" value="<?php echo $pembelian->is_dir; ?>" name="is_dir">
										<input type="hidden" value="<?php echo intval($pembelian->posisi_request)+1; ?>" name="posisi_request">
										<label for="dari_tanggal" class="col-sm-3 control-label">Persetujuan</label>
										<div class="col-sm-9">
											<select name="status" class="form-control">
												<option value="approved">Di Setujui</option>
												<option value="rejected">Di Tolak</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="dari_tanggal" class="col-sm-3 control-label">Note / Deskripsi</label>
										<div class="col-sm-9">
											<textarea name="description" id="" cols="30" rows="4" class="form-control"></textarea>
										</div>
									</div>
									<div class="box-footer">
										<button type="button" onclick="history.back(-1)" class="btn btn-default">Kembali</button>
										<button type="submit" class="btn btn-primary pull-right">Simpan</button>
										<div>&nbsp;</div>
									</div>
								</div>
							</form>
						</div>						
					</div>
				</div>
			<?php else: ?>
				<!-- <div class="box-header with-border">
					<h3 class="box-title">Aprove Permintaan Pembelian</h3>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-offset-0">						
						<div class="col-md-12 col-md-offset-0">
							<div class="col-md-12 col-md-offset-0">
								<form action="<?php echo base_url(); ?>purchase/aprove_update/" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
									<div class="box-body">
										<div class="form-group">
											<input type="hidden" value="<?php echo $pembelian->id; ?>" name="id">
											<input type="hidden" value="<?php echo $pembelian->id; ?>" name="request_id">
											<label for="dari_tanggal" class="col-sm-3 control-label">Persetujuan</label>
											<div class="col-sm-9">
												<select name="status" class="form-control">
													<option value="approved">Di Setujui</option>
													<option value="rejected">Di Tolak</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="dari_tanggal" class="col-sm-3 control-label">Note / Deskripsi</label>
											<div class="col-sm-9">
												<textarea name="desc" id="" cols="30" rows="4" class="form-control" require><?php echo $pembelian->description; ?></textarea>
											</div>
										</div>
										<div class="box-footer">
											<button type="button" onclick="history.back(-1)" class="btn btn-default">Kembali</button>
											<button type="submit" id="btn_submit_perijinan" class="btn btn-primary pull-right">Simpan</button>
											<div>&nbsp;</div>
										</div>
									</div>
								</form>
							</div>
						</div>						
					</div>					
				</div> -->
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>