<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $deskripsi; ?>
            </div>
            <div class="panel-body">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="active tab-pane">
                            <table id="list-pembelian" class="table-report table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No. Request</th>
                                        <th>Name</th>
                                        <th>Divisi</th>
                                        <th>QTY</th>
                                        <th>Perkiraan Biaya</th>
                                        <th>Spesifikasi</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach($purchase as $row): ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $row->no_request; ?></td>
                                            <td><?php echo $row->name; ?></td>
                                            <td><?php echo $row->nama_divisi; ?></td>
                                            <td><?php echo $row->qty; ?></td>
                                            <td><?php echo format_rupiah($row->nominal); ?></td>
                                            <td><?php echo $row->spesifikasi; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($row->date)); ?></td>
                                            <td><?php echo status_request($row->status); ?></td>
                                            <td>
                                                <div class="btn-group" style="display: -webkit-inline-box">	
                                                    <?php if($row->is_po == '1'):?>
                                                        <button class="btn btn-sm btn-success confirm"  title="Proses PO"  data-id="<?php echo $row->id; ?>" data-url="<?php echo site_url('purchase/process_po')?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                                                    <?php endif; ?>						
                                                    <a href="<?php echo base_url(); ?>purchase/cetak/<?php echo $row->id; ?>" target="_blank" title="Cetak Permintaan" class="btn btn-sm btn-warning" data-id="<?php echo $row->id; ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                                                    <a href="<?php echo base_url(); ?>purchase/show/<?php echo $row->id; ?>" title="View Detail" class="btn btn-sm btn-info" data-id="<?php echo $row->id; ?>"><i class="fa fa-file-text-o " aria-hidden="true"></i></a>
                                                    <?php if($row->posisi_request == '1'):?>
                                                        <a href="<?php echo base_url(); ?>purchase/edit/<?php echo $row->id; ?>"  title="Edit"  class="btn btn-sm btn-primary" data-id="<?php echo $row->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                        <button class="btn btn-sm btn-danger delete"  title="Delete"  data-id="<?php echo $row->id; ?>" data-url="<?php echo site_url('purchase/delete')?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    <?php endif; ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>