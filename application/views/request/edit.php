<div class="msg" style="display:none;">
    <?php echo @$this->session->flashdata('msg'); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Pengajuan Pembelian</h3>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-1">
                    <form action="<?php echo base_url('purchase/update/'.$permintaan->id); ?>" class="form-horizontal" id="" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Nama Barang</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="name" required autocomplete="off" value="<?php echo $permintaan->name; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Spesifikasi</label>
                                <div class="col-sm-9">
                                    <textarea name="spesifikasi" id="" cols="30" rows="4" class="form-control" require><?php echo $permintaan->spesifikasi; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Perkiraan Biaya</label>
                                <div class="col-sm-9">
                                    <input type='text' class="form-control" name="nominal" value="<?php echo $permintaan->nominal; ?>" required autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">QTY</label>
                                <div class="col-sm-9">
                                    <input type='number' class="form-control" name="qty" required value="<?php echo $permintaan->qty; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Satuan</label>
                                <div class="col-sm-9">
                                    <select name="satuan_id" class="form-control">
                                        <option value="1" <?php echo $permintaan->satuan_id == '1' ? 'selected':''; ?>>Pcs</option>
                                        <option value="2" <?php echo $permintaan->satuan_id == '2' ? 'selected':''; ?>>Set</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
								<label for="dari_tanggal" class="col-sm-3 control-label">Tanggal Diperlukan</label>
								<div class="col-sm-9">
									<div class="input-group date datetimepicker" data-date-format="yyyy-mm-dd">
										<input type='text' class="form-control" name="date" required autocomplete="off" value="<?php echo $permintaan->date; ?>"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
                            <div class="form-group">
                                <label for="dari_tanggal" class="col-sm-3 control-label">Deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea name="description" id="" cols="30" rows="7" class="form-control" require><?php echo $permintaan->description ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="<?php echo base_url(); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>
							<button type="submit" id="btn_submit_perijinan" class="btn btn-primary pull-right"><i class="fa fa-save"></i>  Simpan</button>
							<div>&nbsp;</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>