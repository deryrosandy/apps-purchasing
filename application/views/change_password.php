<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
		<h3 class="profile-username text-center"><?php echo $userdata->fullname; ?></h3>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Username</b> <a class="pull-right"><?php echo $userdata->username; ?></a>
          </li>
          <li class="list-group-item">
            <b>Divisi</b> <a class="pull-right"></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  
  <div class="col-md-7">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Profile</a></li>
        <li><a href="#password" data-toggle="tab">Ubah Password</a></li>
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="settings">
			<form class="form-horizontal" action="<?php echo base_url('profile/update') ?>" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="inputUsername" class="col-sm-3 control-label">Username</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" disabled id= placeholder="Username" name="UserID" value="<?php echo $userdata->username; ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputNama" class="col-sm-3 control-label">Full Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" placeholder="Name" name="Nama" value="<?php echo $userdata->fullname; ?>">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
        </div>
        <div class="tab-pane" id="password">
          <form class="form-horizontal" action="<?php echo base_url('profile/change_password') ?>" method="POST">
            <div class="form-group">
              <label for="passLama" class="col-sm-3 control-label">Password Lama</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-3 control-label">Password Baru</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-3 control-label">Konfirmasi Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="msg" style="">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>

  </div>
</div>