<div class="row  gridlist-dashboard">
	
<?php if($this->userdata->akses_level == 'staff' || $this->userdata->akses_level == 'kabag') { ?>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
            <h3><?php echo $total_request; ?></h3>
                <p>Total Permintaan</p>
            </div>
            <div class="icon">
            <i class="fa fa-envelope-o"></i>
            </div>
            <a href="<?php echo base_url(); ?>purchase/index" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

<?php }else{ ?>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
            <h3><?php echo $total_request; ?></h3>
                <p>Total Permintaan</p>
            </div>
            <div class="icon">
            <i class="fa fa-envelope-o"></i>
            </div>
            <a href="<?php echo base_url(); ?>purchase/index" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3><?php echo $total_for_approved; ?></h3>
                <p>Total For Approved</p>
            </div>
            <div class="icon">
            <i class="fa fa-check-circle-o"></i>
            </div>
            <a href="<?php echo base_url(); ?>purchase/request_for_approved" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="small-box bg-olive">
            <div class="inner">
            <h3><?php echo $total_po; ?></h3>
                <p>Total PO</p>
            </div>
            <div class="icon">
            <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="<?php echo base_url(); ?>purchase/master/purchase_order" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="small-box bg-primary">
            <div class="inner">
            <h3><?php echo $total_satuan; ?></h3>
                <p>Total Satuan</p>
            </div>
            <div class="icon">
            <i class="fa fa-cubes"></i>
            </div>
            <a href="<?php echo base_url(); ?>master/data_satuan" class="small-box-footer">Buka Data <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

<?php } ?>

</div>