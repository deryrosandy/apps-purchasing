<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_Admin', 'M_General'));
	}

	public function index() {

		$this->data_users();
	}

	public function create_vendor() {

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "create_vendor";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Tambah Vendor";
		
		$this->template->views('master/vendor/create', $data);
	}

	public function create_satuan() {

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "create_satuan";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Tambah Satuan";
		
		$this->template->views('master/satuan/create', $data);
	}


	public function create_divisi() {

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "divisi";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Tambah Divisi";
		
		$this->template->views('master/divisi/create', $data);
	}

	public function create_user() {

		$data['userdata'] 		= $this->userdata;

		$data['page'] 			= "user";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Tambah User";
		$data['data_divisi'] = $this->M_General->get_all_divisi();
		
		$this->template->views('master/user/create', $data);
	}


	public function data_users() {

		$data['userdata'] 		= $this->userdata;	

		$data['users'] = $this->M_General->get_all_user();

		$data['page'] 			= "user";
		$data['judul'] 			= "Master";
		$data['deskripsi'] 		= "Data User Pengguna";
		
		$this->template->views('master/user/index', $data);
	
	}

	public function data_satuan() {

		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "satuan";
		$data['judul'] 			= "Satuan";
		$data['deskripsi'] 		= "";

		$data['data_satuan'] = $this->M_General->get_all_satuan();
		
		$this->template->views('master/satuan/index', $data);
	}
	public function data_divisi() {

		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "divisi";
		$data['judul'] 			= "Satuan";
		$data['deskripsi'] 		= "";

		$data['data_divisi'] = $this->M_General->get_all_divisi();
		
		$this->template->views('master/divisi/index', $data);
	}
	
	public function data_vendor(){

		$data['userdata'] 		= $this->userdata;	

		$data['page'] 			= "vendor";
		$data['judul'] 			= "Vendor";
		$data['deskripsi'] 		= "";

		$data['data_vendor'] = $this->M_General->get_all_vendor();
		
		$this->template->views('master/vendor/index', $data);
	}

	public function get_satuan_by_id(){
		
		$satuan_id 	= $this->input->post('satuan_id');
		
		$data = $this->M_General->get_satuan_by_id($satuan_id);
		
		echo json_encode($data);

	}

	public function get_vendor_by_id(){
		
		$vendor_id 	= $this->input->post('vendor_id');
		
		$data = $this->M_General->get_vendor_by_id($vendor_id);
		
		echo json_encode($data);

	}

	public function edit_satuan($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "satuan";
        $data['judul'] = "Master";
        $data['deskripsi'] = "Edit Satuan";
        $data['satuan'] = $this->M_General->get_satuan_by_id($id);

        $this->template->views('master/satuan/edit', $data);
    }

	public function edit_divisi($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "divisi";
        $data['judul'] = "Master";
        $data['deskripsi'] = "Edit Satuan";
        $data['divisi'] = $this->M_General->get_divisi_by_id($id);

        $this->template->views('master/divisi/edit', $data);
    }

	public function edit_vendor($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "vendor";
        $data['judul'] = "Master";
        $data['deskripsi'] = "Edit Vendor";
        $data['vendor'] = $this->M_General->get_vendor_by_id($id);

        $this->template->views('master/vendor/edit', $data);
    }

	public function edit_user($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = "user";
        $data['judul'] = "Master";
        $data['deskripsi'] = "Edit User";
        $data['user'] = $this->M_General->get_user_by_id($id);
        $data['data_divisi'] = $this->M_General->get_all_divisi();

        $this->template->views('master/user/edit', $data);
    }

	public function list_data_users() {

		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

		/*Menghitung total desa didalam database*/
		$total=$this->db->count_all_results("users");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
			$this->db->or_like("nama_lengkap",$search);
			$this->db->or_like("username",$search);
			$this->db->or_like("akses_level",$search);
			$this->db->or_like("email",$search);
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('user_id','DESC');
		
		$query=$this->db->get('users');
		
		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("nama_lengkap",$search);
		$jum=$this->db->get('users');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		$no=$start+1;
		foreach ($query->result_array() as $users) {
			$output['data'][]=array(
				$no,
				$users['username'],
				$users['nama_lengkap'],
				$users['email'],
				//get_level_user($users['jabatan_id']),
				get_divisi_name($users['divisi_id']),
				get_status_name($users['status']),
				'<div class="btn-group" style="display: -webkit-inline-box">
				<button class="btn btn-xs btn-primary" id="view_users" title="Edit" user_id="' . $users["user_id"] . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
				<button class="btn btn-xs btn-danger delete_user" id="" title="Delete" user_id="' . $users["user_id"] . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
			  </div>',
			);
		$no++;
		}
		
		echo json_encode($output);
	}


	public function tambah_customer() {

		$data = $this->input->post();	
		unset($data['operation']);
		$result = $this->M_General->insert_customer($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Customer Berhasil ditambah'));
			redirect('master/data_customer');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Customer Gagal ditambah'));
			redirect('master/data_customer');
		}
	}

	public function tambah_vendor() {

		$data = $this->input->post();	
		unset($data['operation']);
		$result = $this->M_General->insert_vendor($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Vendor Berhasil ditambah'));
			redirect('master/data_vendor');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Vendor Gagal ditambah'));
			redirect('master/data_vendor');
		}
	}

	public function tambah_satuan() {

		$data = $this->input->post();	
		$result = $this->M_General->insert_satuan($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Satuan Berhasil ditambah'));
			redirect('master/data_satuan');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Satuan Gagal ditambah'));
			redirect('master/data_satuan');
		}
	}

	public function tambah_divisi() {

		$data = $this->input->post();	

		$result = $this->M_General->insert_divisi($data);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Divisi Berhasil ditambah'));
			redirect('master/data_divisi');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Divisi Gagal ditambah'));
			redirect('master/data_divisi');
		}
	}

	public function delete_vendor() {

		$vendor_id = $this->input->post('id');

		$result = $this->M_General->delete_vendor($vendor_id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Vendor Berhasil di Hapus'));
			redirect('master/data_vendor');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Vendor Gagal Di Hapus'));
			redirect('master/data_vendor');
		}
	}

	public function delete_satuan() {

		$vendor_id = $this->input->post('id');

		$result = $this->M_General->delete_satuan($vendor_id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Satuan Berhasil di Hapus'));
			redirect('master/data_satuan');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Satuan Gagal Di Hapus'));
			redirect('master/data_satuan');
		}
	}

	public function tambah_user() {

		$data = $this->input->post();	
		
		$data['status'] = ($data['status']=='on' ? '1':'0');
		$data['password'] = md5($this->input->post('password'));	

		$result = $this->M_General->insert_user($data);

		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil ditambah'));
			redirect('master/data_users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal ditambah'));
			redirect('master/data_users');
		}
		
	}

	public function delete_user() {

		$user_id = $this->input->post('id');

		$result = $this->M_General->delete_user($user_id);
		
		if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil Di Hapus'));
			redirect('master/data_users');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal Di Hapus'));
			redirect('master/data_users');
		}
	}

	public function update_user($id) {

		$data = $this->input->post();	
		if($data['password']==''){
			unset($data['password']);
		}else{
			$data['password'] = md5($this->input->post('password'));	
		}
		$data['status'] = (@$data['status']=='on' ? '1':'0');
		
		$result = $this->M_General->update_user($id, $data);

		if ($result == true) {	
			$this->session->set_flashdata('msg', show_succ_msg('User Berhasil diubah'));	
			redirect('master/data_users');
			
		} else {
			$this->session->set_flashdata('msg', show_err_msg('User Gagal diubah'));
			redirect('master/data_users');
			
		}
	}

	public function update_vendor($id) {

		$data = $this->input->post();
		
		$result = $this->M_General->update_vendor($id, $data);

		if ($result == true) {		
			$this->session->set_flashdata('msg', show_succ_msg('Vendor Berhasil diubah'));
			redirect('master/data_vendor');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Vendor Gagal diubah'));
			redirect('master/data_vendor');
		}
	}

	public function update_satuan($id) {

		$data = $this->input->post();
		$result = $this->M_General->update_satuan($id, $data);

		if ($result == true) {		
			$this->session->set_flashdata('msg', show_succ_msg('Satuan Berhasil diubah'));
			redirect('master/data_satuan');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Satuan Gagal diubah'));
			redirect('master/data_satuan');
		}
	}

	public function update_divisi($id) {

		$data = $this->input->post();
		$result = $this->M_General->update_divisi($id, $data);

		if ($result == true) {		
			$this->session->set_flashdata('msg', show_succ_msg('Divisi Berhasil diubah'));
			redirect('master/data_divisi');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Divisi Gagal diubah'));
			redirect('master/data_divisi');
		}
	}

	public function ganti_password() {
		$this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
		$this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
		$this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
		
		$UserID = $this->userdata->UserID;
		
		if ($this->form_validation->run() == TRUE) {
			if (($this->input->post('passLama')) == $this->userdata->Passwd) {
				if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
					$this->session->set_flashdata('msg', show_err_msg('Password Baru dan Konfirmasi Password harus sama'));
					redirect('profile');
				} else {
					$data = [
						'Passwd' => $this->input->post('passBaru')
					];
					
					$result = $this->M_admin->update($data, $UserID);
					
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Password Berhasil diubah'));
						redirect('profile');
					} else {
						$this->session->set_flashdata('msg', show_err_msg('Password Gagal diubah'));
						redirect('profile');
					}
				}
			} else {
				$this->session->set_flashdata('msg', show_err_msg('Password Salah'));
				redirect('profile');
			}
		} else {
			$this->session->set_flashdata('msg', show_err_msg(validation_errors()));
			redirect('profile');
		}
	}

}