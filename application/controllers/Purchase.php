<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends AUTH_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General', 'M_Purchase'));
	}

    public function input_request()
    {
        $userdata = $this->userdata;
        $data['userdata'] = $this->userdata;
        $data['page'] = 'input_request';
        $data['judul'] = 'Form Pengajuan Pembelian';
        $data['deskripsi'] = '';
        $this->template->views('request/input_request', $data);
    }

    public function store()
    {
        $userdata = $this->userdata;
        $data = $this->input->post();
        $data['divisi_id'] = $userdata->divisi_id;
        $data['created_by'] = $userdata->id;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['status'] = 'open';
        $data['posisi_request'] = '1';
        $data['no_request'] = $this->get_new_no_request();

        $this->M_General->store_purchase($data);
        header('Location: ../purchase/index');
    }

    
    public function get_new_no_request() {
        $sql = "SELECT no_request FROM request ORDER BY id DESC limit 1";
        
        $data = $this->db->query($sql);

        if($data->num_rows() > 0){
            $new_numb = (intval(substr($data->row('no_request'),11)) + 1);
        }else{
            $new_numb = '10001';
        }

        $tahun = (date('Y'));
        $bulan = (date('m'));
        
        $no_request = 'SPPB-' . $tahun . $bulan . $new_numb;
        return $no_request;
    }

    public function get_new_no_po() {
        
        $sql = "SELECT no_po FROM request
        WHERE no_po IS NOT NULL AND is_po=1
        ORDER BY id DESC limit 1";
        
        $data = $this->db->query($sql);

        if($data->num_rows() > 0){
            $new_numb = (intval(substr($data->row('no_po'),9)) + 1);
        }else{
            $new_numb = '10001';
        }

        $tahun = (date('Y'));
        $bulan = (date('m'));
        
        $no_po = 'PO-' . $tahun . $bulan . $new_numb;
        return $no_po;
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        if($this->userdata->akses_level == 'staff'){
            $data['purchase'] = $this->M_Purchase->get_by_user($this->userdata->id);
        }elseif($this->userdata->akses_level == 'kabag') {
            $data['purchase'] = $this->M_Purchase->get_by_divisi($this->userdata->divisi_id);
        }else {
            $data['purchase'] = $this->M_Purchase->get_all_purchase();
        }
        $data['page'] = 'request_list';
        $data['judul'] = 'Request List';
        $data['deskripsi'] = '';
        
        $this->template->views('request/index', $data);
    }

    public function request_for_approved()
    {
        $data['userdata'] = $this->userdata;
        if($this->userdata->akses_level == 'kabag'){
            $data['purchase'] = $this->M_Purchase->get_for_approved_by_divisi($this->userdata->divisi_id);                   
        }else{
            $data['purchase'] = $this->M_Purchase->get_for_approved_all_purchase($this->userdata->akses_level);
        }

        $data['page'] = 'request_for_approved';
        $data['judul'] = 'Request List';
        $data['deskripsi'] = '';
        
        $this->template->views('request/index', $data);
    }

    public function request_for_po()
    {
        $data['userdata'] = $this->userdata;
        $data['purchase'] = $this->M_Purchase->get_for_purchase_order();                   

        $data['page'] = 'request_for_po';
        $data['judul'] = 'Request List';
        $data['deskripsi'] = '';
        
        $this->template->views('request/index', $data);
    }

    public function my_request()
    {
        $data['userdata'] = $this->userdata;
        $data['purchase'] = $this->M_Purchase->get_by_user($this->userdata->id);

        $data['page'] = 'my_request';
        $data['judul'] = 'My Request';
        $data['deskripsi'] = '';
        
        $this->template->views('request/index', $data);
    }

    public function report()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'report_pembelian';
        $data['judul'] = 'Report Pembelian';
        $data['deskripsi'] = 'Report Pembelian';
        $this->template->views('laporan/data_pembelian', $data);
    }

    public function delete(){
        $id = $this->input->post('id');
        $this->M_Purchase->destroy($id);
    }

    public function edit($id)
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = 'request_list';
        $data['judul'] = 'Edit Permintaan';
        $data['deskripsi'] = 'Edit Permintaan';
        $data['permintaan'] = $this->M_Purchase->find($id);
        $this->template->views('request/edit', $data);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $data['price'] = convert_rupiah($this->input->post('price'));
        $result = $this->M_Purchase->update($id, $data);

        if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Berhasil Di Update'));
			redirect('request/index');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Gagal Di Update'));
			redirect('request/index');
		}

        //redirect('/purchase/index', 'refresh');
    }

    public function show($id)
    {
        $data['request_aproval'] = $this->M_Purchase->find_aproval($id, $this->userdata->id);
        $data['userdata'] = $this->userdata;
        $data['page'] = 'request_list';
        $data['judul'] = 'Show Permintaan';
        $data['deskripsi'] = 'Show Permintaan';
        $data['pembelian'] = $this->M_Purchase->find($id);
        $this->template->views('request/show', $data);
    }

    public function cetak($id)
    {
        $data['request_aproval'] = $this->M_Purchase->find_aproval($id, $this->userdata->id);
        $data['permintaan'] = $this->M_Purchase->find($id);
        $this->pdf->load_view('request/print_request', $data);
        $this->pdf->set_paper('A5', 'landscape');
		$this->pdf->render();
		$this->pdf->stream("permintaan-pembelian-" . str_replace("-","", $data['permintaan']->no_request) . ".pdf", array("Attachment" => false));

    }

    public function approval_dir()
    {
        $limit_price = '50000000';
        $data['userdata'] = $this->userdata;
        $data['purchase'] = $this->M_Purchase->get_aprov_dir($limit_price);
        $data['page'] = 'list_pembelian';
        $data['judul'] = 'List Pembelian';
        $data['deskripsi'] = 'Daftar Pembelian';
        
        $this->template->views('request/aprov_dir', $data);
    }

    public function aprove()
    {   
        $data['request_id'] = $this->input->post('request_id');
        $data['posisi_request'] = $this->input->post('posisi_request');
        $data['description'] = $this->input->post('description');
        $data['aproved_by'] = $this->userdata->id;
        $data['status'] = $this->input->post('status');
        $data['created_at'] = date('Y-m-d H:i:s');

        $result = $this->M_Purchase->insert_aprove($data);

        if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('Berhasil'));
			redirect('purchase/show/'.$this->input->post('request_id'));
		} else {
			$this->session->set_flashdata('msg', show_err_msg('Gagal'));
			redirect('purchase/show/'.$this->input->post('request_id'));
		}
    }

    public function process_po()
    {   
        $request_id = $this->input->post('id');
        $data['po_by'] = $this->userdata->id;
        $data['posisi_request'] = 6;
        $data['status'] = 'order';
        $data['no_po'] = $this->get_new_no_po();
        $data['po_date'] = date('Y-m-d H:i:s');
        
        $result = $this->M_Purchase->update($request_id, $data);

        if ($result == true) {
			$this->session->set_flashdata('msg', show_succ_msg('PO Berhasil Di Buat'));
			redirect('purchase/purchase_order/');
		} else {
			$this->session->set_flashdata('msg', show_err_msg('PO Gagal Di Buat'));
			redirect('purchase/purchase_order/');
		}
    }

    public function purchase_order()
    {
        $data['userdata'] = $this->userdata;
        $data['purchase'] = $this->M_Purchase->get_all_po();
        $data['page'] = 'purchase_order';
        $data['judul'] = 'Purchase Order';
        $data['deskripsi'] = '';
        
        $this->template->views('request/po', $data);
    }
}