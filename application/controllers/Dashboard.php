<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('M_General', 'M_Purchase'));
	}

	public function index() {
		
		$data['userdata'] 		= $this->userdata;
		$date_now = date('Y-m-d');
		$akses_level = $data['userdata']->akses_level;
		$divisi_id = $data['userdata']->divisi_id;
		$data['page'] 	= 'dashboard';
		$data['deskripsi'] 	= 'Dashboard';
		$data['judul'] = "Dashboard";

		if($akses_level == 'staff' || $akses_level == 'kabag'){

			$data['total_request'] 	= $this->M_Purchase->get_total_request_by_divisi($divisi_id);
			$data['total_satuan'] 	= $this->M_General->get_total_satuan();

		}else{
			
			$data['total_request'] 	= $this->M_Purchase->get_total_request();
			$data['total_for_approved'] 	= $this->M_Purchase->get_total_for_approved($akses_level);
			$data['total_satuan'] 	= $this->M_General->get_total_satuan();
			$data['total_po'] 	= $this->M_Purchase->get_total_po();

		}

		$this->template->views('dashboard', $data);
	}
}