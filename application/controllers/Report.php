<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_General');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "report";
		$data['judul'] = "Report";
		$data['deskripsi'] = "Laporan Harian";
		$this->template->views('report/report_harian', $data);
	}
	
	public function data_vendor() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan_data_vendor";
		$data['judul'] = "Laporan Data Vendor";
		$data['deskripsi'] = "";

		$data['data_kelas'] = $this->M_General->get_all_vendor();

		$this->template->views('laporan/data_vendor', $data);
	}	

	public function print_data_vendor() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "laporan";
		$data['judul'] = "Laporan Data Siswa";
		$data['deskripsi'] = "";

		$kelas_id = $this->input->get('kelas_id');

		$data['data_vendor'] = $this->M_General->get_all_vendor();

		$this->pdf->load_view('laporan/print_data_vendor', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("cetak_data_vendor.pdf", array("Attachment" => false));		
	}	

	public function request_report() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "request_report";
		$data['judul'] = "Laporan Permintaan";
		$data['deskripsi'] = "";

		$this->template->views('report/request_report', $data);

	}		

	public function po_report() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "po_report";
		$data['judul'] = "Laporan Purchase Order";
		$data['deskripsi'] = "";

		$this->template->views('report/po_report', $data);

	}		

	public function print_request_report() {
		$dari_tanggal = $this->input->get('dari_tanggal');
		$sampai_tanggal = $this->input->get('sampai_tanggal');

		$data['requests'] = $this->M_General->get_request_by_date_range($dari_tanggal, $sampai_tanggal);

		$this->pdf->load_view('report/print_request_report', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("print_request_report.pdf", array("Attachment" => false));		
	}	

	public function print_po_report() {
		$dari_tanggal = $this->input->get('dari_tanggal');
		$sampai_tanggal = $this->input->get('sampai_tanggal');

		$data['requests'] = $this->M_General->get_po_by_date_range($dari_tanggal, $sampai_tanggal);

		$this->pdf->load_view('report/print_po_report', $data);
		$this->pdf->set_paper('A4', 'portrait');
		$this->pdf->render();
		$this->pdf->stream("print_po_report.pdf", array("Attachment" => false));		
	}	

}