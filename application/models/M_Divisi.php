<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Divisi extends CI_Model {

    protected $table = 'divisi';

    public function get_all()
    {
        $sql = "SELECT * FROM $this->table ORDER BY id DESC";
        $result = $this->db->query($sql);
        return $result->result();
    }

    public function store($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function find($id)
    {
        $sql = "SELECT * FROM $this->table WHERE id=$id";
        $result = $this->db->query($sql);
        return $result->row();
    }

    public function update($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->table);
    }
}