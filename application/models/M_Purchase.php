<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Purchase extends CI_Model {
    
    protected $table = 'request';

    public function get_all_purchase()
    {
        $sql = "SELECT request.name, request.id as id, request.no_request, request.nominal, request.is_dir, request.is_po, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function get_all_po()
    {
        $sql = "SELECT request.no_po, request.satuan_id, request.name, request.id as id, request.no_request, request.nominal, request.is_dir, request.is_po, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE no_po IS NOT NULL AND is_po=1
                AND no_po<>''
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function get_for_purchase_order()
    {
        $sql = "SELECT request.name,request.satuan_id,  request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE request.is_po=1
                AND no_po IS NULL
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }
    
    public function get_for_approved_by_divisi($divisi_id)
    {
        $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE divisi.id=$divisi_id
                AND request.posisi_request=1
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function get_for_approved_all_purchase($akses_level)
    {
        if($akses_level=='admin'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                    LEFT JOIN divisi ON request.divisi_id = divisi.id
                    LEFT JOIN users ON request.created_by = users.id
                    ORDER BY request.id DESC";
        }elseif($akses_level=='kabag'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                    LEFT JOIN divisi ON request.divisi_id = divisi.id
                    LEFT JOIN users ON request.created_by = users.id
                    WHERE request.posisi_request=1
                    ORDER BY request.id DESC";
        }elseif($akses_level=='purchase'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=2
            ORDER BY request.id DESC";
        }elseif($akses_level=='kabag_purchase'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=3
            ORDER BY request.id DESC";
        }elseif($akses_level=='directur'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=4
            ORDER BY request.id DESC";
        }
            
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function get_by_user($user_id)
    {
        $sql = "SELECT request.id as id, request.satuan_id, request.name,  request.nominal, request.is_dir, request.is_po, divisi.name as nama_divisi, users.nama_lengkap, request.qty, request.no_request, request.posisi_request, request.description, request.spesifikasi, request.date, request.status, request.vendor_id, 
                request.created_by FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE created_by=$user_id
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function get_by_divisi($divisi_id)
    {
        $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE request.divisi_id=$divisi_id
                ORDER BY request.id DESC";
		
		$query = $this->db->query($sql);
		return $query->result();
    }

    public function destroy($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function find($id)
    {
        $sql = "SELECT request.*, request.id as id, users.nama_lengkap, divisi.name as nama_divisi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE request.id=$id
                ORDER BY request.id DESC";

        $result = $this->db->query($sql);
        return $result->row();
    }

    public function update($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->table);
    }

    public function get_aprov_dir($limit_price)
    {
        $sql = "SELECT request.id as id, request.subject, divisi.name, request.qty, request.price, request.date, request.status, request.jenis FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                WHERE request.price > $limit_price
                ORDER BY id DESC";
        $query = $this->db->query($sql);
		return $query->result();
    }

    public function insert_aprove($data)
    {
        $posisi_request = $data['posisi_request'];
        $request_id = $data['request_id'];
        $status = $data['status'];
        $is_dir = $data['is_dir'];
        unset($data['posisi_request']);
        unset($data['is_dir']);

        $insert_aprove = $this->db->insert('request_aproval', $data);

        if($status=='rejected'){
            $posisi_request = 1;
            $data_update['status'] = $status;
        }elseif($posisi_request==2){
            $data_update['status'] = 'process';
        }

        if($posisi_request==4 && $is_dir==0){
            $data_update['is_po'] = 1;
        }elseif($posisi_request==5 && $is_dir==1){
            $data_update['is_po'] = 1;
        }

        $data_update['posisi_request'] = $posisi_request;

        if($insert_aprove==true){

            $this->db->set($data_update);
            $this->db->where('id', $request_id);
            $result = $this->db->update('request');

            if($result==true){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }        
    }

    public function find_aproval($request_id, $user_id = null)
    {
                $sql = "SELECT request_aproval.status as status_approval, request_aproval.description as  description_approval, request_aproval.aproved_by, request.*, divisi.name as nama_divisi FROM request_aproval
                INNER JOIN request ON request_aproval.request_id = request.id
                INNER JOIN divisi ON request.divisi_id = divisi.id
                INNER JOIN users ON request.created_by = users.id
                WHERE request.id = $request_id";

        // $sql = "SELECT * FROM request_aproval 
        // WHERE aproved_by=$user_id AND request_id=$request_id";
        $result = $this->db->query($sql);
        return $result->row();
    }

    public function update_aprove($data, $id)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('request_aproval');
    }

    public function get_total_request() {

		$sql = "SELECT * FROM request";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

    public function get_total_request_by_divisi($divisi_id) {

		$sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE request.divisi_id=$divisi_id
                ORDER BY request.id DESC";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

    public function get_total_po() {

		$sql = "SELECT request.no_po, request.satuan_id, request.name, request.id as id, request.no_request, request.nominal, request.is_dir, request.is_po, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                LEFT JOIN divisi ON request.divisi_id = divisi.id
                LEFT JOIN users ON request.created_by = users.id
                WHERE no_po IS NOT NULL AND is_po=1
                AND no_po<>''
                ORDER BY request.id DESC";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

    public function get_total_for_approved($akses_level) {

		if($akses_level=='admin'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                    LEFT JOIN divisi ON request.divisi_id = divisi.id
                    LEFT JOIN users ON request.created_by = users.id
                    ORDER BY request.id DESC";
        }elseif($akses_level=='kabag'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
                    LEFT JOIN divisi ON request.divisi_id = divisi.id
                    LEFT JOIN users ON request.created_by = users.id
                    WHERE request.posisi_request=1
                    ORDER BY request.id DESC";
        }elseif($akses_level=='purchase'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=2
            ORDER BY request.id DESC";
        }elseif($akses_level=='kabag_purchase'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=3
            ORDER BY request.id DESC";
        }elseif($akses_level=='directur'){
            $sql = "SELECT request.name, request.satuan_id, request.id as id,  request.nominal, request.is_dir, request.is_po, request.no_request, users.nama_lengkap, request.posisi_request, divisi.name as nama_divisi, request.qty, request.description, request.date, request.status, request.posisi_request, request.vendor_id, request.spesifikasi FROM $this->table 
            LEFT JOIN divisi ON request.divisi_id = divisi.id
            LEFT JOIN users ON request.created_by = users.id
            WHERE request.posisi_request=4
            ORDER BY request.id DESC";
        }
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

   
}
