<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_General extends CI_Model {
	
	public function get_user_by_id($id) {

		$sql = "SELECT * FROM users Where id='" . $id . "'";
		
		$data = $this->db->query($sql);

		return $data->row();
	}

	public function get_jenis_mobil() {
		$sql = "SELECT * FROM jenis_mobil";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_total_user() {
		$sql = "SELECT * FROM users";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_total_satuan() {
		$sql = "SELECT * FROM satuan";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_satuan_by_id($satuan_id) {
		
		$sql = "SELECT * FROM satuan Where id='" . $satuan_id . "'";
		
		$data = $this->db->query($sql);

		return $data->row();
	}

	public function get_vendor_by_id($vendor_id) {
		
		$sql = "SELECT * FROM vendor Where id='" . $vendor_id . "'";
		
		$data = $this->db->query($sql);

		return $data->row();
	}

	public function get_divisi_by_id($divisi_id) {
		
		$sql = "SELECT * FROM divisi Where id='" . $divisi_id . "'";
		
		$data = $this->db->query($sql);

		return $data->row();
	}

	public function get_request_by_date_range($dari_tanggal, $sampai_tanggal) {
		
		$sql = "SELECT * FROM request 
				WHERE DATE(date) >= '" . $dari_tanggal ."' AND DATE(date) <= '" . $sampai_tanggal ."'";
		
		$data = $this->db->query($sql);

		return $data->result();
	}

	public function get_po_by_date_range($dari_tanggal, $sampai_tanggal) {
		
		$sql = "SELECT * FROM request 
				WHERE DATE(date) >= '" . $dari_tanggal ."' AND DATE(date) <= '" . $sampai_tanggal ."'
				AND request.is_po=1
                AND no_po IS NOT NULL";
		
		$data = $this->db->query($sql);

		return $data->result();
	}

	public function get_total_ijin_now() {

		
		$sql = "SELECT * FROM perijinan Where tgl_pengajuan=DATE_FORMAT(NOW(), '%Y-%m-%d')";
		
		$data = $this->db->query($sql);

		return $data->num_rows();

	}

	

	public function get_total_vendor() {
		$sql = "SELECT * FROM vendor";
		
		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function get_vendor_name_by_id($vendor_id) {

		$sql = "SELECT * FROM vendor Where vendor_id='" . $vendor_id . "'";
		
		$data = $this->db->query($sql);

		return $data->row('nama_vendor');
	}

	public function get_all_satuan() {
		$sql = "SELECT * FROM satuan";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_all_vendor($vendor_id = NULL) {
		$sql = "SELECT * FROM vendor";
		
		$query = $this->db->query($sql);

		return $query->result();
	}


	public function get_all_perijinan_by_vendor() {

		$vendor_id = $this->userdata->vendor_id;

		if($vendor_id == NULL){
			$sql = "SELECT * FROM perijinan";
		}else{
			$sql = "SELECT * FROM perijinan WHERE vendor_id='$vendor_id'";
		}

	
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_all_perijinan_disetujui() {
		$sql = "SELECT * FROM perijinan Where status='disetujui'";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_all_perijinan_berjalan() {
		$sql = "SELECT * FROM perijinan Where status='berjalan'";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_all_perijinan_ditolak() {
		$sql = "SELECT * FROM perijinan Where status='ditolak'";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_user_roles() {
		$sql = "SELECT * FROM jabatan";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_all_user(){

		$this->db->select('*'); 
		$this->db->from('users');
		$query = $this->db->get();

		return $query->result();
	}	
	
	public function submit_perijinan($data){
		/*$sql="SELECT nomor
				FROM ketidakhadiran
				Order By id DESC";			
		$query = $this->db->query($sql);
		$nomor = $query->row('nomor');
		*/

		//$data['nomor'] = 'K-' . sprintf("%05d", (intval(substr($nomor, -5)))+1);
		
		//$data['jumlah'] = $start_date->diff($end_date)->days+1;
		//var_dump($data['jumlah']); die();
		$data['tgl_pengajuan'] = date('Y-m-d');
		//var_dump($data); die();
		$result = $this->db->insert('perijinan', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
		
	}

	public function update_satuan($id, $data){

		$this->db->where('id', $id);
		$result = $this->db->update('satuan', $data); 

		if($result=true){
			return true;
		}else{
			return false;
		}

	}

	public function update_divisi($id, $data){

		$this->db->where('id', $id);
		$result = $this->db->update('divisi', $data); 

		if($result=true){
			return true;
		}else{
			return false;
		}

	}

	public function update_vendor($id, $data){

		$this->db->where('id', $id);
		$result = $this->db->update('vendor', $data); 

		if($result=true){
			return true;
		}else{
			return false;
		}

	}

	public function update_user($id, $data){
		$this->db->where('id', $id);
		$result = $this->db->update('users', $data); 
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_vendor($data){
		
		$data['created_at'] = date('Y-m-d H:i:s');

		$result = $this->db->insert('vendor', $data); 
		$insert_id = $this->db->insert_id();
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_satuan($data){

		$result = $this->db->insert('satuan', $data); 
		$insert_id = $this->db->insert_id();
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function insert_divisi($data){

		$result = $this->db->insert('divisi', $data); 
		$insert_id = $this->db->insert_id();
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function submit_register($data){
		
		$datas['nama_vendor'] 		= $data['nama_vendor'];
		$datas['no_telp'] 			= $data['no_telp'];
		$datas['nama_kontak'] 		= $data['nama_kontak'];
		$datas['email'] 			= $data['email'];
		$datas['no_telp_kontak'] 	= $data['no_telp_kontak'];
		$datas['alamat'] 			= $data['alamat'];
		$datas['created_at']		= date('Y-m-d H:i:s');

		unset($data['no_telp']);
		unset($data['nama_vendor']);
		unset($data['no_telp_kontak']);
		unset($data['alamat']);
		
		unset($data['confirm_password']);
		unset($data['nama_kontak']);

		$result = $this->db->insert('vendor', $datas); 
		$insert_id = $this->db->insert_id();

		$data['vendor_id'] 			= $insert_id;
		$data['akses_level'] 		= 'vendor';
		$data['password'] 			= md5($data['password']);
		$data['nama_lengkap'] 		= $datas['nama_kontak'];
		$data['status'] = '1';
		
		if($result==true){

			$result = $this->db->insert('users', $data); 

			return true;
		}else{
			return false;

		}	
	}

	public function delete_satuan($satuan_id){
	
		$this->db->where('id', $satuan_id);
		$result = $this->db->delete('satuan');
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function delete_vendor($vendor_id){
	
		$this->db->where('id', $vendor_id);
		$result = $this->db->delete('vendor');
		
		if($result==true){
			return true;
		}else{
			return false;
		}

	}

	public function insert_user($data){

		$result = $this->db->insert('users', $data); 
		if($result==true){
			return true;
		}else{
			return false;
		}
	}		
	
	public function delete_user($id){

		$this->db->where('id', $id);
		$result = $this->db->delete('users');

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	function isUserExist($username) {
		$this->db->select('id');
		$this->db->where('username', $username);
		$query = $this->db->get('users');
	
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}	

	public function update_profile($data, $UserID) {
		
		$this->db->where('id', $UserID);
		$result = $this->db->update('users', $data); 
		
		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function get_ijin_by_id($ijin_id){
		
		$this->db->where('perijinan_id', $ijin_id); 
		$this->db->from('perijinan');
        $query = $this->db->get();

		return $query->row();
	}

	public function get_all_divisi() {
		$sql = "SELECT * FROM divisi";
		
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function get_divisi_by_name($divisi_id){
		$sql = "SELECT * FROM divisi WHERE id='".$divisi_id."'";
		$query = $this->db->query($sql);
		$res = $query->result();  // this returns an object of all results
		$row = $res[0];           // get the first row
		return $row->name;
	}

	public function store_purchase($data){

		$limit_price = '50000000';

		if(intval($data['nominal']) > $limit_price){
			$data['is_dir'] = 1;
		}

		$result = $this->db->insert('request', $data);

		if($result==true){
			return true;
		}else{
			return false;
		}
	}

	public function delete($table, $column, $id)
	{
		$this->db->delete($table, array($column => $id));
	}

	public function multiple_delete($table, $id)
	{
		$this->db->where_in('id', $id);
		$this->db->delete($table);
	}
}