<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin extends CI_Model {

	public function login($user, $pass) {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$this->db->where('status', 1);

		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			return $data->row();
		} else {
			return false;
		}
	}

	public function update($data, $UserID) {
		
		$this->db->where('user_id', $UserID);
		$this->db->update('users', $data); 
		
		return true;
	}

	public function select($UserID = '') {
		if ($UserID != '') {
			$this->db->where('user_id', $UserID);
		}

		$data = $this->db->get('users');

		return $data->row();
	}
}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */