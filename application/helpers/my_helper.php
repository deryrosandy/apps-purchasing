<?php

/**
 * Check if Result Query more or one row
 *
 * @param unknown $query        	
 * @return
 *
 */
function checkRes($query) {
	if ($query->num_rows() > 0) {
		return $query->result();
	} else {
		return false;
	}
}

/**
 * Check if Result Query has one row
 *
 * @param unknown $query        	
 * @return boolean
 */
function checkRow($query) {
	if ($query->num_rows() > 0) {
		return $query->row();
	} else {
		return false;
	}
}

/**
 * Count Row
 *
 * @param unknown $query        	
 * @return boolean
 */
function countRow($query) {
	if ($query->num_rows() > 0) {
		return $query->num_rows();
	} else {
		return false;
	}
}

/**
 * Only Number Allow
 *
 * @param unknown $num        	
 * @return mixed
 */
function numberOnly($num) {
	return preg_replace('/\D/', '', $num);
}

// MSG
function show_msg($content='', $type='success', $icon='fa-info-circle', $size='14px') {
	if ($content != '') {
				
		return  '<div class="alert alert-' .$type .'alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa ' .$icon .'"></i> ' .$content . '!</h4>
				</div>';
	}
}

function show_succ_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> ' .$content . '!</h4>
				</div>';
	}
}

function show_err_msg($content='', $size='14px') {
	if ($content != '') {
		return  '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-warning"></i> ' .$content . '!</h4>
				</div>';
	}
}

// MODAL
function show_my_modal($content='', $id='', $data='', $size='md') {
	$_ci = &get_instance();

	if ($content != '') {
		$view_content = $_ci->load->view($content, $data, TRUE);

		return '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-' .$size .'" role="document">
					<div class="modal-content">
						' .$view_content .'
					</div>
				  </div>
				</div>';
	}
}

function show_my_confirm($id='', $class='', $title='Konfirmasi', $yes = 'Ya', $no = 'Tidak') {
	$_ci = &get_instance();

	if ($id != '') {
		echo   '<div class="modal fade" id="' .$id .'" role="dialog">
				  <div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
						  <h3 style="display:block; text-align:center;">' .$title .'</h3>
						  
						  <div class="col-md-6">
							<button class="form-control btn btn-primary ' .$class .'"> <i class="glyphicon glyphicon-ok-sign"></i> ' .$yes .'</button>
						  </div>
						  <div class="col-md-6">
							<button class="form-control btn btn-danger" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> ' .$no .'</button>
						  </div>
						</div>
					</div>
				  </div>
				</div>';
	}
}

function tgl_indo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getBulan($bln){
	switch ($bln){
		case 1: 
			return "Januari";
			break;
		case 2:
			return "Februari";
			break;
		case 3:
			return "Maret";
			break;
		case 4:
			return "April";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Juni";
			break;
		case 7:
			return "Juli";
			break;
		case 8:
			return "Agustus";
			break;
		case 9:
			return "September";
			break;
		case 10:
			return "Oktober";
			break;
		case 11:
			return "November";
			break;
		case 12:
			return "Desember";
			break;
	}
}

function get_tahun($tgl){
	$tahun = substr($tgl,0,4);
	return $tahun;		 
}

function get_bulan_tahun($tgl){
		$bulan = getBulan(substr($tgl,3,2));
		$tahun = substr($tgl,0,4);
		return $bulan.' '.$tahun;		 
}
function tgl_indo2($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan2(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getHour($hour){
	$jam = substr($hour,10,6);
	return $jam;	
}

function getBulan2($bln){
	switch ($bln){
		case 1: 
			return "Jan";
			break;
		case 2:
			return "Feb";
			break;
		case 3:
			return "Mar";
			break;
		case 4:
			return "Apr";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Jun";
			break;
		case 7:
			return "Jul";
			break;
		case 8:
			return "Ags";
			break;
		case 9:
			return "Sep";
			break;
		case 10:
			return "Okt";
			break;
		case 11:
			return "Nov";
			break;
		case 12:
			return "Des";
			break;
	}
}

function getHari($tgl){

	$day = date('D', strtotime($tgl));

	if ($day=='Sun'){
		return 'Minggu';
	}elseif($day=='Mon'){
		return 'Senin';
	}elseif($day=='Tue'){
		return 'Selasa';
	}elseif($day=='Wed'){
		return 'Rabu';
	}elseif($day=='Thu'){
		return 'Kamis';
	}elseif($day=='Fri'){
		return 'Jumat';
	}elseif($day=='Sat'){
		return 'Sabtu';
	}else{
		return 'ERROR!';
	}
}

function format_rupiah($angka){
	if($angka != null || $angka != '0'){
		$rupiah = "Rp. " . number_format($angka,0,',','.');
	}else{
		$rupiah = $angka;
	}
	return $rupiah;
}

function format_digit($angka){
	//var_dump($angka); 
	if($angka == '0' || $angka == null){
		return '- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}else{
		$digit = number_format(str_replace(' ', '', $angka),0,',','.');
		return $digit;
	}
}

function format_decimal($angka){
	$digit = number_format($angka,2,',','.');
	return $digit;
}

function status_active($status){
	switch ($status) {
		case "1":
			return '<label class="label label-success">Active</label>';
			break;
		case "0":
			return '<label class="label label-danger">Non Active</label>';
			break;
	}
}

function posisi_request($status){
	switch ($status) {
		case "1":
			return '<label class="label label-default">Input</label>';
			break;
		case "2":
			return '<label class="label label-primary">Approved Kabag</label>';
			break;
		case "3":
			return '<label class="label label-primary">Approved Staff Purchasing</label>';
			break;
		case "4":
			return '<label class="label label-primary">Approved Kabag Purchasing</label>';
			break;
		case "5":
			return '<label class="label label-primary">Approved Direktur</label>';
			break;
		case "6":
			return '<label class="label label-primary">Process PO</label>';
			break;
		case "7":
			return '<label class="label label-primary">Selesai</label>';
			break;
	}
}

function status_request($status){
	switch ($status) {
		case "open":
			return '<label class="label label-default">Open</label>';
			break;
		case "process":
			return '<label class="label label-success">Process</label>';
			break;
		case "order":
			return '<label class="label label-primary">Order</label>';
			break;
		case "closed":
			return '<label class="label label-info">Closed</label>';
			break;
		case "cancel":
			return '<label class="label label-danger">Cancel</label>';
			break;
	}
}

function get_satuan_name($satuan_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$satuan = $CI->M_General->get_satuan_by_id($satuan_id);
	
	if($satuan != null){
		$satuan_name = $satuan->name;
	}else{
		$satuan_name = '-';
	}

	return $satuan_name;
}

function get_divisi_name($divisi_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$divisi = $CI->M_General->get_divisi_by_id($divisi_id);
	
	if($divisi != null){
		$divisi_name = $divisi->name;
	}else{
		$divisi_name = '-';
	}

	return $divisi_name;
}

function get_user_name($user_id){
	$CI = get_instance();
	$CI->load->model('M_General');

	$user_name = $CI->M_General->get_user_by_id($user_id)->nama_lengkap;
	
	return $user_name;
}

function get_status_name($status){
	switch ($status){
		case "0": 
			return "<label class='label label-danger'>Non Active</label>";
			break;
		case "1":
		return "<label class='label label-primary'>Active</label>";
			break;
	}
}

if(!function_exists('convert_rupiah')){
	function convert_rupiah($rp){
		$result = substr(str_replace(".", "", $rp), 3);
		return $result;
	}
}

?>