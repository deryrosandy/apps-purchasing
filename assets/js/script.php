<script type="text/javascript">

	function calculate(){		

        var harga = $('#paket_id option:selected').attr("data-harga");
        var jumlah = $('#jumlah').val();
        var total = harga*jumlah;

        var result = $('#total_harga').val(total);

        return result;
    }

	function get_siswa_by_id(siswa_id){
        $.ajax({
            data: {"siswa_id" : siswa_id},
			url:"<?php echo site_url('master/get_siswa_by_id')?>",
			type: "POST",
			dataType:"json",
			success: function(response){
				get_kelas_name(response.vendor_id);
			},
			error: function(){

			}		
        });
    }

	function get_kelas_name(vendor_id){
        $.ajax({
            data: {"vendor_id" : vendor_id},
			url:"<?php echo site_url('master/get_vendor_by_id')?>",
			type: "POST",
			dataType:"json",
			success: function(response){
				$("#kelas_name").val(response.nama_kelas);
				$("#vendor_id").val(response.vendor_id);
			},
			error: function(){

			}		
        });
    }

	function get_biaya_spp(siswa_id){
		
		get_siswa_by_id(siswa_id);
		
		$.ajax({
			data: {"siswa_id" : siswa_id},
			url:"<?php echo base_url(); ?>transaksi/get_biaya_spp",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#biaya_spp").val(response.nominal_spp);	
				var tahun_awal = 	response.tahun_awal.substr(0, 4);
				var tahun_akhir = 	response.tahun_akhir.substr(0, 4);
				var tahun_ajaran = tahun_awal + '/' + tahun_akhir;
				$("#tahun_ajaran").val(tahun_ajaran);						
			}
		});
	}
	
	window.onload = function() {
		$('input[name=price]').maskMoney({prefix: 'Rp. ', thousands: '.', decimal: ',', precision: 0});
		if ($(window).width() > 330) {
 			$('.navbar').addClass('navbar-static-top');
		}else{
			$('.navbar').addClass('navbar-fixed-top');
		}

	//	data_customer();
	//	list_jabatan();
		//jenis_kelamin();
		//user_roles();
		list_data_users();

		list_data_divisi();
		list_data_pembelian();

		$(function (){
			$('.select2').select2();
		});

		$(document).ready(function() {

			$(function () {
				var date = new Date();
				var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				$('.datetimepicker').datetimepicker({
					language:  'id',
					weekStart: 1,
					todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: today
				});			
			});
			$(function () {
				var date = new Date();
				var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				$('.datemonth').datetimepicker({
					language:  'id',
					weekStart: 1,
					todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 3,
					minView: 3,
					forceParse: 0
				});			
			});
			$(function () {
				var date = new Date();
				var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
				$('.print_date').datepicker({
					language:  'id',
					weekStart: 1,
					todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 0,
					minView: 0,
					forceParse: 0
				});			
			});

			$('#list_data_siswa').DataTable( {
				
				"responsive": true,
				"language": {
					"searchPlaceholder": 'Search...',
					"sSearch": '',
				},
				"paging":   true,
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 50, 100],
			});

		});
	}
	
	var table;

	function formatCurrency(num) {

		//num = num.toString().replace(/\Rp|/g,'');
		if(isNaN(num))
			num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+'.'+
			num.substring(num.length-(4*i+3));
		
		return num;
	}

	$(document).on('click', '.delete', function(){
		var id	 = $(this).data("id");
		var url = $(this).data("url");
		swal({
			title: "Apakah anda yakin ?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id" : id},
					url: url,
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_vendor', function(){
		var vendor_id	 = $(this).attr("vendor_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"vendor_id" : vendor_id},
					url:"<?php echo site_url('master/delete_vendor')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.confirm', function(){
		var id	 = $(this).data("id");
		var url	 = $(this).data("url");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-primary",
			confirmButtonText: "Yes",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id" : id},
					url: url,
					type: "POST",
					success: function(msg){
						swal("Success!", "PO Berhasil Di Buat", "success");		
						//location.reload();		
						location.href = "<?php echo site_url('purchase/purchase_order')?>"; 			
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});
	$(document).on('click', '.status_berjalan', function(){
		var perijinan_id	 = $(this).attr("perijinan_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-primary",
			confirmButtonText: "Yes",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"perijinan_id" : perijinan_id, "status" : "berjalan"},
					url:"<?php echo site_url('perijinan/update_status')?>",
					type: "POST",
					success: function(msg){
						swal("Success!", "Update Status Izin Berhasil", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});
	$(document).on('click', '.status_tolak', function(){
		var perijinan_id	 = $(this).attr("perijinan_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-primary",
			confirmButtonText: "Yes",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"perijinan_id" : perijinan_id, "status" : "ditolak"},
					url:"<?php echo site_url('perijinan/update_status')?>",
					type: "POST",
					success: function(msg){
						swal("Success!", "Update Status Izin Berhasil", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});
	$(document).on('click', '.status_done', function(){
		var perijinan_id	 = $(this).attr("perijinan_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-primary",
			confirmButtonText: "Yes",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"perijinan_id" : perijinan_id, "status" : "selesai"},
					url:"<?php echo site_url('perijinan/update_status')?>",
					type: "POST",
					success: function(msg){
						swal("Success!", "Update Status Izin Berhasil", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_transaksi', function(){
		var transaksi_id	 = $(this).attr("transaksi_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"transaksi_id" : transaksi_id},
					url:"<?php echo site_url('transaksi/delete_transaksi')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_schedule', function(){
		var schedule_id	 = $(this).attr("schedule_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"schedule_id" : schedule_id},
					url:"<?php echo site_url('master/delete_schedule')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_tahun_ajaran', function(){
		var tahun_ajaran_id	 = $(this).attr("tahun_ajaran_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"tahun_ajaran_id" : tahun_ajaran_id},
					url:"<?php echo site_url('master/delete_tahun_ajaran')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_area', function(){
		var id_area	 = $(this).attr("id_area");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"id_area" : id_area},
					url:"<?php echo site_url('master/delete_area_kerja')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.delete_tiket', function(){
		var tiket_id	 = $(this).attr("tiket_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"tiket_id" : tiket_id},
					url:"<?php echo site_url('tiket/delete_tiket')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.view_tiket', function(){
		var tiket_id	 = $(this).attr("tiket_id");
		$('#tiket_id #response').html("");
		$('#tiket_id #keterangan').html("");

		$.ajax({
			url:"<?php echo base_url(); ?>tiket/get_tiket_by_id",
			method:"POST",
			data:{tiket_id:tiket_id},
			dataType:"json",
			success:function(data){
				$('#modal_edit_tiket #tiket_id').val(data.tiket_id);
				$('#modal_edit_tiket #no_tiket').val(data.no_tiket);
				$('#modal_edit_tiket #customer_id').val(data.customer_id);
				$('#modal_edit_tiket #schedule_id').val(data.schedule_id);
				$('#modal_edit_tiket #paket_id').val(data.paket_id);
				$('#modal_edit_tiket #jumlah').val(data.jumlah);
				$('#modal_edit_tiket #keterangan').val(data.keterangan);
				$('#modal_edit_tiket').modal('show');
			}
		});
	});

	$(document).on('click', '.edit_siswa', function(){
		var siswa_id	 = $(this).attr("siswa_id");
		$('.modal-title').text('Edit Siswa');
		$('#edit_siswa_modal #response').html("");
		$('#edit_siswa_modal #keterangan').html("");

		$.ajax({
			url:"<?php echo base_url(); ?>master/get_siswa_by_id",
			method:"POST",
			data:{siswa_id:siswa_id},
			dataType:"json",
			success:function(data){
				console.log('dor');
				kelas(data.vendor_id);
				tahun_ajaran(data.tahun_ajaran_id);
				jenis_kelamin(data.jenis_kelamin);
				status(data.status);
				$('#edit_siswa_modal #siswa_id').val(data.siswa_id);
				$('#edit_siswa_modal #nis').val(data.nis);
				$('#edit_siswa_modal #nama_lengkap').val(data.nama_lengkap);
				$('#edit_siswa_modal #tanggal_lahir').val(data.tanggal_lahir);
				$('#edit_siswa_modal #tempat_lahir').val(data.tempat_lahir);
				$('#edit_siswa_modal #jenis_kelamin').val(data.jenis_kelamin);
				$('#edit_siswa_modal #nama_orangtua').val(data.nama_orangtua);
				$('#edit_siswa_modal #tgl_masuk').val(data.tgl_masuk);
				$('#edit_siswa_modal #alamat').val(data.alamat);
				//$('#edit_siswa_modal #vendor_id').val(data.vendor_id);
				$('#edit_siswa_modal').modal('show');
			}
		});
	});

	$(document).on('click', '.delete_user', function(){
		var user_id	 = $(this).attr("user_id");
		swal({
			title: "Are you sure?",
			text: " ",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
			},
			function () {
				$.ajax({
					data: {"user_id" : user_id},
					url:"<?php echo site_url('master/delete_user')?>",
					type: "POST",
					success: function(msg){
						swal("Deleted!", "Delete Success", "success");		
						location.reload();					
					},
					error: function(){
						swal("Error", "Delete Failed", "error");
						location.reload();
					}					
				});  	               
			}
		);
	});

	$(document).on('click', '.response_cuti', function(){
		var id_cuti	 = $(this).attr("id_cuti");
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_cuti_by_id",
			method:"POST",
			data:{id_cuti:id_cuti},
			dataType:"json",
			success:function(data){
				$('#id_cuti').val(data.id);
				$('#konfirmasi_cuti').modal('show');
			}
		});
	});

	$("#konfirmasi_cuti button[type=submit]").click(function(){
		var response_cuti	= $("#response_cuti").val();
		var id_cuti	 		= $("#id_cuti").val();
		
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/response_cuti",
			method:"POST",
			data:{id_cuti:id_cuti,response_cuti:response_cuti},
			dataType:"json",
			success: function(msg){
				$('#konfirmasi_cuti').modal('hide');
				swal("Success!", "Success", "success");		
				location.reload();					
			},
			error: function(){
				$('#konfirmasi_cuti').modal('hide');
				swal("Error", "Failed", "error");
				location.reload();
			}					
		}); 
	});

	$(document).on('click', '.edit_kar', function(){
		var id_kar	 = $(this).attr("id_kar");
		
		$("#checked_status").html("");
		$('#edit_karyawan_modal #id_karyawan').html("");
		$('#edit_karyawan_modal #nik').html("");
		$('#edit_karyawan_modal #nama_lengkap').html("");
		$('#edit_karyawan_modal #email').html("");
		$('#edit_karyawan_modal #no_ktp').html("");
		$('#edit_karyawan_modal #jabatan_list').html("");
		$('#edit_karyawan_modal #jenis_kelamin').html("");
		$('#edit_karyawan_modal #divisi_list').html("");
		$('#edit_karyawan_modal #jenis_kelamin').html("");
		$('#edit_karyawan_modal #tanggal_masuk').html("");
		$('#edit_karyawan_modal #no_telp').html("");
		$('#edit_karyawan_modal #tempat_lahir').html("");
		$('#edit_karyawan_modal #tanggal_lahir').html("");
		$('#edit_karyawan_modal #agama').html("");
		$('#edit_karyawan_modal #alamat').html("");
		$('#edit_karyawan_modal #status').html("");

		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_karyawan_by_id",
			method:"POST",
			data:{id_kar:id_kar},
			dataType:"json",
			success:function(data){
				console.log(data);
				$("#checked_status").html("");
				$('#edit_karyawan_modal').modal('show');
				$('#edit_karyawan_modal #id_karyawan').val(data.id_karyawan);
				$('#edit_karyawan_modal #nik').val(data.nik);
				$('#edit_karyawan_modal #email').val(data.email);
				$('#edit_karyawan_modal #nama_lengkap').val(data.nama_lengkap);
				$('#edit_karyawan_modal #no_ktp').val(data.no_ktp);
				list_jabatan(data.jabatan_id);
				jenis_kelamin(data.jenis_kelamin);
				list_divisi(data.divisi_id);
				agama_list(data.agama);
				status_kar(data.status);
				$('#edit_karyawan_modal #jenis_kelamin').val(data.jenis_kelamin);
				$('#edit_karyawan_modal #tanggal_masuk').val(data.tanggal_masuk);
				$('#edit_karyawan_modal #no_telp').val(data.no_telp);
				$('#edit_karyawan_modal #tempat_lahir').val(data.tempat_lahir);
				$('#edit_karyawan_modal #tanggal_lahir').val(data.tanggal_lahir);
				//$('#edit_karyawan_modal #agama').val(data.agama);
				$('#edit_karyawan_modal #alamat').val(data.alamat);
				//$('#edit_karyawan_modal #status_kontrak').val(data.status_kontrak);
			}
		});	
	});

	$(document).on('click', '.edit_category', function(){
		$('#id_category').val("");
		$('#keterangan').html("")
		$('#nama_category').val("");

		var id_cat	 = $(this).attr("id_cat");
		$('.modal-title').text('Edit Kategori Cuti'); 
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_category_by_id",
			method:"POST",
			data:{id_cat:id_cat},
			dataType:"json",
			success:function(data){				
				$('#id_category').val(data.id_category);
				$('#keterangan').append(data.keterangan);
				$('#nama_category').val(data.category_name);
				$('#modal_edit_category').modal('show');
			}
		});	
	});

	$(document).on('click', '.edit_vendor', function(){
		$('#modal_edit_vendor .vendor_id').val("");
		$('#modal_edit_vendor .nama_vendor').html("")
		$('#modal_edit_vendor .no_telp').val("");
		$('#modal_edit_vendor .email').val("");
		$('#modal_edit_vendor .alamat').val("");
		$('#modal_edit_vendor .nama_kontak').val("");
		$('#modal_edit_vendor .no_telp_kontak').val("");

		var vendor_id	 = $(this).attr("vendor_id");
		$('.modal-title').text('Edit Vendor'); 
		$.ajax({
			url:"<?php echo base_url(); ?>master/get_vendor_by_id",
			method:"POST",
			data:{vendor_id:vendor_id},
			dataType:"json",
			success:function(data){		
				$('#modal_edit_vendor .vendor_id').val(data.vendor_id);
				$('#modal_edit_vendor .nama_vendor').val(data.nama_vendor);
				$('#modal_edit_vendor .no_telp').val(data.no_telp);
				$('#modal_edit_vendor .email').val(data.email);
				$('#modal_edit_vendor .alamat').val(data.alamat);
				$('#modal_edit_vendor .nama_kontak').val(data.nama_kontak);
				$('#modal_edit_vendor .no_telp_kontak').val(data.no_telp_kontak);
				$('#modal_edit_vendor').modal('show');
			}
		});	
	});

	$(document).on('click', '.edit_tahun_ajaran', function(){
		$('#modal_edit_ajaran .tahun_ajaran_id').val("");
		$('#modal_edit_ajaran .tahun_awal').html("")
		$('#modal_edit_ajaran .tahun_akhir').val("");
		$('#modal_edit_ajaran .nominal_spp').val("");

		var tahun_ajaran_id	 = $(this).attr("tahun_ajaran_id");
		$('.modal-title').text('Edit Tahun Ajaran'); 
		$.ajax({
			url:"<?php echo base_url(); ?>master/get_tahun_ajaran_by_id",
			method:"POST",
			data:{tahun_ajaran_id:tahun_ajaran_id},
			dataType:"json",
			success:function(data){		
				$('#modal_edit_ajaran .tahun_ajaran_id').val(data.tahun_ajaran_id);
				$('#modal_edit_ajaran .tahun_awal').val(data.tahun_awal);
				$('#modal_edit_ajaran .tahun_akhir').val(data.tahun_akhir);
				$('#modal_edit_ajaran .nominal_spp').val(data.nominal_spp);
				$('#modal_edit_ajaran').modal('show');
			}
		});	
	});

	$(document).on('click', '.edit_customer', function(){
		var customer_id	 = $(this).attr("customer_id");
		$.ajax({
			url:"<?php echo base_url(); ?>master/get_customer_by_id",
			method:"POST",
			data:{customer_id:customer_id},
			dataType:"json",
			success:function(data){
				//console.log(data);
				$('#edit_customer_modal').modal('show');
				$('#edit_customer_modal .nama_lengkap').val(data.nama_lengkap);
				$('#edit_customer_modal .no_ktp').val(data.no_ktp);
				$('#edit_customer_modal .email').val(data.email);
				$('#edit_customer_modal .phone_number').val(data.phone_number);
				$('#edit_customer_modal .alamat').val(data.alamat);

				jenis_kelamin(data.jenis_kelamin);
				
				$('#edit_customer_modal .customer_id').val(data.customer_id);
			}
		});	
	});

	$(document).on('click', '.view_customer', function(){
		var customer_id	 = $(this).attr("customer_id");
		$('#view_customer_modal .no_ktp').html("");
		$('#view_customer_modal .nama_lengkap').html("");
		$('#view_customer_modal .email').html("");
		$('#view_customer_modal .phone_number').html("");
		$('#view_customer_modal .alamat').html("");
		$('#view_customer_modal .jenis_kelamin').html("");

		$.ajax({
			url:"<?php echo base_url(); ?>master/get_customer_by_id",
			method:"POST",
			data:{customer_id:customer_id},
			dataType:"json",
			success:function(data){
				//console.log(data);
				$('#view_customer_modal').modal('show');
				$('#view_customer_modal .nama_lengkap').append(data.nama_lengkap);
				$('#view_customer_modal .no_ktp').append(data.no_ktp);
				$('#view_customer_modal .email').append(data.email);
				$('#view_customer_modal .phone_number').append(data.phone_number);
				$('#view_customer_modal .alamat').append(data.alamat);

				if(data.jenis_kelamin=='L'){
					var jenis_kelamin = 'Laki-laki';
				}else{
					var jenis_kelamin = 'Perempuan';
				}
				$('#view_customer_modal .jenis_kelamin').append(jenis_kelamin);

				//jenis_kelamin(data.jenis_kelamin);
			}
		});	
	});

	function ConfirmDeleteUser(){
		$(document).on('click', '#delete_user', function(){
			var user_id	 = $(this).attr("user_id");
			
			var x = confirm("Are you sure you want to delete?");
			if (x){
				$.ajax({
					data: {"user_id" : user_id},
					url:"<?php echo site_url('master/delete_user')?>",
					type: "POST",
					success:function(response){
						location.reload();
					}
				});
			}else{
				return false;
			}
		});
	}

	function checkisNaN(num) {

		if(isNaN(num))
			num = 0;

		return num;
	}

	function detail_harian(){
		$(document).ready(function() {
			$('#data-harian-detail').DataTable({
				"paging":   false,
				"ordering": false,
				"info":     false,
				"searching": false,
				"responsive": true,
				"columnDefs": [
					{ "width": "40%", "targets": 0 },
					{ "width": "5%", "targets": 1 },
					{ "width": "20%", "targets": 2 },
					{ "width": "5%", "targets": 3 },
					{ "width": "20%", "targets": 4 }
				],
				"fixedColumns": true,
			});
		} );
	};
	
	function jenis_kelamin(jeniskelamin){
		if(jeniskelamin=='L'){			
			$(".jenis_kelamin").html("");	
			var str2 = "<option selected value='L'>Laki-laki</option>" +
						"<option value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		}else if(jeniskelamin=='P'){
			$(".jenis_kelamin").html("");	
			var str2 = "<option  value='L'>Laki-laki</option>" +
						"<option selected value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		}else{
			$(".jenis_kelamin").html("");	
			var str2 = "<option>Pilih Jenis Kelamin</option>" +
						"<option value='L'>Laki-laki</option>" +
						"<option value='P'>Perempuan</option>";
			$(".jenis_kelamin").append(str2);
		}		
	}
	function status_kar(status){
		if(status=='0'){			
			$(".status").html("");	
			var str2 = '<option value="1">Active</option>' + 
						'<option selected value="0">Non Active</option>';
			$(".status").append(str2);
		}else{
			$(".status").html("");	
			var str2 = '<option selected value="1">Active</option>' + 
						'<option  value="0">Non Active</option>';
			$(".status").append(str2);
		}		
	}
	function status(status){
		if(status=='0'){			
			$("#status").html("");	
			var str2 = '<option value="1">Active</option>' + 
						'<option selected value="0">Non Active</option>';
			$("#status").append(str2);
		}else{
			$("#status").html("");	
			var str2 = '<option selected value="1">Active</option>' + 
						'<option  value="0">Non Active</option>';
			$("#status").append(str2);
		}		
	}

	function status_kontrak(status){
		if(status=='tetap'){			
			$(".status_kontrak").html("");	
			var str2 = '<option selected value="tetap">Pegawai Tetap</option>' +
						'<option value="kontrak">Pegawai Kontrak</option>' +
						'<option value="magang">Pegawai Magang</option>' +
						'<option value="mengundurkan diri">Mengundurkan Diri</option>';
			$(".status_kontrak").append(str2);
		}else if(status=='kontrak'){
			$(".status_kontrak").html("");	
			var str2 = '<option  value="tetap">Pegawai Tetap</option>' +
						'<option selected value="kontrak">Pegawai Kontrak</option>' +
						'<option value="magang">Pegawai Magang</option>' +
						'<option value="mengundurkan diri">Mengundurkan Diri</option>';
			$(".status_kontrak").append(str2);
		}else if(status=='magang'){
			$(".status_kontrak").html("");	
			var str2 = '<option value="tetap">Pegawai Tetap</option>' +
						'<option value="kontrak">Pegawai Kontrak</option>' +
						'<option selected value="magang">Pegawai Magang</option>' +
						'<option value="mengundurkan diri">Mengundurkan Diri</option>';
			$(".status_kontrak").append(str2);
		}else if(status=='mengundurkan diri'){
			$(".status_kontrak").html("");	
			var str2 = '<option value="tetap">Pegawai Tetap</option>' +
						'<option value="kontrak">Pegawai Kontrak</option>' +
						'<option  value="magang">Pegawai Magang</option>' +
						'<option selected value="mengundurkan diri">Mengundurkan Diri</option>';
			$(".status_kontrak").append(str2);
		}else{
			$(".status_kontrak").html("");	
			var str2 = '<option value="tetap">Pegawai Tetap</option>' +
						'<option value="kontrak">Pegawai Kontrak</option>' +
						'<option  value="magang">Pegawai Magang</option>' +
						'<option value="mengundurkan diri">Mengundurkan Diri</option>';
			$(".status_kontrak").append(str2);
		}		
	}

	function agama_list(agama){
		if(agama=='islam'){			
			$(".agama").html("");	'islam','protestan','katolik','hindu','budha','kepercayaan'
			var str2 = "<option selected value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else if(agama=='protestan'){
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option selected value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else if(agama=='katolik'){
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option selected value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else if(agama=='hindu'){
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option selected value='hindu'>Hindu</option>" +
						"<option value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else if(agama=='budha'){
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option selected value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else if(agama=='kepercayaan'){
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option  value='budha'>Budha</option>" +
						"<option selected value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}else{
			$(".agama").html("");	
			var str2 = "<option  value='islam'>Islam</option>" +
						"<option value='protestan'>Protestan</option>" +
						"<option value='katolik'>Katolik</option>" +
						"<option value='hindu'>Hindu</option>" +
						"<option  value='budha'>Budha</option>" +
						"<option value='kepercayaan'>Kepercayaan</option>";
			$(".agama").append(str2);
		}		
	}

	function list_jabatan(jabatan){
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_jabatan_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$(".jabatan_list").html("");
				var str = "<option value='0'>- Pilih Jabatan -</option>";
				$(".jabatan_list").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var kd_jabatan = response[i].kd_jabatan;
					var nama_jabatan = response[i].nama_jabatan;
					var id_jabatan = response[i].id_jabatan;
					if(id_jabatan==jabatan){				
						var str2 = "<option selected value='" + id_jabatan + "'>" + nama_jabatan + "</option>";
					}else{
						var str2 = "<option value='" + id_jabatan + "'>" + nama_jabatan + "</option>";
					}
					$(".jabatan_list").append(str2);
				}						
			}
		});
	}

	function list_divisi(divisi){
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_divisi_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$(".divisi_list").html("");
				var str = "<option value='0'>- Pilih Divisi -</option>";
				$(".divisi_list").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var id_divisi = response[i].id_divisi;
					var nama_divisi = response[i].nama_divisi;
					if(id_divisi==divisi){				
						var str2 = "<option selected value='" + id_divisi + "'>" + nama_divisi + "</option>";
					}else{
						var str2 = "<option value='" + id_divisi + "'>" + nama_divisi + "</option>";
					}
					$(".divisi_list").append(str2);
				}						
			}
		});
	}
	function category(category){
		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_category_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#category_list").html("");
				var str = "<option value='0'>- Pilih Kategori -</option>";
				$("#category_list").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var id_category = response[i].id_category;
					var category_name = response[i].category_name;
					if(id_category==category){				
						var str2 = "<option selected value='" + id_category + "'>" + category_name + "</option>";
					}else{
						var str2 = "<option value='" + id_category + "'>" + category_name + "</option>";
					}
					$("#category_list").append(str2);
				}						
			}
		});
	}

	function kelas(kelas){
		$.ajax({
			url:"<?php echo base_url(); ?>master/get_kelas_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#vendor_id").html("");
				var str = "<option value='0'>- Pilih Kelas -</option>";
				$("#vendor_id").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var vendor_id = response[i].vendor_id;
					var nama_kelas = response[i].nama_kelas;
					if(kelas==vendor_id){				
						var str2 = "<option selected value='" + vendor_id + "'>" + nama_kelas + "</option>";
					}else{
						var str2 = "<option value='" + vendor_id + "'>" + nama_kelas + "</option>";
					}
					$("#vendor_id").append(str2);
				}						
			}
		});
	}

	function tahun_ajaran(tahun_ajaran){
		$.ajax({
			url:"<?php echo base_url(); ?>master/get_tahun_ajaran_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#tahun_ajaran_id").html("");
				var str = "<option value='0'>- Pilih Tahun Ajaran -</option>";
				$("#tahun_ajaran_id").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var tahun_ajaran_id = response[i].tahun_ajaran_id;
					var tahun_awal = response[i].tahun_awal.substr(0, 4);
					var tahun_akhir = response[i].tahun_akhir.substr(0, 4);
					if(tahun_ajaran==tahun_ajaran_id){				
						var str2 = "<option selected value='" + tahun_ajaran_id + "'>" + tahun_awal + '/' + tahun_akhir + "</option>";
					}else{
						var str2 = "<option value='" + tahun_ajaran_id + "'>" + tahun_awal + '/' + tahun_akhir + "</option>";
					}
					$("#tahun_ajaran_id").append(str2);
				}						
			}
		});
	}

	function data_karyawan(){
		$(document).ready(function(){			
			//datatables
			table = $('#data-karyawan').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('master/list_data_karyawan')?>",
					"type": "POST"
				},
				"draw" : false,
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	function data_customer(){
		$(document).ready(function(){			
			//datatables
			table = $('#data-customer').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('master/list_data_customer')?>",
					"type": "POST"
				},
				"draw" : false,
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('no_ktp', aData[1]);
				}

			});	
		});
	};

	function data_saldo_cuti(){
		$(document).ready(function(){			
			//datatables
			table = $('#data-saldo').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('master/list_data_saldo_cuti')?>",
					"type": "POST"
				},
				"draw" : false,
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	function data_tiket_all(){
		$(document).ready(function(){			
			//datatables
			table = $('#data_tiket_all').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('tiket/list_data_tiket_all')?>",
					"type": "POST"
				},
				"draw" : false,

				//Set column definition initialisation properties.
				/*
				"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": false, //set not orderable
					},
					{ "width": "20px", "targets": 0 },
					{ "width": "80px", "targets": 1 },
					{ "width": "140px", "targets": 2 },
					{ "width": "60px", "targets": 3 },
					{ "width": "80px", "targets": 4 },
					{ "width": "60px", "targets": 5 },
					{ "width": "80px", "targets": 6 },
					{ "width": "40px", "targets": 7 },
				],
				*/
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('no_tiket', aData[1]);
				}

			});	
		});
	};

	function data_ijin_tanpa_upah(){
		$(document).ready(function(){			
			//datatables
			table = $('#data_ijin_tanpa_upah').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('ketidakhadiran/list_data_ijin_tanpa_upah')?>",
					"type": "POST"
				},
				"draw" : false,

				//Set column definition initialisation properties.
				/*
				"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": false, //set not orderable
					},
					{ "width": "20px", "targets": 0 },
					{ "width": "80px", "targets": 1 },
					{ "width": "140px", "targets": 2 },
					{ "width": "60px", "targets": 3 },
					{ "width": "80px", "targets": 4 },
					{ "width": "60px", "targets": 5 },
					{ "width": "80px", "targets": 6 },
					{ "width": "40px", "targets": 7 },
				],
				*/
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	function data_ijin_sakit(){
		$(document).ready(function(){			
			//datatables
			table = $('#data_ijin_sakit').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('ketidakhadiran/list_data_ijin_sakit')?>",
					"type": "POST"
				},
				"draw" : false,

				//Set column definition initialisation properties.
				/*
				"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": false, //set not orderable
					},
					{ "width": "20px", "targets": 0 },
					{ "width": "80px", "targets": 1 },
					{ "width": "140px", "targets": 2 },
					{ "width": "60px", "targets": 3 },
					{ "width": "80px", "targets": 4 },
					{ "width": "60px", "targets": 5 },
					{ "width": "80px", "targets": 6 },
					{ "width": "40px", "targets": 7 },
				],
				*/
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	function data_ijin_dinas(){
		$(document).ready(function(){			
			//datatables
			table = $('#data_ijin_dinas').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('ketidakhadiran/list_data_ijin_dinas')?>",
					"type": "POST"
				},
				"draw" : false,

				//Set column definition initialisation properties.
				/*
				"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": false, //set not orderable
					},
					{ "width": "20px", "targets": 0 },
					{ "width": "80px", "targets": 1 },
					{ "width": "140px", "targets": 2 },
					{ "width": "60px", "targets": 3 },
					{ "width": "80px", "targets": 4 },
					{ "width": "60px", "targets": 5 },
					{ "width": "80px", "targets": 6 },
					{ "width": "40px", "targets": 7 },
				],
				*/
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	function data_cuti_tahunan(){
		$(document).ready(function(){			
			//datatables
			table = $('#data_cuti_tahunan').DataTable({ 

				"processing": true, //Feature control the processing indicator.
				"serverSide": true, //Feature control DataTables' server-side processing mode.
				"order": [], //Initial no order.
				"pageLength": 20,
				"lengthMenu": [ 10, 20, 30, 50 ],
				"responsive": true,

				// Load data for the table's content from an Ajax source
				"ajax": {
					//"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
					"url": "<?php echo site_url('ketidakhadiran/list_data_cuti_tahunan')?>",
					"type": "POST"
				},
				"draw" : false,

				//Set column definition initialisation properties.
				/*
				"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": false, //set not orderable
					},
					{ "width": "20px", "targets": 0 },
					{ "width": "80px", "targets": 1 },
					{ "width": "140px", "targets": 2 },
					{ "width": "60px", "targets": 3 },
					{ "width": "80px", "targets": 4 },
					{ "width": "60px", "targets": 5 },
					{ "width": "80px", "targets": 6 },
					{ "width": "40px", "targets": 7 },
				],
				*/
				"fnCreatedRow": function( nRow, aData, iDataIndex ) {
					$(nRow).attr('nik', aData[1]);
					$(nRow).attr('nik', aData[1]);
				}

			});	
		});
	};

	$("#tambah_customer_modal button[type=submit]").click(function(){
		$('.form_customer').validate({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(form) {
				// do other things for a valid form
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('master/tambah_customer'); ?>",
					data: $('#tambah_customer_modal .form_customer').serialize(),
							success: function(msg){
								$("#status").html(msg)
								$("#tambah_customer_modal").modal('hide'); 
								location.reload();
							},
					error: function(){

					}
				});	
			}
		});
	});
	
	$("#tambah_siswa_modal button[type=submit]").click(function(){
		$('.form_siswa').validate({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(form) {
				// do other things for a valid form
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('master/tambah_siswa'); ?>",
					data: $('#tambah_siswa_modal .form_siswa').serialize(),
							success: function(msg){
								$("#status").html(msg)
								$("#tambah_siswa_modal").modal('hide'); 
								location.reload();
							},
					error: function(){

					}
				});	
			}
		});
	});
	
	/*
	$("#edit_customer_modal button[type=submit]").click(function(){
		$('.form_customer').validate({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			},
			submitHandler: function(form) {
				// do other things for a valid form
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('master/update_customer'); ?>",
					data: $('#edit_customer_modal .form_customer').serialize(),
							success: function(msg){
								$("#status").html(msg)
								$("#edit_customer_modal").modal('hide'); 
								location.reload();
							},
					error: function(){

					}
				});	
			}
		});
	});
	*/
	
	$("#edit_customer_modal button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/update_customer'); ?>",
			data: $('#edit_customer_modal .form_customer').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#edit_customer_modal").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});
	
	$("#modal_tambah_ajaran button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/tambah_ajaran'); ?>",
			data: $('#modal_tambah_ajaran .form_ajaran').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_tambah_ajaran").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#edit_siswa_modal button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/update_siswa'); ?>",
			data: $('#edit_siswa_modal .form_siswa').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#edit_siswa_modal").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_vendor button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/update_vendor'); ?>",
			data: $('#modal_edit_vendor .form_vendor').serialize(),
					success: function(msg){
						//$("#status").html(msg)
						$("#modal_edit_vendor").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_ajaran button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/update_tahun_ajaran'); ?>",
			data: $('#modal_edit_ajaran .form_ajaran').serialize(),
					success: function(msg){
						//$("#status").html(msg)
						$("#modal_edit_ajaran").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_tambah_vendor button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/tambah_vendor'); ?>",
			data: $('#modal_tambah_vendor .form_vendor').serialize(),
					success: function(msg){
						$("#status").html(msg)
						//$("#modal_tambah_vendor").modal('hide'); 
						//location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_tambah_kategori button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/tambah_kategori'); ?>",
			data: $('#modal_tambah_kategori .form_category').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_tambah_kategori").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_category button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_category'); ?>",
			data: $('#modal_edit_category .form_category').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_category").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});
	
	$("#edit_karyawan_modal button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_karyawan'); ?>",
			data: $('#edit_karyawan_modal .form_karyawan').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#edit_karyawan_modal").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_tiket button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('tiket/edit_tiket'); ?>",
			data: $('#modal_edit_tiket .form_edit_tiket').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_tiket").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_divisi button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_divisi'); ?>",
			data: $('#modal_edit_divisi .form_divisi').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_divisi").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});

	$("#modal_edit_area_kerja button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_area_kerja'); ?>",
			data: $('#modal_edit_area_kerja .form_area_kerja').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_area_kerja").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});
	$("#modal_edit_saldo_cuti button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_saldo_cuti'); ?>",
			data: $('#modal_edit_saldo_cuti .form_saldo_cuti').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_saldo_cuti").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});
	
	$("#modal_edit_jenis_cuti button[type=submit]").click(function(){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('ajax/edit_jenis_cuti'); ?>",
			data: $('.form_jenis_cuti').serialize(),
					success: function(msg){
						$("#status").html(msg)
						$("#modal_edit_jenis_cuti").modal('hide'); 
						location.reload();
					},
			error: function(){
			alert("Gagal");
			}
		});
	});
	
	$(function(){
		$('#data_additional_product').on('click', 'tr', function () {
			
			$('#data_additional_product').find('tr').removeClass('active');
			$(this).addClass('active');
			$('#detail_additional tr').remove();
			$('#kodekar').html("");
			$('#namakar').html("");
			$('#branch_id').html("");

			var dari_tanggal = $('#dari_tanggal').val();
			var sampai_tanggal =  $('#sampai_tanggal').val();
			var kodekar = $(this).attr('kodekar');
			var namakar = $(this).attr('namakar');
			var branch_id = $(this).attr('branch_id');
			
			$.ajax({
				url: "<?php echo site_url('report/data_additional_product_detail'); ?>",
				data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
				type : 'POST',
				dataType: 'JSON',
				success: function(response){	
					var len = response.length;
					var grand_total = 0;

					$.ajax({
						url: "<?php echo site_url('ajax/data_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar,},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							var len = response.length;
							var grand_total = 0;
							$('#kodekar').append(kodekar);
							$('#namakar').append(namakar);

							for(var i=0; i<len; i++){
								var kodeservice = response[i].kodeservice;
								var namaservice = response[i].namaservice;
								var jumlah		= response[i].jumlah;
								var total_komisi = response[i].total_komisi;
								var harga_komisi = response[i].harga_komisi;
							
								var tr_str = "<tr>" +
								//"<td align='left'>" + (i+1) + "</td>" +
								"<td align='center'>" + kodeservice + "</td>" +
								"<td align='left'>" + namaservice + "</td>" +
								"<td align='center'>" + jumlah + "</td>" +
								"<td align='right'>" + harga_komisi+ "</td>" +
								"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
								"</tr>";
								
								$("#detail_additional").prepend(tr_str);
							}
						}
					});

					for(var i=0; i<len; i++){
						var kodeservice = response[i].kodeprod;
						var namaservice = response[i].namaprod;
						var jumlah = response[i].jumlah;
						var komisi = response[i].komisi_trp;
						var komisi_peritem = komisi.toLocaleString(undefined, {maximumFractionDigits:2});
						var total_komisi = jumlah * komisi
						
						grand_total += total_komisi;

						var tr_str = "<tr>" +
						//"<td align='left'>" + (i+1) + "</td>" +
						"<td align='center'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						"<td align='right'>" + komisi_peritem+ "</td>" +
						"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"</tr>";

						$("#detail_additional").append(tr_str);
					}

					$.ajax({
						url: "<?php echo site_url('ajax/get_total_komisi_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar, branch_id: branch_id},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	

							var total_komisi_premium 	= response;
							var grand_total_komisi_add = total_komisi_premium+grand_total;

							var tr2_str = "<tr>" +
								"<td colspan='4' align='right'><strong>Total</strong></td>" +
								"<td align='right'><strong>" + grand_total_komisi_add.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
							"</tr>";

							$("#detail_additional").append(tr2_str);		

						}
					});		
				}
			});
		});
	});

	$(function(){
		$('#data_penjualan_gro').on('click', 'tr', function () {
			
			$('#data_penjualan_gro').find('tr').removeClass('active');
			$(this).addClass('active');
			$('#detail_penjualan_gro tr').remove();
			$('#kodegro').html("");
			$('#branch_id').html("");

			var dari_tanggal = $('#dari_tanggal_gro').val();
			var sampai_tanggal =  $('#sampai_tanggal_gro').val();
			var kodegro = $(this).attr('kodegro');
			var branch_id = $(this).attr('branch_id');
			
			$.ajax({
				url: "<?php echo site_url('report/data_penjualan_gro_detail'); ?>",
				data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
				type : 'POST',
				dataType: 'JSON',
				success: function(response){	
					
					var len = response.length;
					var grand_total = 0;

					for(var i=0; i<len; i++){
						var kodeservice = response[i].kodeprod;
						var namaservice = response[i].namaprod;
						var jumlah = response[i].jumlah;
						var komisi = response[i].komisi_gro;
						var komisi_peritem = komisi.toLocaleString(undefined, {maximumFractionDigits:2});
						var total_komisi = jumlah * komisi;

						//var persen = 29;
						
						var text_q = namaservice.toLowerCase();
						
						if (text_q.match(/hot.*/)) {
							var komisi_peritem = $("#komisi_hotstone").html();
						}

						grand_total += total_komisi;

						var tr_str = "<tr>" +
						//"<td align='left'>" + (i+1) + "</td>" +
					//	"<td align='left'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						//"<td align='right'>" + komisi_peritem+ "</td>" +
						//"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"</tr>";

						$("#detail_penjualan_gro").append(tr_str);
					}

					var tr2_str = "<tr>" +
								"<td colspan='4' align='right'><strong>Total</strong></td>" +
								"<td colspan='2' align='right'><strong>" + grand_total.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
							"</tr>";

					//$("#detail_penjualan_gro").append(tr2_str);
					/*
					$.ajax({
						url: "<?php //echo site_url('ajax/get_total_komisi_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar,},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	

							var total_komisi_premium 	= response;
							var grand_total_komisi_add = total_komisi_premium+grand_total;

							var tr2_str = "<tr>" +
								"<td colspan='4' align='right'><strong>Total</strong></td>" +
								"<td align='right'><strong>" + grand_total_komisi_add.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
							"</tr>";

							$("#detail_penjualan_gro").append(tr2_str);		

						}
					});		
					*/
				}
			});
		});
	});

	$(function(){
		$('#data_rekap_gaji_therapist').on('click', 'tr', function () {
			
			$('#data_rekap_gaji_therapist').find('tr').removeClass('active');
			$(this).addClass('active');
			$('#detail_gaji_therapist tr').remove();
			$('#namakar').html("");
			$('#kodekar').html("");
			$('#kodetrp').html("");	

			$('#total_sesi').html("");		
			$('#total_komisi_lulur').html("");		
			$('#total_point').html("");		
			$('#total_additional').html("");		
			$('#total_komisi_fnb').html("");		
			$('#total_komisi_lain').html("");		

			$('#potongan_yayasan').html("");
			$('#potongan_asuransi').html("");
			$('#potongan_koperasi').html("");
			$('#potongan_mess').html("");
			$('#potongan_zion').html("");
			$('#potongan_denda').html("");
			$('#potongan_hutang').html("");
			$('#potongan_tiket').html("");
			$('#potongan_sertifikasi').html("");
			$('#potongan_trt_glx').html("");
			$('#potongan_bpjs_kry').html("");
			$('#potongan_bpjs_prs').html("");
			$('#total_potongan').html("");
			$('#total_potongan_summary').html("");
			$('#sisa_penghasilan').html("");
			$('#total_penghasilan').html("");
			$('#total_penghasilan_summary').html("");
			$('#jumlah_tabungan').html("");
			$('#total_kuesioner').html("");
			$('#take_homepay').html("");			

			$.ajax({
				url: "<?php echo site_url('ajax/get_notes'); ?>",
				data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal},
				type : 'POST',
				success: function(response){
					$('#label_notes').html(response);

					$('#view_notes').attr('data-tkar', kodekar);
					$('#view_notes').attr('data-from', dari_tanggal);
					$('#view_notes').attr('data-to', sampai_tanggal);
				}
			});
			
			$.ajax({
				url: "<?php echo site_url('ajax/get_total_sesi'); ?>",
				data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id : branch_id},
				type : 'POST',
				dataType: 'JSON',
				success: function(response){
					$('#kodekar').append(kodekar);
					$('#kodetrp').append(kodetrp);
					$('#namakar').append(namakar);
					$('#modal_komisi_lulur').attr('data-tkar', kodekar);
					$('#modal_komisi_lulur').attr('data-from', dari_tanggal);
					$('#modal_komisi_lulur').attr('data-to', sampai_tanggal);

					var total_komisi_premium = response.total_pre*10000;
					var total_komisi_lulur = response.komisi_lulur;
					$('#total_komisi_lulur').html(total_komisi_lulur.toLocaleString(undefined, {maximumFractionDigits:2}));
					//$('#total_komisi_lulur').html(total_komisi_lulur);
					$('#total_sesi').html(response.total_sesi);
					$('#total_point').html(response.total_point);
					//$('#total_premium').html(total_komisi_premium.toLocaleString(undefined, {maximumFractionDigits:2}));
					//$('#total_premium').html(total_komisi_premium);
				
					$.ajax({
						url: "<?php echo site_url('ajax/get_total_add_by_kodekar'); ?>",
						data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id : branch_id},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							var additional = response.total_komisi_additional;
							var komisi_additional = parseInt(additional)+parseInt(total_komisi_premium);
							$('#total_additional').html(parseInt(komisi_additional).toLocaleString(undefined, {maximumFractionDigits:2}));
							$.ajax({
								url: "<?php echo site_url('ajax/get_komisi_fnb_by_kodekar'); ?>",
								data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
								type : 'POST',
								dataType: 'JSON',
								success: function(response){
									if(response.jumlah_komisi==undefined || response.jumlah_komisi=='undefined'){
										var komisi_fnb = 0;
									}else{
										var komisi_fnb = response.jumlah_komisi;
									}			
									//console.log(komisi_fnb);				
									$('#total_komisi_fnb').html(parseInt(komisi_fnb).toLocaleString(undefined, {maximumFractionDigits:2}));
									
									
									$.ajax({
											url: "<?php echo site_url('ajax/get_komisi_lain_by_kodekar'); ?>",
											data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
											type : 'POST',
											dataType: 'JSON',
											success: function(response){
												
												if(response!==null){
													var komisi_lain = response.jumlah_komisi;
												}else{
													var komisi_lain = 0;
												}		
												
												$('#total_komisi_lain').html(parseInt(komisi_lain).toLocaleString(undefined, {maximumFractionDigits:2}));																
												var total_penghasilan = parseInt(komisi_additional) + parseInt(total_komisi_lulur) + parseInt(komisi_fnb) + parseInt(komisi_lain);
												//var total_penghasilan = parseInt(komisi_additional) + parseInt(total_komisi_lulur) + parseInt(komisi_fnb) + parseInt(komisi_lain) + parseInt(komisi_kuesioner);
												$('#total_penghasilan').html(total_penghasilan.toLocaleString(undefined, {maximumFractionDigits:2}));
												$('#total_penghasilan_summary').html(total_penghasilan.toLocaleString(undefined, {maximumFractionDigits:2}));
												
												$.ajax({
													url: "<?php echo site_url('ajax/get_potongan_by_kodekar'); ?>",
													data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id : branch_id},
													type : 'POST',
													dataType: 'JSON',
													success: function(response){

														var yayasan = 0;
														var asuransi = 0;
														var koperasi = 0;
														var mess = 0;
														var tabungan = 0;
														var zion = 0;
														var denda = 0;
														var hutang = 0;
														var tiket = 0;
														var sertifikasi = 0;
														var trt_glx = 0;
														var bpjs_kry = 0;
														var bpjs_prs = 0;

														var a = 0;
														var b = 0;
														var c = 0;
														var d = 0;
														var e = 0;
														var f = 0;
														var g = 0;
														var h = 0;
														var i = 0;
														var j = 0;
														var k = 0;
														var l = 0;
														var m = 0;

														yayasan =  response.yayasan;
														asuransi =  response.asuransi;
														koperasi =  response.koperasi;
														mess =  response.mess;
														tabungan = response.tabungan;
														zion = response.zion;
														denda =  response.denda;
														hutang =  response.hutang;																	
														tiket =  response.tiket;																	
														sertifikasi =  response.sertifikasi;																	
														trt_glx =  response.trt_glx;																	
														bpjs_kry =  response.bpjs_kry;																	
														bpjs_prs =  response.bpjs_prs;																	

														a =  checkisNaN(parseInt(yayasan));
														b =  checkisNaN(parseInt(asuransi));
														c =  checkisNaN(parseInt(koperasi));
														d = checkisNaN(parseInt(mess));
														f =  checkisNaN(parseInt(zion));
														g = checkisNaN(parseInt(denda));
														h = checkisNaN(parseInt(hutang));																								
														i = checkisNaN(parseInt(tiket));																								
														j = checkisNaN(parseInt(sertifikasi));																								
														k = checkisNaN(parseInt(trt_glx));																								
														l = checkisNaN(parseInt(bpjs_kry));																								
														m = checkisNaN(parseInt(bpjs_prs));																								
																											
														var total_potongan_temp = (a+b+c+d+f+g+h+i+j+k+l+m);
														//console.log(a, b, c,d, e, f, g, h);
														var total_potongan = total_potongan_temp;
														
														$('#id_potongan').val(response.id_potongan);
														
														$('#potongan_yayasan').html(formatCurrency(yayasan));
														$('#potongan_asuransi').html(formatCurrency(asuransi));
														$('#potongan_koperasi').html(formatCurrency(koperasi));
														$('#potongan_mess').html(formatCurrency(mess));
														$('#potongan_zion').html(formatCurrency(zion));
														$('#potongan_denda').html(formatCurrency(denda));
														$('#potongan_hutang').html(formatCurrency(hutang));
														$('#potongan_tiket').html(formatCurrency(tiket));
														$('#potongan_sertifikasi').html(formatCurrency(sertifikasi));
														$('#potongan_trt_glx').html(formatCurrency(trt_glx));
														$('#potongan_bpjs_kry').html(formatCurrency(bpjs_kry));
														$('#potongan_bpjs_prs').html(formatCurrency(bpjs_prs));
														$('#total_potongan').html(formatCurrency(total_potongan));
														$('#total_potongan_summary').html(formatCurrency(total_potongan));

														var sisa_penghasilan_temp = total_penghasilan-total_potongan_temp;
														var sisa_penghasilan = (sisa_penghasilan_temp).toLocaleString(undefined, {maximumFractionDigits:2});
														$('#sisa_penghasilan').html(sisa_penghasilan);
														if(sisa_penghasilan_temp >= 1500000 && levelkomisi =='SR'){
															var jtb = 0.3*total_penghasilan;
														}else{
															var jtb = 0;
														}

														$.ajax({
															url: "<?php echo site_url('ajax/save_tabungan'); ?>",
															data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, jumlah_tabungan: jtb, branch_id: branch_id},
															type : 'POST',
															dataType: 'JSON',
															success: function(response){

															}
														});
														
														$.ajax({
															url: "<?php echo site_url('ajax/get_komisi_kuesioner_by_kodekar'); ?>",
															data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
															type : 'POST',
															dataType: 'JSON',
															success: function(response){
																
																var komisi_kuesioner = response;
																
																//$('#total_kuesioner').html(parseInt(komisi_kuesioner).toLocaleString(undefined, {maximumFractionDigits:2}));

																var jumlah_tabungan = (jtb).toLocaleString(undefined, {maximumFractionDigits:2});
																$('#jumlah_tabungan').html('(' + jumlah_tabungan + ')');;
																//console.log(komisi_kuesioner);
																var komisi_kuesioner = parseInt(komisi_kuesioner);
																$('#total_kuesioner').html(komisi_kuesioner.toLocaleString(undefined, {maximumFractionDigits:2}));
																var take_homepay_temp = sisa_penghasilan_temp-jtb+komisi_kuesioner;
																var take_homepay = (take_homepay_temp).toLocaleString(undefined, {maximumFractionDigits:2});
																$('#take_homepay').html(take_homepay);
															}
														});														
													}
												});
											}
										});
								}
							});
						}
					});					
				}
			});

		});
	});
	
	$(function(){
		$('#data_premium_room').on('click', 'tr', function () {
			$('#data_premium_room').find('tr').removeClass('active');
			$(this).addClass('active');
		});
	});

	var date = new Date();
	var end_date = new Date(date.getFullYear(), date.getMonth(), '20', 0);
	var start_date = new Date(date.getFullYear(), date.getMonth()-1, '21', 0);
	var start_date_gro = new Date(date.getFullYear(), date.getMonth()-1, '1', 0);
	var end_date_gro = new Date(date.getFullYear(), date.getMonth(), '0', 0);
	//console.log(start_date_gro);
	//console.log(end_date_gro);
	
	//Datepicker Dashboard Therapist
	$('#bulan_periode').datepicker({
		format: "yyyy-mm",
		autoclose: true,
		startView: "year", 
		minViewMode: "months",
		todayBtn: "linked"
	}).datepicker();
	
	$('#bulan_periode').datepicker({
		//endDate: new Date,
		format: "yyyy-mm",
		autoclose: true,
		todayBtn: "linked",
		startView: "year", 
		minViewMode: "months"
	}).datepicker("setDate",start_date);
	
	$('.datepicker').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker();

	//Date picker
	$('#tanggal_lahir').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker();
	

	//Date picker
    $('#dari_tanggal').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker("setDate",start_date);
	
	//console.log(start_date);
	//console.log(end_date);
    $('#sampai_tanggal').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker("setDate",end_date);

	//Date picker
    $('#dari_tanggal_gro').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker("setDate",start_date_gro);
	
	//console.log(start_date);
	//console.log(end_date);
    $('#sampai_tanggal_gro').datepicker({
		//endDate: new Date,
		format: "yyyy-mm-dd",
		autoclose: true,
		todayBtn: "linked"
    }).datepicker("setDate",end_date_gro);

	$(document).on("click", "#button_filter_rekap_gaji_therapist", function(){
		$('#rekap_gaji_therapist').DataTable().clear().destroy();
		report_rekap_gaji_therapist();
	});
	
	$(document).on("click", "#button_filter_penjualan_gro", function(){
		$('#penjualan_gro').DataTable().clear().destroy();
		report_penjualan_gro();
		data_gro_period();
		data_penerimaan_gaji();
	});

	$(document).on("click", "#button_filter_premium", function(){
		$('#premium_room').DataTable().clear().destroy();
		report_premium_room();
	});
	
	$(document).on("click", "#button_filter_lulur", function(){
		$('#komisi_lulur').DataTable().clear().destroy();
		report_komisi_lulur();
	});
	
	$(document).on("click", "#button_filter_komisi_fnb", function(){
		$('#komisi_fnb').DataTable().clear().destroy();
		report_komisi_fnb_therapist();
	});

	$(document).on("click", "#button_filter_komisi_lain", function(){
		$('#komisi_lain').DataTable().clear().destroy();
		report_komisi_lain_lain();
	});

	$(document).on("click", "#button_filter_komisi_kuesioner", function(){
		$('#komisi_kuesioner').DataTable().clear().destroy();
		report_komisi_kuesioner();
	});

	$(document).on("click", "#button_filter_potongan", function(){
		$('#potongan_therapist').DataTable().clear().destroy();
		report_potongan_therapist();
	});
	
	$(document).on("click", "#button_filter_tabungan", function(){
		$('#tabungan_therapist').DataTable().clear().destroy();
		report_tabungan_therapist();
	});
	
	$(document).on("click", "#button_approval", function(){
		set_approve_registry();
	});

	$(document).on("click", "#button_filter_additional", function(){
		$('#additional_product').DataTable().clear().destroy();
		report_additional_product();
	});

	$(document).on("click", "#button_filter_gaji_spv", function(){
		$('#gaji_spv_therapist').DataTable().clear().destroy();
		gaji_spv_therapist();
	});
	
	
	function set_approve_registry(){
		
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();		
		
		$.ajax({
			url: "<?php echo site_url('ajax/set_approval_registry'); ?>",
			data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
			type : 'POST',
			success: function(response){
				location.reload();
				//$('#label_notes').html(response);
			}
		});
	}

	function report_additional_product(){
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		
		//datatables
		table = $('#additional_product').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
       	 	"ordering": false,
       		"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_additional_product')?>",
				"type": "POST"
			},

			//Set column definition initialisation properties.
			"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": false, //set not orderable
			},
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('namakar', aData[3]);
				$(nRow).attr('branch_id', branch_id);
			}

		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				var new_date1 = moment(dari_tanggal).format('DD/MM/YYYY');
				var new_date2 = moment(sampai_tanggal).format('DD/MM/YYYY');
				$('#branch_name').html(response);
				$('#periode').html(new_date1 + ' - ' + new_date2);
			}
		});

		$('#button_approval').attr('dari_tanggal', dari_tanggal);
		$('#button_approval').attr('sampai_tanggal', sampai_tanggal);
		$('#button_approval').attr('branch_code', branch_id);
		
	};
	
	function data_spv_therapist(){
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();

		$.ajax({
			url: "<?php echo site_url('ajax/data_spv_therapist')?>",
			data: {"branch_id" : branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var len = response.length;
				for(var i=0; i<len; i++){
					var id_spv = response[i].id_spv;
					var nama_spv = response[i].nama_spv;
					var jabatan = response[i].is_mess;
					var gajibulan = parseInt(response[i].gajibulan);
					var no_rek = parseInt(response[i].no_rek);
					var no_telp = parseInt(response[i].no_rek);
					var no = i+1;		
					
					if(jabatan==0){
						var jabatan = "SPV Therapist";
					}else{ 
						var jabatan = "Ibu Mess";
					}
					
					if(no_rek==0){
						var no_rek = "-";
					}else{ 
						var no_rek = no_rek;
					}
					
					if(no_telp==0){
						var no_telp = "-";
					}else{ 
						var no_telp = no_telp;
					}
					
					var gajibulan = (gajibulan).toLocaleString(undefined, {maximumFractionDigits:2});

					var tr_str = "<tr id_spv='" +  id_spv + "'>" +
								"<td align='center'>" + no + "</td>" +
								"<td align='left'>" + nama_spv + "</td>" +
								//"<td align='center'>" + no_rek + "</td>" +
								"<td align='left'>" + jabatan + "</td>" +
								//"<td align='center'>" + no_telp + "</td>" +
								"<td align='center'>" +
									"<div class='btn-group' style='display: -webkit-inline-box'>" +
										"<button class='btn btn-xs btn-primary btn-edit'  id_spv='" + id_spv + "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>" +
										"<button class='btn btn-xs btn-danger btn-delete'  id_spv='" + id_spv + "'><i class='fa fa-trash' aria-hidden='true'></i></button>" +
									"</div>" +
								"</td>" +
								"</tr>";

					$("#data_spv_therapist").append(tr_str);
					
				}
			}
		});
	};

	function hitung_gaji_spv_therapist(){
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();

		$.ajax({
			url: "<?php echo site_url('ajax/hitung_gaji_spv_therapist')?>",
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){
				console.log('Data Sukses Dimasukkan');
			}
		});

		location.reload();
	}


	function gaji_spv_therapist(){
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();

		$.ajax({
			url: "<?php echo site_url('ajax/data_gaji_spv_therapist')?>",
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){
				//$("#hitung_gaji_spv_therapist").hide();
				var len = response.length;
				for(var i=0; i<len; i++){
					var id_gaji = response[i].id_gaji;
					var nama_spv = response[i].nama_spv;
					var jabatan = response[i].is_mess;
					var gaji_pokok = parseInt(response[i].gaji);
					var kuesioner = parseInt(response[i].kuesioner);
					var total_gaji = parseInt(response[i].total_gaji);
					var no = i+1;		
					
					if(jabatan==0){
						var jabatan = "SPV Therapist";
					}else{ 
						var jabatan = "Ibu Mess";
					}
					
					var gaji_pokok = (gaji_pokok).toLocaleString(undefined, {maximumFractionDigits:2});
					var kuesioner = (kuesioner).toLocaleString(undefined, {maximumFractionDigits:2});
					var total_gaji = (total_gaji).toLocaleString(undefined, {maximumFractionDigits:2});

					var tr_str = "<tr>" +
								"<td align='left'>" + no + "</td>" +
								"<td align='left'>" + nama_spv + "</td>" +
								"<td align='center'>" + jabatan + "</td>" +
								"<td align='left'>" + gaji_pokok + "</td>" +
								"<td align='left'>" + kuesioner + "</td>" +
								"<td align='left'>" + total_gaji + "</td>" +
								"<td align='center'>" +
									"<div class='btn-group' style='display: -webkit-inline-box'>" +
											"<button class='btn btn-xs btn-primary btn-edit'  id_gaji='" + id_gaji + "'>Edit</button>" +
										"</div>" +
								"</td>" +
								"</tr>";
					
					
					$("#data_gaji_spv_therapist").append(tr_str);										
					
				}
				
				var tr_str2 = "<tr>" +
								"<td colspan='7' align'center'>Data Tidak Tersedia</td>" +
								"</tr>";
					
				if(response.length==0){
					//$("#hitung_gaji_spv_therapist").show();
					$("#data_gaji_spv_therapist").append(tr_str2);
					
				}
			}
		});


		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				var new_date1 = moment(dari_tanggal).format('DD/MM/YYYY');
				var new_date2 = moment(sampai_tanggal).format('DD/MM/YYYY');
				$('#branch_name').html(response);
				$('#periode').html(new_date1 + ' - ' + new_date2);
			}
		});

		$('#gaji_spv_therapist').attr('branch_id', branch_id);
		
	};
	
	function report_penjualan_gro(){
		var dari_tanggal = $('#dari_tanggal_gro').val();
		var sampai_tanggal = $('#sampai_tanggal_gro').val();
		var branch_id = $('#branch_id').val();
		
		/*
		//datatables
		table = $('#penjualan_gro').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
       	 	"ordering": false,
       		"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "//echo site_url('report/data_penjualan_gro'),
				"type": "POST"
			},

			//Set column definition initialisation properties.
			"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": false, //set not orderable
			},
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('namakar', aData[3]);
				$(nRow).attr('branch_id', branch_id);
			}

		});	
		
		*/

		$.ajax({
			url: "<?php echo site_url('report/data_penjualan_gro')?>",
			data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var len = response.length;

				for(var i=0; i<len; i++){
					var kodegro = response[i].kodegro;
					var namagro = response[i].namagro;
					
					var tr_str = "<tr kodegro='" +  kodegro + "' branch_id='" +  branch_id + "'>" +
								"<td align='left'>" + (i+1) + "</td>" +
								"<td align='left'>" + kodegro + "</td>" +
								"<td align='left'>" + namagro + "</td>" +
								"</tr>";

					$("#data_penjualan_gro").append(tr_str);
					
				}
			}
		});
		
		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				var new_date1 = moment(dari_tanggal).format('DD/MM/YYYY');
				var new_date2 = moment(sampai_tanggal).format('DD/MM/YYYY');
				$('#branch_name').html(response);
				$('#periode').html(new_date1 + ' - ' + new_date2);
			}
		});
		
		var date_r = dari_tanggal.split('-');
		var dateInMonth = new Date(date_r[0], date_r[1], 0).getDate();
		$('#total_hari_target').html(dateInMonth);		
		var target_day = $('#target_day').html();
		var target_month = $('#total_hari_target').html();
		var target_per_month = target_day*target_month;
		$('#target_mud').html(target_per_month);

		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_mud')?>",
			type: "POST",
			success:function(response){
				$('#total_mud').html(response);
			}
		});			
		
		var total_mud = $('#total_mud').html();

		if(total_mud<=target_per_month){
			var status = '<label data-status="0" class="label label-danger">Tidak Tercapai</label>';
			$('#status_target_mud').html(status);
		}else{
			var status = '<label data-status="1" class="label label-success">Tercapai</label>';
			$('#status_target_mud').html(status);
		}
		
		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_treatment')?>",
			type: "POST",
			success:function(response){
				//console.log(console);
				$('#total_treatment').html(response);
			}
		});	

		$.ajax({
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_hotstone')?>",
			type: "POST",
			success:function(response){
				$('#total_hotstone').html(response);
				var total_hotstone = $('#total_hotstone').html();	
				var total_treatment = $('#total_treatment').html();
				
				var persen_hotstone = total_hotstone*100/total_treatment;

				var new_persen = Math.floor(persen_hotstone);
				
				$('#persen_hotstone').html(new_persen + '%');		

				if(new_persen=>25 && new_persen<=29){
					var status = 'komisi_1';
					//$('#komisi_hotstone').html(2500);	
				}else if(new_persen=>30 && new_persen<=34){
					var status = 'komisi_1';
					//$('#komisi_hotstone').html(4000);
				}else if(new_persen=>35){
					var status = 'komisi_1';
					//$('#komisi_hotstone').html(6000);
				}

				$.ajax( {
					data: {"status" : status},
					url:"<?php echo site_url('ajax/komisi_hotstone')?>",
					type: "POST",
					success:function(response){
						var komisi_hot_tbl = parseInt(response);
						//console.log(komisi_hot_tbl);
						$('#komisi_hotstone').html(komisi_hot_tbl);						

						var total_hot_tbl 			= $('#total_hotstone').html();	
						var total_komisi_hot_tbl 	= total_hot_tbl*komisi_hot_tbl;				
						$('#total_hot_tbl').html(total_hot_tbl);
						$('#komisi_hot_tbl').html(komisi_hot_tbl);
						$('#total_komisi_hot_tbl').html(parseFloat(Math.round((total_komisi_hot_tbl) * 100) / 100).toLocaleString());
						
						$('#total_komisi_gro').html(total_komisi_hot_tbl);
					}
				});				
			}
		});	

		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_aroma')?>",
			type: "POST",
			success:function(response){
				var total_aroma_tbl = response;	

				$.ajax( {
					url:"<?php echo site_url('ajax/komisi_aroma')?>",
					type: "POST",
					success:function(response){
						var komisi_aroma_tbl = parseInt(response);
						$('#komisi_hotstone').html(komisi_aroma_tbl);						
						var total_komisi_aroma_tbl 	= total_aroma_tbl*komisi_aroma_tbl;				
						$('#total_aroma_tbl').html(total_aroma_tbl);
						$('#komisi_aroma_tbl').html(komisi_aroma_tbl);
						$('#total_komisi_aroma_tbl').html(parseFloat(Math.round((total_komisi_aroma_tbl) * 100) / 100).toLocaleString());

						var komisi_gro_temp = parseInt($('#total_komisi_gro').html());
						var komisi_gro = komisi_gro_temp+total_komisi_aroma_tbl;
						$('#total_komisi_gro').html(komisi_gro);

					}
				});		
			}
		});
		
		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_mud')?>",
			type: "POST",
			success:function(response){
				var total_mud_tbl 			= response;	

				$.ajax( {
					url:"<?php echo site_url('ajax/komisi_mud')?>",
					type: "POST",
					success:function(response){
						var komisi_mud_tbl = parseInt(response);
						$('#komisi_mud').html(komisi_mud_tbl);						
						var total_komisi_mud_tbl 	= total_mud_tbl*komisi_mud_tbl;				
						$('#total_mud_tbl').html(total_mud_tbl);
						$('#komisi_mud_tbl').html(komisi_mud_tbl);
						$('#total_komisi_mud_tbl').html(parseFloat(Math.round((total_komisi_mud_tbl) * 100) / 100).toLocaleString());

						var komisi_gro_temp = parseInt($('#total_komisi_gro').html());
						var komisi_gro = komisi_gro_temp+total_komisi_mud_tbl;
						$('#total_komisi_gro').html(komisi_gro);
						//$('#total_komisi_gro').html(komisi_gro.toLocaleString());
					}
				});	
			}
		});
		
		komisi_gro_treatment();
		
		//$('#button_approval').attr('sampai_tanggal', sampai_tanggal);
		//$('#button_approval').attr('branch_code', branch_id);
		
	};
	
	function report_rekap_gaji_therapist(){

		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		
		$('#button_approval').remove();

		$('#detail_gaji_therapist tr').remove();
		$('#namakar').html("");
		$('#kodekar').html("");
		$('#kodetrp').html("");

		$('#detail_gaji_therapist tr').remove();
		$('#kodetrp').html("");
		$('#total_sesi').html("");
		$('#total_point').html("");
		$('#total_komisi_lulur').html("");
		$('#total_additional').html("");
		$('#total_komisi_fnb').html("");
		$('#total_komisi_lain').html("");
		$('#potongan_yayasan').html("");
		$('#potongan_asuransi').html("");
		$('#potongan_koperasi').html("");
		$('#potongan_mess').html("");
		$('#potongan_zion').html("");
		$('#potongan_denda').html("");
		$('#potongan_hutang').html("");
		$('#potongan_tiket').html("");
		$('#potongan_sertifikasi').html("");
		$('#potongan_trt_glx').html("");
		$('#potongan_bpjs_kry').html("");
		$('#potongan_bpjs_prs').html("");
		$('#total_potongan').html("");
		$('#total_penghasilan').html("");
		$('#total_penghasilan_summary').html("");
		$('#total_potongan_summary').html("");
		$('#sisa_penghasilan').html("");
		$('#jumlah_tabungan').html("");
		$('#total_kuesioner').html("");
		$('#take_homepay').html("");
		
		//datatables
		table = $('#rekap_gaji_therapist').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
       	 	"ordering": false,
       		"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			//Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_rekap_gaji_therapist')?>",
				"type": "POST"
			},

			//Set column definition initialisation properties.
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
					{ "width": "20px",  "className": "text-center",  "targets": 0 },
					{ "width": "40px",  "className": "text-center", "targets": 1 },
					{ "width": "160px", "targets": 2 },
					{ "width": "80px", "className": "text-center",  "targets": 3 },
					{ "width": "100px", "className": "text-center", "targets": 4 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[2]);
				$(nRow).attr('kodetrp', aData[1]);
				$(nRow).attr('namakar', aData[3]);
				$(nRow).attr('levelkomisi', aData[4]);
				$(nRow).attr('branch_id', branch_id);
				$(nRow).attr('dari_tanggal', dari_tanggal);
				$(nRow).attr('sampai_tanggal', sampai_tanggal);
				$('td:eq(2)', nRow).html(aData[3] );
				$('td:eq(3)', nRow).html(aData[4] );
				$('td:eq(4)', nRow).html(aData[5] );
			}

		});	

		$.ajax({
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				var new_date1 = moment(dari_tanggal).format('DD/MM/YYYY');
				var new_date2 = moment(sampai_tanggal).format('DD/MM/YYYY');
				$('#branch_name').html(response);
				$('#branch_code').val(branch_id);
				$('#periode').html(new_date1 + ' - ' + new_date2);
				
				var url = '<?php echo base_url(); ?>report/print_rekapan_gaji_all?branch_code=' + branch_id + '&from_date=' + dari_tanggal + '&to_date=' + sampai_tanggal;
				$("#btn_print_rekapan_gaji").attr("href", url);
			}
		});

		$.ajax({
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_approval_registry')?>",
			type: "POST",
			success:function(response){
				if(response==1){
					$("#status_approve").append('<button style="margin-right: 10px;" title="Approved" class="btn btn-md btn-success" dari_tanggal="' + dari_tanggal + '" sampai_tanggal="' + sampai_tanggal + '" branch_code="' + branch_id + '"><i class="fa fa-check"></i> Approved</button>');
				}else{
					$("#status_approve").append('<button style="margin-right: 10px;" id="button_approval" title="Set Approve" class="btn btn-md btn-danger" dari_tanggal="' + dari_tanggal + '" sampai_tanggal="' + sampai_tanggal + '" branch_code="' + branch_id + '"><i class="fa fa-close"></i> Set Approve</button>');
				}
			}
		});
		
	};	

	function report_premium_room(){
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		
		//datatables
		table = $('#premium_room').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
       	 	"ordering": false,
       		"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_premium_room')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "20px",  "className": "text-center",  "targets": 0 },
				{ "width": "40px",  "className": "text-center", "targets": 1 },
				{ "width": "200px", "targets": 2 },
				{ "width": "80px", "className": "text-center",  "targets": 3 },
				{ "width": "100px", "className": "text-right", "targets": 4 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			}

		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});
		/*
		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
			url:"<?php //echo site_url('ajax/summary_harian')?>",
			type: "POST",
			success:function(response){
				$('#summary_harian').html(response);
			}
		});
		*/
		
	};

	function report_komisi_lulur(){

		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		
		//datatables
		table = $('#komisi_lulur').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
       	 	"ordering": false,
       		"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_komisi_lulur')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "20px",  "className": "text-center",  "targets": 0 },
				{ "width": "40px",  "className": "text-center", "targets": 1 },
				{ "width": "300px", "className": "text-left",  "targets": 2 },
				{ "width": "50px", "className": "text-center",  "targets": 3 },
				{ "width": "50px", "className": "text-center", "targets": 4 },
				{ "width": "50px", "className": "text-center", "targets": 5 },
				{ "width": "50px",  "className": "text-center", "targets": 6 },
				{ "width": "50px",  "className": "text-center", "targets": 7 },
				{ "width": "50px",  "className": "text-center", "targets": 8 },
				{ "width": "50px",  "className": "text-center", "targets": 9 },
				{ "width": "50px",  "className": "text-center", "targets": 10 },
				{ "width": "120px",  "className": "text-right text-bold", "targets": 11 },
				{ "width": "120px",  "className": "text-right text-bold", "targets": 12 },
				{ "width": "120px",  "className": "text-right text-bold", "targets": 13 }
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			}

	});	

	$.ajax( {
		data: {"branch_id" : branch_id},
		url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
		type: "POST",
		success:function(response){
			$('#branch_name').html(response);
		}
	});
};

function report_potongan_therapist(){

	var dari_tanggal = $('#dari_tanggal').val();
	var sampai_tanggal = $('#sampai_tanggal').val();
	var branch_id = $('#branch_id').val();
	//datatables
	table = $('#potongan_therapist').DataTable({ 
		
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"order": [], //Initial no order.
		"pageLength": 20,
		"paging":   false,
		"ordering": false,
		"info":     false,
		"searching": false,
		"lengthMenu": [ 5,10, 50, 50, 75, 100 ],
		"responsive": true,

		// Load data for the table's content from an Ajax source
		"ajax": {
			"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			"url": "<?php echo site_url('report/data_potongan_therapist')?>",
			"type": "POST"
		},
		
		"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": false, //set not orderable
			},
			{ "width": "15px",  "className": "text-center",  "targets": 0 },
			{ "width": "20px",  "className": "text-center", "targets": 1 },
			{ "width": "150px", "className": "text-left",  "targets": 2 },
			{ "width": "60px", "className": "text-right",  "targets": 3 },
			{ "width": "50px", "className": "text-right", "targets": 4 },
			{ "width": "50px",  "className": "text-right", "targets": 5 },
			{ "width": "50px",  "className": "text-right", "targets": 6 },
			{ "width": "50px",  "className": "text-right", "targets": 7 },
			{ "width": "50px",  "className": "text-right", "targets": 8 },
			{ "width": "30px",  "className": "text-right text-bold", "targets": 9 },
			{ "width": "30px",  "className": "text-center text-bold", "targets": 10 },
		],
		"fnCreatedRow": function( nRow, aData, iDataIndex ) {
			$(nRow).attr('kodekar', aData[1]);
			$(nRow).attr('branch_id', aData[1]);
			}
		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});

	};

function report_tabungan_therapist(){

	var dari_tanggal = $('#dari_tanggal').val();
	var sampai_tanggal = $('#sampai_tanggal').val();
	var branch_id = $('#branch_id').val();
	//datatables
	table = $('#tabungan_therapist').DataTable({ 
		
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"order": [], //Initial no order.
		"pageLength": 20,
		"paging":   false,
		"ordering": false,
		"info":     false,
		"searching": false,
		"lengthMenu": [ 5,10, 50, 50, 75, 100 ],
		"responsive": true,

		// Load data for the table's content from an Ajax source
		"ajax": {
			"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			"url": "<?php echo site_url('report/data_tabungan_therapist')?>",
			"type": "POST"
		},
		
		"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": false, //set not orderable
			},
			{ "width": "15px",  "className": "text-center",  "targets": 0 },
			{ "width": "20px",  "className": "text-center", "targets": 1 },
			{ "width": "150px", "className": "text-left",  "targets": 2 },
			{ "width": "150px",  "className": "text-right", "targets": 3 },
			{ "width": "150px",  "className": "text-right text-bold", "targets": 4 },
		],
		"fnCreatedRow": function( nRow, aData, iDataIndex ) {
			$(nRow).attr('kodekar', aData[1]);
			$(nRow).attr('branch_id', aData[1]);
			}
		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});

	};

	function report_komisi_fnb_therapist(){

		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		//datatables
		table = $('#komisi_fnb').DataTable({ 
			
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 5,10, 50, 50, 75 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_komisi_fnb')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "15px",  "className": "text-center",  "targets": 0 },
				{ "width": "20px",  "className": "text-center", "targets": 1 },
				{ "width": "150px", "className": "text-left",  "targets": 2 },
				{ "width": "80px", "className": "text-right text-bold",  "targets": 3 },
				{ "width": "50px", "className": "text-center", "targets": 4 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			}
		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});

	};

	function report_komisi_lain_lain(){

		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		//datatables
		table = $('#komisi_lain').DataTable({ 
			
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 5,10, 50, 50, 75 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_komisi_lain')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "15px",  "className": "text-center",  "targets": 0 },
				{ "width": "20px",  "className": "text-center", "targets": 1 },
				{ "width": "150px", "className": "text-left",  "targets": 2 },
				{ "width": "80px", "className": "text-right text-bold",  "targets": 3 },
				{ "width": "50px", "className": "text-center", "targets": 4 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			}
		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});

	};

	function report_komisi_kuesioner(){

		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		var branch_id = $('#branch_id').val();
		//datatables
		table = $('#komisi_kuesioner').DataTable({ 
			
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 5,10, 50, 50, 75 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/data_komisi_kuesioner')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "15px",  "className": "text-center",  "targets": 0 },
				{ "width": "20px",  "className": "text-center", "targets": 1 },
				{ "width": "150px", "className": "text-left",  "targets": 2 },
				{ "width": "80px", "className": "text-right text-bold",  "targets": 3 },
				{ "width": "50px", "className": "text-center", "targets": 4 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			}
		});	

		$.ajax( {
			data: {"branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_branch_name_by_branch_id')?>",
			type: "POST",
			success:function(response){
				$('#branch_name').html(response);
			}
		});

	};

	function list_data_therapist(){
		//datatables
		table = $('#data_therapist').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   true,
			"ordering": true,
			"info":     true,
			"searching": true,
			"lengthMenu": [ 10, 20, 140,80, 60, 100, 100, 100, 80, 120 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				//"data": {"branch_id" : branch_id},
				"url": "<?php echo site_url('data_therapist/list_data_therapist')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "10",  "className": "text-center", "targets": 0 },
				{ "width": "20",  "className": "text-center", "targets": 1 },
				{ "width": "300", "targets": 2 },
				{ "width": "300", "className": "text-center", "targets": 3 },
				{ "width": "40px",  "className": "text-center", "targets": 4 },
				{ "width": "100px", "className": "text-center", "targets": 5 },
				{ "width": "100px", "className": "text-center", "targets": 6 },
				{ "width": "100px", "className": "text-center", "targets": 7 },
				{ "width": "80px",  "className": "text-center", "targets": 8 },
				{ "width": "80px", "className": "text-center", "targets": 9 },
				{ "width": "120px", "className": "text-center", "targets": 10 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
			}
		});	
		
	};

	function list_data_divisi(){
		$('#data-divisi').DataTable({
			"columnDefs": [
				{ "orderable": false, "targets": 2, "searchable": false }
			],
		});
	}

	function list_data_pembelian(){
		$('#list-pembelian').DataTable({
			"columnDefs": [
				{ "orderable": false, "targets": 7, "searchable": false }
			]
		});
	}

	function list_data_users(){
		//datatables
		table = $('#data_users').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 30,
			"paging":   true,
			"ordering": true,
			"info":     true,
			"searching": true,
			"lengthMenu": [ 10, 60, 100,80, 60, 60 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				//"data": {"branch_id" : branch_id},
				"url": "<?php echo site_url('master/list_data_users')?>",
				"type": "POST"
			},
			
			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "10",  "className": "text-center", "targets": 0, "searchable": false, "orderable": false},
				{ "width": "50",  "className": "text-center", "targets": 1, "searchable": false, "orderable": false},
				{ "width": "100", "targets": 2, "searchable": false, "orderable": false},
				{ "width": "100", "className": "text-center", "targets": 3, "searchable": false, "orderable": false},
				{ "width": "60",  "className": "text-center", "targets": 4, "searchable": false, "orderable": false},
				{ "width": "40", "className": "text-center", "targets": 5, "searchable": false, "orderable": false},
				{ "width": "40", "className": "text-center", "targets": 6, "searchable": false, "orderable": false },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('user_id', aData[1]);
			}
		});	
		
	};

	function kategori_cuti(){
		
		$(document).ready(function(){
			$.ajax({
				url:"<?php echo site_url('ajax/get_category')?>",
				method:"POST",
				dataType:"json",
				success:function(response){
					$("#kategori_cuti").html("");
					var str = "<option value='0'>- Pilih Kategori Ketidakhadiran -</option>";
					$("#kategori_cuti").append(str);
					var len = response.length;

					for(var i=0; i<len; i++){
						var id_category = response[i].id_category;
						var category_name = response[i].category_name;
						var str2 = "<option value='" + id_category + "'>" + category_name +"</option>";
						$("#kategori_cuti").append(str2);
					}
				}
			});			
		});
	};

	function user_roles(){
		
		$(document).ready(function(){
			$.ajax({
				url:"<?php echo site_url('ajax/get_user_roles')?>",
				method:"POST",
				dataType:"json",
				success:function(response){
					$(".user_roles").html("");
					var str = "<option selected'>- Pilih Roles -</option>";
					$(".user_roles").append(str);
					var len = response.length;

					for(var i=0; i<len; i++){
						var id_jabatan = response[i].id_jabatan;
						var nama_jabatan = response[i].nama_jabatan;
						var str2 = "<option value='" + id_jabatan + "'>" + nama_jabatan +"</option>";
						$(".user_roles").append(str2);
					}
				}
			});			
		});
	};

	function sub_cuti_tahunan(){
		
		$(document).on('change', '#kategori_cuti', function(){
			var sub_cuti = $(this).val();
			//console.log(sub_cuti);
			var id_kar = $(this).val();
			$('#btn_submit_cuti').removeAttr('disabled', true);
			if(sub_cuti==2){
				$.ajax({
					data: {"id_kar" : id_kar},
					url:"<?php echo site_url('ajax/get_saldo_cuti')?>",
					method:"POST",
					dataType:"json",
					success:function(response){
						var sisa_cuti = response;
						$("#sub_cuti_list").html("");
						$("#sub_cuti_label").html("");
						var str1 = "<h4><span class='label label-lg label-info'>" + sisa_cuti + " Hari</span></h4>";
						$("#sub_cuti_label").html("Saldo Cuti");
						$("#sub_cuti_list").append(str1);
						if(sisa_cuti<1){
							$('#btn_submit_cuti').attr('disabled', true);
						}
					}
				});
			}else{
				$("#sub_cuti_label").html("");
				$("#sub_cuti_list").html("");
			}
				
		});
	};
	
	function report_filter_detail(){
		
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		
		//datatables
		table = $('#data-detail-penjualan').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
				"url": "<?php echo site_url('report/ajax_detail_penjualan')?>",
				"type": "POST"
			},

			//Set column definition initialisation properties.
			"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": true, //set not orderable
				"className": "text-right",
				"targets": [ 6, 9, 10, 11 ]
			},
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', aData[2]);
			}

		});		
	};
	
	function rekap_penjualan(){
		
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		
		//datatables
		table = $('#data-rekap-penjualan').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
				"url": "<?php echo site_url('report/ajax_rekap_penjualan')?>",
				"type": "POST"
			},

			//Set column definition initialisation properties.
			/*
			"columnDefs": [
			{ 
				"targets": [ 0 ], //first column / numbering column
				"orderable": true, //set not orderable
				"className": "text-right",
				"targets": [ 6, 9, 10, 11 ]
			},		
			],
			*/
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', aData[2]);
			}

		});		
	};
	
	function sales_average(){
		
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		
		//datatables
		table = $('#data-sales-average').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"pageLength": 20,
			"lengthMenu": [ 5, 20, 30, 75 ],
			"responsive": true,
			"filter": false,
			
			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
				"url": "<?php echo site_url('ajax/sales_average')?>",
				"type": "POST",
			},
			
			"columns": [
		          { "data": "No" },
		          { "data": "Tanggal" },
		          { "data": "TotalBill" },
		          { "data": "AvgSales" },
			],	

			//Set column definition initialisation properties.
			"columnDefs": [
						{ 
							"className": "text-center",
							"targets": [ 2 ]
						},
						{ 
							"className": "text-right",
							"targets": [ 3 ]
						},
				],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', aData[2]);
			}
			
		});		
	};
	
	function ranking_penjualan(){
		
		var dari_tanggal = $('#dari_tanggal').val();
		var sampai_tanggal = $('#sampai_tanggal').val();
		
		//datatables
		table = $('#data-ranking-penjualan').DataTable({ 
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 100,
			"paging":   false,
			"info":     false,
			"searching": false,
			"ordering": false,
			"responsive": true,
			"columnDefs": [
				{ "width": "20px", "targets": 0 },
				{ "width": "250px", "targets": 1 },
				{ "width": "30px", "targets": 2 },
				{ "width": "100px", "targets": 3 },
				{ "className": "text-center", "targets": [2]},
				{ "className": "text-right", "targets": [3]},
				{ "className": "dt-nowrap", "targets": [3]}
			],			

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal},
				"url": "<?php echo site_url('report/ajax_ranking_penjualan')?>",
				"type": "POST",
				"dataSrc": function (json){
					var return_data = new Array();
					var total_draft = 0;
					for(var i=0;i< json.data.length; i++){
						return_data.push({
						  'no': json.data[i][0],
						  'name'  : json.data[i][1],
						  'qty' : json.data[i][2],
						  //'total' : json.data[i][3],
						  'total' : parseFloat(Math.round((json.data[i][3]) * 100) / 100).toLocaleString(),
						  'total_draft' : total_draft + json.data[i][3],
						});
						total_draft = total_draft + json.data[i][3];
						total_draft = total_draft;
					}
					return return_data;
				},
			},		
			"columns"    : [
				{'data': 'no'},   //Column1
				{'data': 'name'}, //Column2
				{'data': 'qty'},   //Column3
				{'data': 'total'}   //Column4
			],	
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;
	 
				// converting to interger to find total
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	 
				// computing column Total of the complete result 
				var QtyTotal = api
					.column( 2 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );
					
				var TotalAll = api
					.column(3)
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					}, 0 );
				
				var return_data2 = new Array();
				var total_all = 0;
				//console.log(data.total_draft);
				for(var n=0;n< data.length; n++){
					total_all = data[n].total_draft;
					//console.log(total_all);
				}
				
				// Update footer by showing the total with the reference of the column index 
				$( api.column( 1 ).footer() ).html('Total');
				$( api.column( 2 ).footer() ).html(QtyTotal);
				$( api.column( 3 ).footer() ).html('<b>' + parseFloat(Math.round(total_all * 100) / 100).toLocaleString() + '</b>');
				//$( api.column( 3 ).footer() ).html((TotalAll.toLocaleString({minimumFractionDigits: 2})));
			},
			
			//Set column definition initialisation properties.
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('id', aData[2]);
			},
			"createdRow": function ( row, data, index ) {
			if ( data[5].replace(/[\$,]/g, '') * 1 > 150000 ) {
				$('td', row).eq(5).addClass('highlight');
				}
			}
		});
	};
		
		
	
	$(document).ready(function() {

		/*
		$('#filter_dashboard_therapist #dari_tanggal').datepicker({
			endDate: new Date,
			format: "yyyy-mm-dd",
			autoclose: true,
			todayBtn: "linked"
		}).datepicker("setDate",'now');
		
		$('#filter_dashboard_therapist #sampai_tanggal').datepicker({
			endDate: new Date,
			format: "yyyy-mm-dd",
			autoclose: true,
			todayBtn: "linked"
		}).datepicker("setDate",'now');
		*/
		
		$(document).on("click", "#button_filter_dashboard", function(){
			//filter_dashboard_therapist();
			summary_gaji_therapist_periode();
		});

		$(document).on("click", ".insert_note", function(){
			var kodekar = $('#kodekar').html();
			var namakar = $('#namakar').html();
			var dari_tanggal = $('#modal_komisi_lulur').attr('data-from');
			var sampai_tanggal = $('#modal_komisi_lulur').attr('data-to');
			modal_insert_note(kodekar, namakar, dari_tanggal, sampai_tanggal);
		});
		$(document).on("click", "#modal_komisi_lulur", function(){
			modal_komisi_lulur();
		});

		$(document).on("click", "#modal_total_sesi", function(){
			modal_komisi_lulur();
		});

		$(document).on("click", "#modal_total_point", function(){
			modal_komisi_lulur();
		});
		
		$(document).on("click", "#modal_komisi_additional", function(){
			modal_komisi_additional();
		});

		//Modal Edit For FNB
		$(document).on("click", "#komisi_fnb .btn-edit", function(){
			var kodekar = $(this).attr('kodekar');
			var id_fnb = $(this).attr('data-id');
			modal_komisi_fnb(kodekar, id_fnb);
		});

		//Modal Edit For Data SPV Therapist
		$(document).on("click", "#data_spv_therapist .btn-edit", function(){
			var id_spv = $(this).attr('id_spv');
			modal_data_spv_therapist(parseInt(id_spv));
		});
		//Modal Edit For Data SPV Therapist
		$(document).on("click", "#data_spv_therapist .btn-delete", function(){
			var id_spv = $(this).attr('id_spv');
			//modal_data_spv_therapist(parseInt(id_spv));
			swal({
                title: "Are you sure?",
                text: " ",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            	},
				function () {
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('ajax/delete_spv_therapist'); ?>",
						data: {"id_spv" : id_spv},
						success: function(msg){
							swal("Deleted!", "Delete Success", "success");
							location.reload();
						},
						error: function(){
							swal("Error", "Delete Failed", "error");
						}
					});    	               
           		}
			);
		});

		//Modal Edit For Data SPV Therapist
		$(document).on("click", "#tambah_spv_therapist", function(){
			modal_tambah_data_spv_therapist();
		});
		
		$(document).on("click", "#hitung_gaji_spv_therapist", function(){
			hitung_gaji_spv_therapist();
		});
		
		//Modal Edit For Data SPV Therapist
		$('#data_gaji_spv_therapist').on('click', '.btn-edit', function () {
			var id_gaji = $(this).attr('id_gaji');
			var dari_tanggal =  $('#dari_tanggal').val();
			var sampai_tanggal =  $('#sampai_tanggal').val();
			modal_edit_gaji_spv_therapist(id_gaji, dari_tanggal, sampai_tanggal);
		});

		//Modal Edit For FNB
		$(document).on("click", "#komisi_fnb .btn-view", function(){
			var kodekar = $(this).attr('kodekar');
			var dari_tanggal = $(this).attr('dari_tanggal');
			var sampai_tanggal = $(this).attr('sampai_tanggal');
			modal_view_komisi_fnb(kodekar, dari_tanggal, sampai_tanggal);
		});

		$(document).on("click", "	#modal_komisi_fnb", function(){
			var kodekar =  $('#rekap_gaji_therapist tr.active').attr('kodekar');
			var dari_tanggal =  $('#rekap_gaji_therapist tr.active').attr('dari_tanggal');
			var sampai_tanggal =  $('#rekap_gaji_therapist tr.active').attr('sampai_tanggal');
			modal_view_komisi_fnb(kodekar, dari_tanggal, sampai_tanggal);
		});

		$("#modal_edit_komisi_fnb button[type=submit]").click(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('report/update_komisi_fnb'); ?>",
				data: $('form.form_komisi_fnb').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_komisi_fnb").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});		

		$("#modal_insert_notes button[type=submit]").click(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('ajax/insert_notes'); ?>",
				data: $('form.form_insert_notes').serialize(),
				success: function(msg){
						$("#status").html(msg)
						$("#modal_insert_notes").modal('hide'); 
						location.reload();
				},
				error: function(){
				alert("Gagal");
				}
			});
		});
		
		$(document).on("click", "#view_notes", function(){
			
			var kodekar = $(this).attr('data-tkar');
			var dari_tanggal = $(this).attr('data-from');
			var sampai_tanggal = $(this).attr('data-to');

			$.ajax({
				type: "POST",
				data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal": sampai_tanggal, "kodekar": kodekar},
				url: "<?php echo site_url('ajax/view_notes'); ?>",
				dataType: 'JSON',
				success: function(response){		
					$('#body_notes').html("");
					var len = response.length;
					var no = 1;
				
					for(var i=0; i<len; i++){
						var created_t = response[i].created;
						var created = moment(created_t).format('DD/MM/YYYY');
						var deskripsi = response[i].deskripsi;
						var user_id = response[i].users_id;

						var str = '<div class="post clearfix">' +
									'<div class="user-block">' +
										'<span class="lead">' +
											'<a href="#">' + created + '</a>' +
										'</span><br/>' +
									'</div>' +
									'<p>' + deskripsi +
									'</p>' +
								'</div> ';
						$('#body_notes').append(str);
					}
					
					$('#modal_view_notes').modal('show');
				},
				error: function(){
				//alert("Gagal");
				}
			});
		});

		//Modal Edit For Potongan
		$(document).on("click", "#potongan_therapist .btn-edit", function(){
			var kodekar = $(this).attr('kodekar');
			var id_potongan = $(this).attr('data-id');
			modal_potongan_therapist(kodekar, id_potongan);
		});

		$("#modal_edit_potongan_therapist button[type=submit]").click(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('report/update_potongan_therapist'); ?>",
				data: $('form.form_potongan_therapist').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_potongan_therapist").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});

		//Modal Edit For Komisi Lain
		$(document).on("click", "#komisi_lain .btn-edit", function(){
			var kodekar = $(this).attr('kodekar');
			var id_lain = $(this).attr('data-id');
			modal_komisi_lain(kodekar, id_lain);
		});

		$("#modal_edit_komisi_lain button[type=submit]").click(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('report/update_komisi_lain'); ?>",
				data: $('form.form_komisi_lain').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_komisi_lain").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});

		$("#modal_edit_spv_therapist button[type=submit]").click(function(){
			//console.log( $('form#form_spv_therapist').serialize())
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('ajax/update_insert_spv_therapist'); ?>",
				data: $('form#form_spv_therapist').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_spv_therapist").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});

		$("#modal_edit_gaji_spv_therapist button[type=submit]").click(function(){
			//console.log( $('form#form_spv_therapist').serialize())
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('ajax/update_gaji_spv_therapist'); ?>",
				data: $('form#form_gaji_spv_therapist').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_gaji_spv_therapist").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});

		//Modal Edit For Komisi Kuesioner
		$(document).on("click", "#komisi_kuesioner .btn-edit", function(){
			var kodekar = $(this).attr('kodekar');
			var id_kuesioner = $(this).attr('data-id');
			modal_komisi_kuesioner(kodekar, id_kuesioner);
		});

		$("#modal_edit_komisi_kuesioner button[type=submit]").click(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('report/update_komisi_kuesioner'); ?>",
				data: $('form.form_komisi_kuesioner').serialize(),
						success: function(msg){
								$("#status").html(msg)
							$("#modal_edit_komisi_kuesioner").modal('hide'); 
							location.reload();
						},
				error: function(){
				alert("Gagal");
				}
			});
		});


	});

	function komisi_gro_treatment() {
		
		var dari_tanggal = $('#dari_tanggal_gro').val();
		var sampai_tanggal = $('#sampai_tanggal_gro').val();
		var branch_id = $('#branch_id').val();

		$.ajax( {
			data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/total_lulur')?>",
			type: "POST",
			success:function(response){
				var total_lulur_tbl 			= response;	
				
				$.ajax({
					url:"<?php echo site_url('ajax/komisi_lulur')?>",
					type: "POST",
					success:function(response){
						var komisi_lulur_tbl = parseInt(response);
						$('#komisi_lulur').html(komisi_lulur_tbl);						
						var total_komisi_lulur_tbl 	= total_lulur_tbl*komisi_lulur_tbl;				
						$('#total_lulur_tbl').html(total_lulur_tbl);
						$('#komisi_lulur_tbl').html(komisi_lulur_tbl);
						$('#total_komisi_lulur_tbl').html(parseFloat(Math.round((total_komisi_lulur_tbl) * 100) / 100).toLocaleString());

						var komisi_gro_temp = parseInt($('#total_komisi_gro').html());
						var komisi_gro = komisi_gro_temp+total_komisi_lulur_tbl;
						$('#total_komisi_gro').html(komisi_gro.toLocaleString());	

						var status_target = $('#status_target_mud label').attr('data-status');
						if(status_target==0){
							var potongan_komisi = komisi_gro*0.1;
							var komisi_gro = komisi_gro-potongan_komisi;
							$('#persen_potongan_komisi').html(potongan_komisi.toLocaleString());
							$('#total_komisi_gro_net').html(komisi_gro.toLocaleString());
						}else{
							$('#persen_potongan_komisi').html('-');
							$('#total_komisi_gro_net').html(komisi_gro.toLocaleString());
						}
						
						$('#komisi_trt_gro').html(komisi_gro.toLocaleString());
						var total_mud = $('#total_mud').html();
						var total_mud_hk = total_mud*1000;
						var sisa_komisi_treatment = komisi_gro-total_mud_hk;
						$('#mud_hk').html(total_mud_hk.toLocaleString());

						var total_trt_gro = sisa_komisi_treatment*0.805;
						
						$('#sisa_komisi_treatment').html(sisa_komisi_treatment.toLocaleString());
						$('#komisi_gro_after').html(Math.floor(total_trt_gro).toLocaleString());
						
						var total_point_all_a = 0;

						$.ajax({
							data: {"dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
							url:"<?php echo site_url('ajax/data_total_point_all')?>",
							type: "POST",
							async: false,
							success:function(response){
								//console.log(response);
								total_point_all_a = response;						
							}
						});

						$('#total_point_gro').html((total_point_all_a).toLocaleString());	
						$('#point_per_gro').html((total_trt_gro/total_point_all_a).toLocaleString());	
						//$('#data_value_gaji_gro').val((total_trt_gro/total_point_all).toLocaleString());

						var point_per_gro = total_trt_gro/total_point_all_a;

						$.ajax({
							url: "<?php echo site_url('ajax/data_gro_period'); ?>",
							data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
							type : 'POST',
							dataType: 'JSON',
							success: function(response){	

								var len = response.length;

								$(document).ready(function(){
									var per_point_gro = $('#data_value_gaji_gro').val();

									//console.log(point_per_gro);
								});
								
								for(var i=0; i<len; i++){
									var kodegro = response[i].kodegro;
									var namagro = response[i].namagro;
									
									var total_suite = 0;
									var total_hot = 0;
									var total_aroma = 0;
									var total_mud = 0;
									var total_lulur = 0;
									var total_point_all = 0;
									
									$.ajax({
										url: "<?php echo site_url('ajax/data_hot_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											//console.log(response);
											total_hot = response;
										}
									});	

									$.ajax({
										url: "<?php echo site_url('ajax/data_aroma_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											total_aroma = response;
										}
									});	

									$.ajax({
										url: "<?php echo site_url('ajax/data_mud_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											total_mud = response;
										}
									});	

									$.ajax({
										url: "<?php echo site_url('ajax/data_suite_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											total_suite = response;							
										}
									});	

									$.ajax({
										url: "<?php echo site_url('ajax/data_lulur_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											total_lulur = response;
										}
									});	

									$.ajax({
										url: "<?php echo site_url('ajax/data_total_point_by_kodegro'); ?>",
										data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
										type : 'POST',
										dataType: 'JSON',
										async: false,
										success: function(response){
											total_point_all = response;
										}
									});	

									var total_gaji = point_per_gro;
									var total_gaji = Math.floor(point_per_gro*total_point_all).toLocaleString();

									var tr_str = "<tr data-gro='" + kodegro + "'>" +
												"<th align='left'>" + namagro + "</th>" +
												"<th align='left'>" + total_point_all + "</th>" +
												"<th align='left'>" + total_gaji + "</th>" +
												"</tr>";

									$("#data_penerimaan_gaji_gro").append(tr_str);				
									
								}
							}
						});	
						
					}
				});	
			}

		});

	}

	function modal_komisi_fnb(kodekar, id_fnb) {
		
		$.ajax({
			url: "<?php echo site_url('report/data_edit_komisi_fnb'); ?>",
			data: {kodekar: kodekar, id_fnb: id_fnb},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_fnb = response.data.id_fnb;
				var kodekar = response.data.kodekar;
				var jumlah_komisi = response.data.jumlah_komisi;

				//$(".username .therapist_name").html('');
				//$("#modal_edit_komisi_fnb tbody").html('');
				//$("#modal_edit_komisi_fnb tfoot").html('');
				
				$('#modal_edit_komisi_fnb').modal('show');
				$('.jumlah_komisi').val(jumlah_komisi); 
				$('.id_fnb').val(id_fnb); 
				$('.modal-title').text('Komisi FNB'); 
				//$(".username .therapist_name").append(kodekar);
			}
		});

	}


	function modal_data_spv_therapist(id_spv) {
		var branch_id = $("#gaji_spv_therapist").attr('branch_id');
		$.ajax({
			url: "<?php echo site_url('ajax/data_edit_spv_therapist'); ?>",
			data: {id_spv: id_spv},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_spv = response.data.id_spv;
				var nama_spv = response.data.nama_spv;
				var gajipokok = response.data.gajibulan;
				var no_rek = response.data.no_rek;
				var no_telp = response.data.no_telp;
				var is_mess = response.data.is_mess;
				
				$('#modal_edit_spv_therapist').modal('show');
				$('#id_spv').val(id_spv); 
				$('#nama_spv').val(nama_spv); 
				$('#gajipokok').val(gajipokok); 
				$('#no_rek').val(no_rek); 
				$('#no_telp').val(no_telp); 
				$('#branch_code').val(branch_id); 
				$('#branch_id_list').html(""); 
				$('#jabatan').append('<option ' + (is_mess == '0' ? 'selected ' : '') + 'value="0">SPV Therapist</option>');
				$('#jabatan').append('<option ' + (is_mess == '1' ? 'selected ' : '') + 'value="1">Ibu Mess</option>');
				$('.modal-title').text('Edit Data SPV Therapist & Ibu Mess'); 

				$.ajax({
					url:"<?php echo base_url(); ?>ajax/get_branch_list",
					method:"POST",
					dataType:"json",
					success:function(response){

						$("#branch_id").html("");
						var str = "<option value='0'>- All Outlet -</option>";
						$("#branch_id").append(str);
						var len = response.length;
						for(var i=0; i<len; i++){
							var csname = response[i].csname;
							var cname = response[i].cname;
							if(csname==branch_id){				
								var str2 = "<option selected value='" + csname + "'>" + cname + "</option>";
							}else{
								var str2 = "<option value='" + csname + "'>" + cname + "</option>";
							}
							$("#branch_id_list").append(str2);
						}
						$('#modal_edit_spv_therapist').modal('show');	
								
					}
				});
			}
		});
	}

	function modal_tambah_data_spv_therapist() {

		var branch_id = $("#gaji_spv_therapist").attr('branch_id');	
		$('#modal_edit_spv_therapist').modal('show');				

		$('#id_spv').val("");
		$('#nama_spv').val("");
		$('#gajipokok').val("");
		$('#no_rek').val("");
		$('#no_telp').val("");
		$('#jabatan').html("");
		$('#branch_code').val(branch_id); 	
		$('#branch_id_list').html(""); 						

		$('#jabatan').append('<option value="0">SPV Therapist</option>');
		$('#jabatan').append('<option value="1">Ibu Mess</option>');

		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_branch_list",
			method:"POST",
			dataType:"json",
			success:function(response){

				$("#branch_id").html("");
				var str = "<option value='0'>- All Outlet -</option>";
				$("#branch_id").append(str);
				var len = response.length;
				for(var i=0; i<len; i++){
					var csname = response[i].csname;
					var cname = response[i].cname;
					if(csname==branch_id){				
						var str2 = "<option selected value='" + csname + "'>" + cname + "</option>";
					}else{
						var str2 = "<option value='" + csname + "'>" + cname + "</option>";
					}
					$("#branch_id_list").append(str2);
				}
				$('#modal_edit_spv_therapist').modal('show');	
				$('.modal-title').text('Tambah Data SPV Therapist & Ibu Mess'); 
			}
		});				
	};

	function modal_edit_gaji_spv_therapist(id_gaji, dari_tanggal, sampai_tanggal) {

		$.ajax({
			url: "<?php echo site_url('ajax/data_edit_gaji_spv_therapist'); ?>",
			data: {id_gaji, dari_tanggal, sampai_tanggal},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_gaji = response.data.id_gaji;
				var nama_spv = response.data.nama_spv;
				var gajipokok = response.data.gaji;
				var kuesioner = response.data.kuesioner;
				var tunj_luarkota = response.data.tunj_luarkota;
				var tunj_transport = response.data.tunj_transport;
				var bpjs_kry = response.data.bpjs_kry;
				var bpjs_prs = response.data.bpjs_prs;
				var total_gaji = response.data.total_gaji;
				var total_gaji = response.data.total_gaji;

				$('.modal-title').text('Edit Gaji SPV Therapist & Ibu Mess'); 
				$('#modal_edit_gaji_spv_therapist').modal('show');

				$('#modal_edit_gaji_spv_therapist #id_gaji').val(id_gaji); 
				$('#modal_edit_gaji_spv_therapist #id_spv').val(id_spv); 
				$('#modal_edit_gaji_spv_therapist #nama_spv').val(nama_spv); 
				$('#modal_edit_gaji_spv_therapist #gajipokok').val(gajipokok); 
				$('#modal_edit_gaji_spv_therapist #kuesioner').val(kuesioner); 
				$('#modal_edit_gaji_spv_therapist #tunj_luarkota').val(tunj_luarkota); 
				$('#modal_edit_gaji_spv_therapist #tunj_transport').val(tunj_transport); 
				$('#modal_edit_gaji_spv_therapist #bpjs_kry').val(bpjs_kry); 
				$('#modal_edit_gaji_spv_therapist #bpjs_prs').val(bpjs_prs); 
				$('#modal_edit_gaji_spv_therapist #total_gaji').val(total_gaji); 

			}
		});

		
		
				
	};

	function modal_view_komisi_fnb(kodekar, dari_tanggal, sampai_tanggal) {
		//console.log(kodekar);
		//console.log(dari_tanggal);
		//console.log(sampai_tanggal);
		$.ajax({
			url: "<?php echo site_url('report/data_view_komisi_fnb'); ?>",
			data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal },
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				$(".username .therapist_name").html('');
				$("#data_modal_videw_komisi_fnb").html('');
				var len = response.length;
				var no = 1;
				var total_komisi = 0;
				for(var i=0; i<len; i++){
					//console.log(response[i].total);
					var total = response[i].total;
					var namaservice = response[i].namaservice;
					var harga = response[i].harga;
					var jumlah = response[i].jumlah;
					var kodeservice = response[i].kodeservice;

					total_komisi += parseInt(total);
				
					var tr_str = "<tr>" +
						"<td align='center'>" + no + "</td>" +
						"<td align='center'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						"<td align='right'>" + formatCurrency(harga) + "</td>" +
						"<td align='right'>" + formatCurrency(total) + "</td>" +
						"</tr>";
						$("#data_modal_videw_komisi_fnb").append(tr_str);
					no++;
				}
				var komisi_fnb = parseInt(total_komisi)*0.05;
				var tr_str2 ="<tr>" +
					"<td class='text-bold' colspan='5' align='right'>Total</td>" +
					"<td class='text-bold' align='right'>" + formatCurrency(total_komisi) + "</td>" +
					"</tr>" +
					"<tr>" +
					"<td class='text-bold' colspan='5' align='right'>Total Komisi (5%) </td>" +
					"<td class='text-bold' align='right'>" + formatCurrency(komisi_fnb) + "</td>" +
					"</tr>";
				
				$("#data_modal_videw_komisi_fnb").append(tr_str2);

				$('#modal_view_komisi_fnb').modal('show');
				$('.modal-title').text('Komisi FNB'); 
				//$(".username .therapist_name").append(namakar);
			}
		});

	}

	function modal_komisi_lain(kodekar, id_lain) {
		
		$.ajax({
			url: "<?php echo site_url('report/data_edit_komisi_lain'); ?>",
			data: {kodekar: kodekar, id_lain: id_lain},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_lain = response.data.id_lain;
				var kodekar = response.data.kodekar;
				var jumlah_komisi = response.data.jumlah_komisi;

				//$(".username .therapist_name").html('');
				//$("#modal_edit_komisi_fnb tbody").html('');
				//$("#modal_edit_komisi_fnb tfoot").html('');
				
				$('#modal_edit_komisi_lain').modal('show');
				$('.jumlah_komisi').val(jumlah_komisi); 
				$('.id_lain').val(id_lain); 
				$('.modal-title').text('Komisi Lain-lain'); 
				//$(".username .therapist_name").append(kodekar);
			}
		});

	}

	function modal_insert_note(kodekar, namakar, dari_tanggal, sampai_tanggal) {
		$(".username .therapist_name").html('');
		$(".id_catatan").val('');
		$(".date_awal").val('');
		$(".date_akhir").val('');

		$('#modal_insert_notes').modal('show');
		$('.modal-title').text('Tambah Catatan'); 
		
		$(".id_catatan").val(kodekar);
		$(".date_awal").val(dari_tanggal);
		$(".date_akhir").val(sampai_tanggal);
		$(".username .therapist_name").append(namakar);
	}

	function modal_komisi_kuesioner(kodekar, id_kuesioner) {
		
		$.ajax({
			url: "<?php echo site_url('report/data_edit_komisi_kuesioner'); ?>",
			data: {kodekar: kodekar, id_kuesioner: id_kuesioner},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_kuesioner = response.data.id_kuesioner;
				var kodekar = response.data.kodekar;
				var jumlah_komisi = response.data.jumlah_komisi;
				
				$('#modal_edit_komisi_kuesioner').modal('show');
				$('.jumlah_komisi').val(jumlah_komisi); 
				$('.id_kuesioner').val(id_kuesioner); 
				$('.modal-title').text('Komisi Kuesioner'); 
				//$(".username .therapist_name").append(kodekar);
			}
		});

	}

	function modal_potongan_therapist(kodekar, id_potongan) {
		
		$.ajax({
			url: "<?php echo site_url('report/data_edit_potongan_therapist'); ?>",
			data: {kodekar: kodekar, id_potongan: id_potongan},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	
				var id_potongan = response.data.id_potongan;
				var kodekar = response.data.kodekar;
				var yayasan = response.data.yayasan;
				var asuransi = response.data.asuransi;
				var koperasi = response.data.koperasi;
				var mess = response.data.mess;
				var yayasan = response.data.yayasan;
				var hutang = response.data.hutang;
				var denda = response.data.denda;
				var zion = response.data.zion;
				var tiket = response.data.tiket;
				var sertifikasi = response.data.sertifikasi;
				var trt_glx = response.data.trt_glx;
				var bpjs_kry = response.data.bpjs_kry;
				var bpjs_prs = response.data.bpjs_prs;
				var lain_lain = response.data.lain_lain;

				//$("#modal_edit_komisi_fnb tbody").html('');
				//$("#modal_edit_komisi_fnb tfoot").html('');
				
				$('#modal_edit_potongan_therapist').modal('show');
				$('#id_potongan').val(id_potongan);
				$('#yayasan').val(yayasan); 
				$('#asuransi').val(asuransi); 
				$('#koperasi').val(koperasi); 
				$('#mess').val(mess); 
				$('#hutang').val(hutang); 
				$('#denda').val(denda); 
				$('#zion').val(zion); 
				$('#lain_lain').val(lain_lain); 
				$('#tiket').val(tiket); 
				$('#sertifikasi').val(sertifikasi); 
				$('#trt_glx').val(trt_glx); 
				$('#bpjs_kry').val(bpjs_kry); 
				$('#bpjs_prs').val(bpjs_prs); 
				$('.modal-title').text('Potongan Therapist'); 
				//$(".username .therapist_name").append(kodekar);
			}
		});

	}

	function modal_komisi_lulur() {
		var namakar = $('#namakar').html();
		var kodekar = $('#modal_komisi_lulur').attr('data-tkar');
		var dari_tanggal = $('#modal_komisi_lulur').attr('data-from');
		var sampai_tanggal = $('#modal_komisi_lulur').attr('data-to');
		var branch_id = $('#branch_code').val();		
		
		$.ajax({
			data: {"kodekar" : kodekar, "dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_detail_komisi_lulur')?>",
			type: "POST",
			dataType: "JSON",
			success:function(response){
				//console.log(response);
				//var data = JSON.parse(response);
				$(".username .therapist_name").html('');
				$("#data_komisi_lulur_plus tbody").html('');
				$("#data_komisi_lulur_plus tfoot").html('');
				$("#modal_footer_info").html('');

				var totals_sesi = 0;
				var total_point = 0;
				var len = response.length;
				var no = 1;
				for(var i=0; i<len; i++){
					//console.log(response[i]);
					var tanggal = moment(response[i].tgl).format('DD/MM/YYYY');
					var nokamar = response[i].nokamar;
					var kodeservice		= response[i].kodeservice;
					var sesi = response[i].jmlsesi;
					var point = response[i].jmlext;					
					totals_sesi += sesi;					
					total_point += point;
				
					var tr_str = "<tr>" +
					"<td align='center'>" + no + "</td>" +
					"<td align='center'>" + tanggal + "</td>" +
					"<td align='center'>" + nokamar + "</td>" +
					"<td align='center'>" + kodeservice + "</td>" +
					"<td align='center'>" + sesi + "</td>" +
					"<td align='center'>" + point + "</td>" +
					"</tr>";
					$("#data_komisi_lulur_plus").append(tr_str);
					no++;
				}
				
				$.ajax({
					data: {"kodekar" : kodekar, "dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
					url:"<?php echo site_url('ajax/get_detail_komisi_lulur_category')?>",
					type: "POST",
					dataType: "JSON",
					success:function(response){
						var lenth = response.length;
						for(var i=0; i<lenth; i++){
							var kodeservice = response[i].kodeservice;
							var jmlsesi	= response[i].jmlsesi;

							var footer_info = "<p class='cat pull-left' style='padding-right:10px;'><b>" + kodeservice + "</b> : " + jmlsesi + "</p>";
							$("#modal_footer_info").append(footer_info);

						}
					}
				});
				
				var tr_str2 = "<tfoot>" + 
					"<tr>" +
					"<td class='text-bold' colspan='4' align='right'>Total</td>" +
					"<td class='text-bold' align='center'>" + totals_sesi + "</td>" +
					"<td class='text-bold' align='center'>" + total_point + "</td>" +
					"</tr>"
					"</tfoot>";
				
				var footer_info = "<p class='cat pull-left' style='padding-right:10px;'><b>SUI</b> : 19</p>";

				$("#data_komisi_lulur_plus").append(tr_str2);

				$('#modal_data_komisi_lulur_plus').modal('show');
				$('.modal-title').text('Komisi Lulur'); 
				$(".username .therapist_name").append(namakar);
			}
		});

	}

	function modal_komisi_fnb() {
		var namakar = $('#namakar').html();
		var kodekar = $('#modal_komisi_lulur').attr('data-tkar');
		var dari_tanggal = $('#modal_komisi_lulur').attr('data-from');
		var sampai_tanggal = $('#modal_komisi_lulur').attr('data-to');
		var branch_id = $('#branch_code').val();
		
		
		$.ajax({
			data: {"kodekar" : kodekar, "dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_detail_komisi_fnb')?>",
			type: "POST",
			dataType: "JSON",
			success:function(response){
				$(".username .therapist_name").html('');
				$("#data_komisi_lulur_plus tbody").html('');
				$("#data_komisi_lulur_plus tfoot").html('');
				$("#modal_footer_info").html('');

				var totals_sesi = 0;
				var total_point = 0;
				var len = response.length;
				var no = 1;
				for(var i=0; i<len; i++){
					//console.log(response[i]);
					var tanggal = moment(response[i].tgl).format('DD/MM/YYYY');
					var nokamar = response[i].nokamar;
					var kodeservice		= response[i].kodeservice;
					var sesi = response[i].jmlsesi;
					var point = response[i].jmlext;					
					totals_sesi += sesi;					
					total_point += point;
				
					var tr_str = "<tr>" +
					"<td align='center'>" + no + "</td>" +
					"<td align='center'>" + tanggal + "</td>" +
					"<td align='center'>" + nokamar + "</td>" +
					"<td align='center'>" + kodeservice + "</td>" +
					"<td align='center'>" + sesi + "</td>" +
					"<td align='center'>" + point + "</td>" +
					"</tr>";
					$("#data_komisi_lulur_plus").append(tr_str);
					no++;
				}
				
				$.ajax({
					data: {"kodekar" : kodekar, "dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id" : branch_id},
					url:"<?php echo site_url('ajax/get_detail_komisi_lulur_category')?>",
					type: "POST",
					dataType: "JSON",
					success:function(response){
						var lenth = response.length;
						for(var i=0; i<lenth; i++){
							var kodeservice = response[i].kodeservice;
							var jmlsesi	= response[i].jmlsesi;

							var footer_info = "<p class='cat pull-left' style='padding-right:10px;'><b>" + kodeservice + "</b> : " + jmlsesi + "</p>";
							$("#modal_footer_info").append(footer_info);

						}
					}
				});
				
				var tr_str2 = "<tfoot>" + 
					"<tr>" +
					"<td class='text-bold' colspan='4' align='right'>Total</td>" +
					"<td class='text-bold' align='center'>" + totals_sesi + "</td>" +
					"<td class='text-bold' align='center'>" + total_point + "</td>" +
					"</tr>"
					"</tfoot>";
				
				var footer_info = "<p class='cat pull-left' style='padding-right:10px;'><b>SUI</b> : 19</p>";

				$("#data_komisi_lulur_plus").append(tr_str2);

				$('#modal_data_komisi_lulur_plus').modal('show');
				$('.modal-title').text('Komisi Lulur'); 
				$(".username .therapist_name").append(namakar);
			}
		});

	}

	function modal_komisi_additional() {
		
		var namakar = $('#namakar').html();
		var kodekar = $('#modal_komisi_lulur').attr('data-tkar');
		var dari_tanggal = $('#modal_komisi_lulur').attr('data-from');
		var sampai_tanggal = $('#modal_komisi_lulur').attr('data-to');
		var branch_id = $('#branch_code').val();

		//console.log(branch_id);
		
		$.ajax({
			url: "<?php echo site_url('report/data_additional_product_detail'); ?>",
			data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){					
				
				//console.log(response);

				$(".username .therapist_name").html('');
				$("#data_komisi_additional tbody").html('');
				$("#data_komisi_additional tfoot").html('');

				var len = response.length;
				var grand_total = 0;

				$.ajax({
					url: "<?php echo site_url('ajax/data_premium_room'); ?>",
					data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar, branch_id: branch_id},
					type : 'POST',
					dataType: 'JSON',
					success: function(response){	
						var len = response.length;
						var grand_total = 0;

						for(var i=0; i<len; i++){
							var kodeservice = response[i].kodeservice;
							var namaservice = response[i].namaservice;
							var jumlah		= response[i].jumlah;
							var total_komisi = response[i].total_komisi;
							var harga_komisi = response[i].harga_komisi;
						
							var tr_str = "<tr>" +
							//"<td align='left'>" + (i+1) + "</td>" +
							"<td align='center'>" + kodeservice + "</td>" +
							"<td align='left'>" + namaservice + "</td>" +
							"<td align='center'>" + jumlah + "</td>" +
							"<td align='right'>" + harga_komisi+ "</td>" +
							"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
							"</tr>";
							
							$("#data_komisi_additional").prepend(tr_str);
						}
					}
				});

				for(var i=0; i<len; i++){
					var kodeservice = response[i].kodeprod;
					var namaservice = response[i].namaprod;
					var jumlah = response[i].jumlah;
					var komisi = response[i].komisi_trp;
					var komisi_peritem = komisi.toLocaleString(undefined, {maximumFractionDigits:2});
					var total_komisi = jumlah * komisi
					
					grand_total += total_komisi;

					var tr_str = "<tr>" +
					//"<td align='left'>" + (i+1) + "</td>" +
					"<td align='center'>" + kodeservice + "</td>" +
					"<td align='left'>" + namaservice + "</td>" +
					"<td align='center'>" + jumlah + "</td>" +
					"<td align='right'>" + komisi_peritem+ "</td>" +
					"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
					"</tr>";

					$("#data_komisi_additional").append(tr_str);
				}
				
				$.ajax({
					url: "<?php echo site_url('ajax/get_total_komisi_premium_room'); ?>",
					data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar, branch_id: branch_id},
					type : 'POST',
					dataType: 'JSON',
					success: function(response){	

						var total_komisi_premium 	= response;
						var grand_total_komisi_add = total_komisi_premium+grand_total;

						var tr2_str = "<tfoot>" +
								"<tr>" +
									"<td colspan='4' align='right'><strong>Total</strong></td>" +
									"<td align='right'><strong>" + grand_total_komisi_add.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
								"</tr>"
							"</tfoot>";

						$("#data_komisi_additional").append(tr2_str);
					}
				});	
				
				$('#modal_data_komisi_additional').modal('show');
				$('.modal-title').text('Komisi Additional'); 
				$(".username .therapist_name").append(namakar);
			},
			error: function(response){
				console.log(response);
			}
		});

	}

	function isChecked() {
		var isChecked = false;
		if (document.getElementById('checked_status').checked) {
			isChecked = true;
			console.log(isChecked);
		} else {
			isChecked = false;
			console.log(isChecked);
		}
	}

	function formatDate(date) {
		var monthNames = [
			"January", "February", "March",
			"April", "May", "June", "July",
			"August", "September", "October",
			"November", "December"
		];

		var day = date.getDate();
		var monthIndex = date.getMonth();
		var year = date.getFullYear();

		return day + ' ' + monthNames[monthIndex] + ' ' + year;
	}

	function editPotonganYayasan(kodekar){		
		var kodekar = $('#kodekar').html();		
		var yayasan_value = $('#potongan_yayasan').html();
		$.ajax({
			data: {"kodekar" : kodekar},
			url:"<?php echo site_url('ajax/edit_potongan_yayasan')?>",
			type: "POST",
			success:function(response){
				var html = "";
					html += "<div class='input-group input-group-sm'>";
					html += "   <input type='text' id='yayasan_value' value='" + yayasan_value + "' class='form-control'>";
					html += "     <span class='input-group-btn'>";
					html += "		<button type='button' class='btn btn-primary' onClick='SavePotonganYayasan();'>Save</button>";
					html += "	</span>";
					html += "</div>";
				$('#potongan_yayasan').html('');
				$('#potongan_yayasan').append(html);
			}
		});
	}

	function SavePotonganYayasan(kodekar){		
		var id_potongan = $('#id_potongan').val();
		var kodekar = $('#kodekar').html();
		var branch_id = $('#branch_code').val();
		var periode = $('#sampai_tanggal').html();
		var yayasan_value = $('#yayasan_value').val();
		$.ajax({
			data: {"kodekar" : kodekar, "branch_id" : branch_id, "id_potongan" : id_potongan, "yayasan_value" : yayasan_value},
			url:"<?php echo site_url('ajax/save_potongan_yayasan')?>",
			type: "POST",
			success:function(response){
				var new_val = $('#yayasan_value').val();
				$('#potongan_yayasan').html('');
				$('#potongan_yayasan').html((parseInt(new_val)).toLocaleString(undefined, {maximumFractionDigits:2}));
			}
		});
	}

	function total_sesi_therapist(){		
		var kodekar = $('#total_sesi_kar').attr('kodekar');
		var data_from = $('#total_sesi_kar').attr('data-from');
		var data_to = $('#total_sesi_kar').attr('data-to');	
		var branch_id = $('#branch_id').val();	

		$("#footer_info_sesi").html('');

		//datatables
		table = $('#total_sesi_therapist').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75, 100 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"kodekar" : kodekar, "dari_tanggal" : data_from, "sampai_tanggal" : data_to, "branch_id" : branch_id},
				"url": "<?php echo site_url('report/total_sesi_therapist')?>",
				"type": "POST"
			},

			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "20px",  "className": "text-center",  "targets": 0 },
				{ "width": "40px",  "className": "text-center", "targets": 1 },
				{ "width": "200px",  "className": "text-center", "targets": 2 },
				{ "width": "80px", "className": "text-center",  "targets": 3 },
				{ "width": "100px", "className": "text-center", "targets": 4 },
				{ "width": "100px", "className": "text-center", "targets": 5 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			},

			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api();
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	
				// Total over all pages
				var totalpoint = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
	
				// Total over this page
				var totalsesi = api
					.column( 5, { page: 'current'} )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
					/*
					var p = api.column(4).data().reduce(function (a, b) {
						return a + b;
					}, 0)
					var q = api.column(3).data().reduce(function (a, b) {
						return a + b;
					}, 0)
					*/
           		//$(api.column(4).footer()).html(q);
            	//$(api.column(3).footer()).html(p);
				// Update footer
				$(api.column(5).footer()).html(totalsesi);
				$(api.column(6).footer()).html(totalpoint);
				/*
				jQuery( api.column( 5 ).footer() ).html(
					totalsesi +'  ('+ totalpoint +' Point)'
				);
				*/
			},

		});
		
		$.ajax({
			data: {"kodekar" : kodekar, "dari_tanggal" : data_from, "sampai_tanggal" : data_to, "branch_id" : branch_id},
			url:"<?php echo site_url('ajax/get_detail_komisi_lulur_category')?>",
			type: "POST",
			dataType: "JSON",
			success:function(response){
				var lenth = response.length;
				for(var i=0; i<lenth; i++){
					var kodeservice = response[i].kodeservice;
					var jmlsesi	= response[i].jmlsesi;

					var footer_info = "<p class='cat pull-left' style='padding-right:10px;'><b>" + kodeservice + "</b> : " + jmlsesi + "</p>";
					$("#footer_info_sesi").append(footer_info);

				}
			}
		});

	};

	function data_gro_period(){		
		var dari_tanggal = $('#dari_tanggal_gro').val();
		var sampai_tanggal = $('#sampai_tanggal_gro').val();
		var branch_id = $('#branch_id').val();
		
		$("#data_point_detail_gro").html("");
		
		$.ajax({
			url: "<?php echo site_url('ajax/data_gro_period'); ?>",
			data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
			type : 'POST',
			dataType: 'JSON',
			success: function(response){	

				var len = response.length;

				for(var i=0; i<len; i++){
					var kodegro = response[i].kodegro;
					var namagro = response[i].namagro;
					
					var total_suite = 0;
					var total_hot = 0;
					var total_aroma = 0;
					var total_mud = 0;
					var total_lulur = 0;
					var total_point_all = 0;

					$.ajax({
						url: "<?php echo site_url('ajax/data_hot_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_hot = response;
						}
					});	

					$.ajax({
						url: "<?php echo site_url('ajax/data_aroma_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_aroma = response;
						}
					});	

					$.ajax({
						url: "<?php echo site_url('ajax/data_mud_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_mud = response;
						}
					});	

					$.ajax({
						url: "<?php echo site_url('ajax/data_suite_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_suite = response;							
						}
					});	

					$.ajax({
						url: "<?php echo site_url('ajax/data_lulur_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_lulur = response;
						}
					});	

					$.ajax({
						url: "<?php echo site_url('ajax/data_total_point_by_kodegro'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id, kodegro: kodegro},
						type : 'POST',
						dataType: 'JSON',
						async: false,
						success: function(response){
							total_point_all = response;
						}
					});	

					var tr_str = "<tr data-gro='" + kodegro + "'>" +
								"<th align='left'>" + namagro + "</th>" +
								"<th align='left'>" + total_suite + "</th>" +
								"<th align='left'>" + total_hot + "</th>" +
								"<th align='left'>" + total_aroma + "</th>" +
								"<th align='left'>" + total_mud + "</th>" +
								"<th align='left'>" + total_lulur + "</th>" +
								"<th align='left'>" + total_point_all + "</th>" +
								"</tr>";

					$("#data_point_detail_gro").append(tr_str);
				
					
				}
			}
		});	

	};

	function branch_list(){

		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_branch_list",
			method:"POST",
			dataType:"json",
			success:function(response){
				$("#branch_id_list").html("");
				var str = "<option value='0'>- All Outlet -</option>";
				$("#branch_id_list").append(str);
				//console.log(response.length);
				var len = response.length;
				for(var i=0; i<len; i++){
					var csname = response[i].csname;
					var cname = response[i].cname;
					var str2 = "<option value='" + csname + "'>" + cname + "</option>";
					$("#branch_id_list").append(str2);
				}
				$('.modal-title').text("Tambah User");
				$('#addusersModal').modal('show');
			}
		})

	}

	function data_penerimaan_gaji(){	

		var dari_tanggal = $('#dari_tanggal_gro').val();
		var sampai_tanggal = $('#sampai_tanggal_gro').val();
		var branch_id = $('#branch_id').val();
		
		$("#data_penerimaan_gaji_gro").html("");
		
		

	};

	function myCallback(response) {
		result = response;
		console.log("Inside ajax: "+result);         
	}

	function total_komisi_additional_therapist(){		
		var kodekar = $('#total_sesi_kar').attr('kodekar');
		var data_from = $('#total_sesi_kar').attr('data-from');
		var data_to = $('#total_sesi_kar').attr('data-to');	

		//datatables
		table = $('#komisi_add_product').DataTable({ 

			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.
			"pageLength": 20,
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
			"lengthMenu": [ 10, 20, 50, 75 ],
			"responsive": true,

			// Load data for the table's content from an Ajax source
			"ajax": {
				"data": {"kodekar" : kodekar, "dari_tanggal" : data_from, "sampai_tanggal" : data_to},
				"url": "<?php echo site_url('report/detail_additional_therapist')?>",
				"type": "POST"
			},

			"columnDefs": [
				{ 
					"targets": [ 0 ], //first column / numbering column
					"orderable": false, //set not orderable
				},
				{ "width": "20px",  "className": "text-center",  "targets": 0 },
				{ "width": "40px",  "className": "text-center", "targets": 1 },
				{ "width": "200px",  "className": "text-center", "targets": 2 },
				{ "width": "80px", "className": "text-center",  "targets": 3 },
			],
			"fnCreatedRow": function( nRow, aData, iDataIndex ) {
				$(nRow).attr('kodekar', aData[1]);
				$(nRow).attr('branch_id', aData[1]);
			},
			/*
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api();
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
	
				// Total over all pages
				var totalpoint = api
					.column( 6 )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
	
				// Total over this page
				var totalsesi = api
					.column( 5, { page: 'current'} )
					.data()
					.reduce( function (a, b) {
						return intVal(a) + intVal(b);
					} );
	
				// Update footer
				$( api.column( 5 ).footer() ).html(
					totalsesi
				);
				$( api.column( 6 ).footer() ).html(
					totalpoint
				);
			}
			*/

		});
	};

	$(function(){
		$('#laporan_pendapatan_therapist, #komisi_fnb, #komisi_kuesioner, #komisi_lain, #potongan_therapist, #list_data_therapist, #penjualan_gro').on('click', 'tr', function () {			
			$('#laporan_pendapatan_therapist, #komisi_fnb, #komisi_kuesioner, #komisi_lain, #potongan_therapist, #list_data_therapist, #penjualan_gro').find('tr').removeClass('active');
			$(this).addClass('active');
		});
	});

	$(function(){
		$('#laporan_pendapatan_therapist').on('click', '#total_sesi_kar', function () {
			$('#komisi_add_product').hide();
			$('#total_sesi_therapist').show();
			$('#total_sesi_therapist').DataTable().clear().destroy();
			total_sesi_therapist();

		});
	});

	$(function(){
		$('#laporan_pendapatan_therapist').on('click', '#total_point_kar', function () {
			$('#komisi_add_product').hide();
			$('#total_sesi_therapist').show();
			$('#total_sesi_therapist').DataTable().clear().destroy();
			total_sesi_therapist();

		});
	});

	$(function(){
		$('#laporan_pendapatan_therapist').on('click', '#total_komisi_lulur_kar', function () {
			$('#komisi_add_product').hide();
			$('#total_sesi_therapist').show();
			$('#total_sesi_therapist').DataTable().clear().destroy();
			total_sesi_therapist();

		});
	});

	$(function(){
		$('#laporan_pendapatan_therapist').on('click', '#total_additional_kar', function () {
			
			$("#detail_additional").html("");
			$("#footer_info_sesi").html('');

			$('#total_sesi_therapist').hide();
			$('#komisi_add_product').show();

			var kodekar = $('#total_sesi_kar').attr('kodekar');
			var dari_tanggal = $('#total_sesi_kar').attr('data-from');
			var sampai_tanggal = $('#total_sesi_kar').attr('data-to');	
			var branch_id = $('#branch_id').val();
			
			$.ajax({
				url: "<?php echo site_url('report/data_additional_product_detail'); ?>",
				data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, branch_id: branch_id},
				type : 'POST',
				dataType: 'JSON',
				success: function(response){	
					var len = response.length;
					var grand_total = 0;

					$.ajax({
						url: "<?php echo site_url('ajax/data_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar,branch_id: branch_id},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							
							var len = response.length;
							var grand_total = 0;
							$('#kodekar').append(kodekar);

							for(var i=0; i<len; i++){
								var kodeservice = response[i].kodeservice;
								var namaservice = response[i].namaservice;
								var jumlah		= response[i].jumlah;
								var total_komisi = response[i].total_komisi;
								var harga_komisi = response[i].harga_komisi;
							
								var tr_str = "<tr>" +
								//"<td align='left'>" + (i+1) + "</td>" +
								"<td align='center'>" + kodeservice + "</td>" +
								"<td align='left'>" + namaservice + "</td>" +
								"<td align='center'>" + jumlah + "</td>" +
								"<td align='right'>" + harga_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
								"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
								"</tr>";
								
								$("#detail_additional").prepend(tr_str);
							}
						}
					});

					for(var i=0; i<len; i++){
						var kodeservice = response[i].kodeprod;
						var namaservice = response[i].namaprod;
						var jumlah = response[i].jumlah;
						var komisi = response[i].komisi_trp;
						var komisi_peritem = parseInt(komisi);
						var total_komisi = jumlah * komisi
						
						grand_total += total_komisi;

						var tr_str = "<tr>" +
						//"<td align='left'>" + (i+1) + "</td>" +
						"<td align='center'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						"<td align='right'>" + komisi_peritem.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"</tr>";

						$("#detail_additional").append(tr_str);
					}

					$.ajax({
						url: "<?php echo site_url('ajax/get_total_komisi_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar, branch_id: branch_id},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							var total_komisi_premium 	= response;
							var grand_total_komisi_add = total_komisi_premium+grand_total;

							var tr2_str = "<tr>" +
								"<td colspan='4' align='right'><strong>Total</strong></td>" +
								"<td align='right'><strong>" + grand_total_komisi_add.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
							"</tr>";

							$("#detail_additional").append(tr2_str);		

						}
					});		
				}
			});
		});
	});

	$(function(){
		$('#laporan_pendapatan_therapist').on('click', '#total_fnb_kar', function () {

			$("#detail_additional").html("");
			$("#footer_info_sesi").html('');

			$('#total_sesi_therapist').hide();
			$('#komisi_add_product').show();

			var kodekar = $('#total_sesi_kar').attr('kodekar');
			var dari_tanggal = $('#total_sesi_kar').attr('data-from');
			var sampai_tanggal = $('#total_sesi_kar').attr('data-to');	
			var branch_id = $('#branch_id').val();	

			$.ajax({
				data: {"kodekar" : kodekar, "dari_tanggal" : dari_tanggal, "sampai_tanggal" : sampai_tanggal, "branch_id": branch_id},
				url:"<?php echo site_url('ajax/get_detail_komisi_fnb')?>",
				type: "POST",
				dataType: "JSON",
				success:function(response){

					var len = response.length;
					var grand_total = 0;

					for(var i=0; i<len; i++){
						var kodeservice = response[i].kodeservice;
						var namaservice = response[i].namaservice;
						var jumlah = response[i].jumlah;
						var komisi_temp = response[i].total;
						var harga = response[i].harga;
						//var komisi_peritem = komisi.toLocaleString(undefined, {maximumFractionDigits:2});
						var komisi_peritem = harga*0.05;
						var total_komisi = jumlah*komisi_peritem
						grand_total += total_komisi;

						var tr_str = "<tr>" +
						//"<td align='left'>" + (i+1) + "</td>" +
						"<td align='center'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						"<td align='right'>" + komisi_peritem.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"</tr>";

						$("#detail_additional").append(tr_str);
					}

					var tr2_str = "<tr>" +
							"<td colspan='4' align='right'><strong>Total</strong></td>" +
						"<td align='right'><strong>" + grand_total.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
					"</tr>";

					$("#detail_additional").append(tr2_str);
				}
			});

			/*
			$.ajax({
				url: "<?php echo site_url('report/get_detail_komisi_fnb'); ?>",
				data: {kodekar: kodekar, dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal},
				type : 'POST',
				dataType: 'JSON',
				success: function(response){	
					var len = response.length;
					var grand_total = 0;

					$.ajax({
						url: "<?php echo site_url('ajax/data_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar,},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							
							var len = response.length;
							var grand_total = 0;
							$('#kodekar').append(kodekar);

							for(var i=0; i<len; i++){
								var kodeservice = response[i].kodeprod;
								var namaservice = response[i].namaprod;
								var jumlah		= response[i].jumlah;
								var total_komisi = response[i].total_komisi;
								var harga_komisi = response[i].harga_komisi;
							
								var tr_str = "<tr>" +
								//"<td align='left'>" + (i+1) + "</td>" +
								"<td align='center'>" + kodeservice + "</td>" +
								"<td align='left'>" + namaservice + "</td>" +
								"<td align='center'>" + jumlah + "</td>" +
								"<td align='right'>" + harga_komisi+ "</td>" +
								"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
								"</tr>";
								
								$("#detail_additional").prepend(tr_str);
							}
						}
					});

					for(var i=0; i<len; i++){
						var kodeservice = response[i].kodeprod;
						var namaservice = response[i].namaprod;
						var jumlah = response[i].jumlah;
						var komisi = response[i].komisi_trp;
						var komisi_peritem = komisi.toLocaleString(undefined, {maximumFractionDigits:2});
						var total_komisi = jumlah * komisi
						
						grand_total += total_komisi;

						var tr_str = "<tr>" +
						//"<td align='left'>" + (i+1) + "</td>" +
						"<td align='center'>" + kodeservice + "</td>" +
						"<td align='left'>" + namaservice + "</td>" +
						"<td align='center'>" + jumlah + "</td>" +
						"<td align='right'>" + komisi_peritem+ "</td>" +
						"<td align='right'>" + total_komisi.toLocaleString(undefined, {maximumFractionDigits:2}) + "</td>" +
						"</tr>";

						$("#detail_additional").append(tr_str);
					}

					$.ajax({
						url: "<?php echo site_url('ajax/get_total_komisi_premium_room'); ?>",
						data: {dari_tanggal: dari_tanggal, sampai_tanggal: sampai_tanggal, kodekar: kodekar,},
						type : 'POST',
						dataType: 'JSON',
						success: function(response){	
							var total_komisi_premium 	= response;
							var grand_total_komisi_add = total_komisi_premium+grand_total;

							var tr2_str = "<tr>" +
								"<td colspan='4' align='right'><strong>Total</strong></td>" +
								"<td align='right'><strong>" + grand_total_komisi_add.toLocaleString(undefined, {maximumFractionDigits:2}) + "</strong></td>" +
							"</tr>";

							$("#detail_additional").append(tr2_str);		

						}
					});		
				}
			});
			*/

		});
	});

	$(document).on('click', '#btn_tambah_ajaran', function(){
		var jabatan = '';
		var jeniskelamin = '';
		var divisi = '';
		var area = '';
		$('#modal_tambah_ajaran').modal('show');
	});

	$(document).on('click', '#btn_tambah_siswa_modal', function(){
		var jabatan = '';
		var jeniskelamin = '';
		var divisi = '';
		var area = '';
		jenis_kelamin(jeniskelamin);
		$('#tambah_siswa_modal').modal('show');
	});

	$(document).on('click', '#btn_tambah_paket', function(){
		var jabatan = '';
		var jeniskelamin = '';
		var divisi = '';
		var area = '';
		$('#modal_tambah_paket').modal('show');
	});

	$(document).on('click', '#tambah_user', function(){

		$('#addusersModal').modal('show');
		
	});
	
	$(document).on('click', '#tambah_vendor', function(){

		$('.modal-title').text("Tambah Vendor");
		$('#modal_tambah_vendor').modal('show');
		
	});
	$(document).on('click', '#tambah_area_kerja', function(){

		$('.modal-title').text("Tambah Area Kerja");
		$('#modal_tambah_area_kerja').modal('show');
		
	});
	$(document).on('click', '#tambah_kategori', function(){

		$('.modal-title').text("Tambah Kategori");
		$('#modal_tambah_kategori').modal('show');
		
	});
	
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass   : 'iradio_minimal-blue'
    })

	$(document).on('click', '#view_users', function(){
		var id	 = $(this).attr("user_id");

		$.ajax({
			url:"<?php echo base_url(); ?>ajax/get_user_by_id",
			method:"POST",
			data:{id:id},
			dataType:"json",
			success:function(data){

				$("#checked_status").html("");
				$('#usersModal').modal('show');
				$('#user_id').val(data.user_id);
				$('#fullname').val(data.nama_lengkap);
				$('#username').val(data.username);
				$('#email').val(data.email);
				
				$.ajax({
					url:"<?php echo site_url('ajax/get_user_roles')?>",
					method:"POST",
					dataType:"json",
					success:function(response){
						$("#level").html("");
						var len = response.length;
						for(var i=0; i<len; i++){
							var id_jabatan = response[i].id_jabatan;
							var nama_jabatan = response[i].nama_jabatan;
							var str2 = "<option " + (id_jabatan == data.jabatan_id ? 'selected ' : '') + "value='" + id_jabatan + "'>" + nama_jabatan +"</option>";
							$("#level").append(str2);
						}
					}
				});		

				$('#status').val(data.status);
				
				if(data.status==1){
					var status = '<input type="checkbox" value="on" name="status" class="minimal" checked />';
				}else{
					//console.log(data.status);
					var status = '<input type="checkbox" name="status" class="minimal" />';
				}				

				$("#checked_status").append(status);				
				$('.modal-title').text("Edit User");				
				$('#action').val("Submit");
				$('#operation').val("Edit");

			}
		})
	});
 
</script>