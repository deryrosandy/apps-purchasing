/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : apps_purchasing_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 13/08/2021 04:29:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for divisi
-- ----------------------------
DROP TABLE IF EXISTS `divisi`;
CREATE TABLE `divisi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of divisi
-- ----------------------------
INSERT INTO `divisi` VALUES (1, 'No Divisi', '\r\n');
INSERT INTO `divisi` VALUES (2, 'Akunting', '');
INSERT INTO `divisi` VALUES (3, 'IT', '-');
INSERT INTO `divisi` VALUES (4, 'Purchasing', '');
INSERT INTO `divisi` VALUES (5, 'Direktur', '');

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_request` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_po` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `spesifikasi` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date` date NULL DEFAULT NULL,
  `posisi_request` int(11) NOT NULL DEFAULT 1 COMMENT '1 = open, 2 = approved kabag, 3 = approved purchasing, 4 = approved kabag purchasing, 5 = approved direktur, 6 = order, 7 = selesai',
  `satuan_id` int(11) NULL DEFAULT NULL,
  `vendor_id` int(11) NULL DEFAULT NULL,
  `divisi_id` int(11) NULL DEFAULT NULL,
  `status` enum('open','process','order','closed','rejected') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'open',
  `nominal` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_dir` tinyint(5) NULL DEFAULT 0,
  `is_po` tinyint(5) NULL DEFAULT 0,
  `po_date` datetime(0) NULL DEFAULT NULL,
  `po_by` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request
-- ----------------------------
INSERT INTO `request` VALUES (1, 'SPPB-20210810001', 'PO-20210810001', 'Pembelian Software Windows', 'WIndows 10 Enterprise', 10, 'WIndows 10 Enterprise Untuk Staff Baru', '2021-08-29', 6, 1, NULL, 4, 'order', '10000000', 0, 1, '2021-08-13 03:17:10', 13, 9, '2021-08-13 03:13:43');

-- ----------------------------
-- Table structure for request_aproval
-- ----------------------------
DROP TABLE IF EXISTS `request_aproval`;
CREATE TABLE `request_aproval`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NULL DEFAULT NULL,
  `aproved_by` int(11) NULL DEFAULT NULL,
  `status` enum('rejected','approved') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'approved',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request_aproval
-- ----------------------------
INSERT INTO `request_aproval` VALUES (1, 1, 14, 'approved', 'ok', '2021-08-13 03:14:27.000000');
INSERT INTO `request_aproval` VALUES (2, 1, 9, 'approved', 'siap', '2021-08-13 03:14:55.000000');
INSERT INTO `request_aproval` VALUES (3, 1, 13, 'approved', 'siappp', '2021-08-13 03:16:54.000000');

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of satuan
-- ----------------------------
INSERT INTO `satuan` VALUES (1, 'pcs', '-');
INSERT INTO `satuan` VALUES (2, 'pax', NULL);
INSERT INTO `satuan` VALUES (3, 'kg', '-');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `akses_level` enum('admin','staff','kabag','purchase','kabag_purchase','directur') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'admin',
  `status` tinyint(1) NULL DEFAULT 1,
  `divisi_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (2, 'Admin', 'admin@admin.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1, 1, NULL);
INSERT INTO `users` VALUES (9, 'Staff Purchase', 'staff_purchase@gmail.com', 'purchase', '21232f297a57a5a743894a0e4a801fc3', 'purchase', 1, 4, NULL);
INSERT INTO `users` VALUES (10, 'staff IT', 'staff@gmail.com', 'staff', '21232f297a57a5a743894a0e4a801fc3', 'staff', 1, 3, NULL);
INSERT INTO `users` VALUES (11, 'Direktur', 'directur@gmail.com', 'directur', '21232f297a57a5a743894a0e4a801fc3', 'directur', 1, 1, NULL);
INSERT INTO `users` VALUES (13, 'Kabag Puchase', 'kabag@gmail.com', 'kabagpurchase', '21232f297a57a5a743894a0e4a801fc3', 'kabag_purchase', 1, 4, NULL);
INSERT INTO `users` VALUES (14, 'kabag', 'kabag@gmail.com', 'kabag', '21232f297a57a5a743894a0e4a801fc3', 'kabag', 1, 4, NULL);

-- ----------------------------
-- Table structure for vendor
-- ----------------------------
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pic` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp_pic` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of vendor
-- ----------------------------
INSERT INTO `vendor` VALUES (9, 'Vendor Saya', '87812125', 'vendor@gmail.com', 'Agus', '085754552', 'Bekasi Jawa Barat', NULL);

SET FOREIGN_KEY_CHECKS = 1;
